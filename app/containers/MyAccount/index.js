import React, { useState, useEffect, Fragment } from 'react';
import PropTypes from 'prop-types';
import { push } from 'connected-react-router';

import { connect } from 'react-redux';
import { compose } from 'redux';
import moment from 'moment';
import { Grid, Typography, Tooltip } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { createStructuredSelector } from 'reselect';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';

import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import AccessibilityIcon from '@material-ui/icons/AccessibilityNew';
import IconButton from '@material-ui/core/IconButton';
import ApproveIcon from '@material-ui/icons/ThumbUp';
import DenyIcon from '@material-ui/icons/ThumbDown';

import TextField from 'components/TextField';
import SelectField from 'components/Select';
import StatesSelect from 'components/StatesSelect';

import { MEMBER_TYPES, VDPC_LITE_PLAN_TYPES } from 'utils/config';

import countries from 'utils/countries.json';
import ethnicities from 'utils/ethnicities.json';
import races from 'utils/races.json';

import {
  handleChange,
  handleEmailChange,
  setValuesFrom,
  extractFormValues,
  highlightFormErrors,
} from 'utils/formHelper';

import {
  getPatientDetail,
  updatePatient,
  logoutUser,
  resetUpdatedPatient,
  getPatientDependents,
  postFamilyInvite,
  resetFamilyInviteData,
  postAddDependent,
  resetAddDependent,
  getFamilyMembers,
  putFamilyMemberApproval,
  resetFamilyMemberApproval,
} from 'containers/App/actions';

import {
  makeSelectVerifyAuth,
  makeSelectUpdatedPatient,
  makePatientDependents,
  makeSelectFamilyInvite,
  makeSelectAddDependent,
  makeSelectFamilyMembers,
  makeSelectApproveFamilyMember,
} from 'containers/App/selectors';

import AddDependentModal from './add-dependent-modal';
import InviteMemberModal from './invite-member-modal';
import ApproveMemberModal from './approve-member-modal';

import model from './model';

const useStyles = makeStyles(theme => ({
  formContainer: {
    width: '100%',
  },
  loginLink: {
    marginLeft: '1rem',
    textDecoration: 'none !important',
    '& hover': {
      textDecoration: 'none !important',
    },
  },
  scroll: {
    overflow: 'auto',
    maxHeight: '300px',
    border: '1px solid #d9d9d9',
    borderRadius: '5px',
    margin: '1rem',
  },
  familyMemberDiv: {
    [theme.breakpoints.down('sm')]: {
      marginBottom: '3rem',
    },
  },
}));

export function MyAccount({
  dispatch,
  doLogoutUser,
  doResetUpdatedPatient,
  doFamilyInvite,
  enqueueSnackbar,
  updatedPatient,
  auth,
  dependents,
  familyInvite,
  doResetFamilyInvite,
  doAddDependent,
  addDependent,
  doResetAddDependent,
  doGetPatientDependents,
  doGetFamilyMembers,
  familyMembers,
  approveFamilyMember,
  doApproveFamilyMember,
  doClearMemberApprove,
}) {
  const [profile, setProfile] = useState({ ...model });
  const [showDepdenentForm, setShowDependentForm] = useState(false);
  const [showInviteForm, setShowInviteForm] = useState(false);
  const [memberApproval, setMemberApproval] = useState({
    open: false,
    member: {},
  });
  useEffect(() => {
    if (auth.data) {
      dispatch(doGetFamilyMembers(auth.data.patient.id));
    }
  }, []);

  useEffect(() => {
    if (auth.data) {
      const fieldsWithValues = setValuesFrom(
        { ...auth.data.patient, gender: auth.data.patient.genderId.toString() },
        profile,
        true,
      );

      setProfile({ ...fieldsWithValues });
    }
  }, [auth]);

  useEffect(() => {
    if (!familyInvite.error && familyInvite.data) {
      enqueueSnackbar(familyInvite.data, {
        variant: 'success',
      });
      dispatch(doResetFamilyInvite());
      handleFamilyInviteToggle();
    }
  }, [familyInvite]);

  useEffect(() => {
    if (!addDependent.error && addDependent.data) {
      enqueueSnackbar('Dependent Successfully added', {
        variant: 'success',
      });
      dispatch(doGetPatientDependents(auth.data.patient.id));
      dispatch(doResetAddDependent());
      handleShowDependentToggle();
    }
  }, [addDependent]);

  useEffect(() => {
    if (updatedPatient.data) {
      enqueueSnackbar('Account successfully verified. Please wait.', {
        variant: 'success',
      });
      setTimeout(() => {
        dispatch(doResetUpdatedPatient());
        dispatch(doLogoutUser());
        dispatch(push('/login'));
      }, 3000);
    }
  }, [updatedPatient]);

  useEffect(() => {
    if (approveFamilyMember.error) {
      enqueueSnackbar('Something went wrong. Please try again.', {
        variant: 'error',
      });
      dispatch(doClearMemberApprove());
    }
    if (approveFamilyMember.data) {
      dispatch(doGetFamilyMembers(auth.data.patient.id));
    }
  }, [approveFamilyMember]);

  const classes = useStyles();

  const onInputChange = field => event => {
    handleChange({
      field: field.id,
      state: profile,
      event,
      saveStepFunc: setProfile,
    });
  };

  const onEmailChange = field => event => {
    handleEmailChange({
      field: field.id,
      state: profile,
      event,
      saveStepFunc: setProfile,
    });
  };

  const renderInfo = field => (
    <Grid container spacing={3} justify="center" alignItems="center">
      <Grid item xs={12} md={6}>
        <TextField
          field={field.firstName}
          variant="outlined"
          onChange={onInputChange(field.firstName)}
        />
      </Grid>
      <Grid item xs={12} md={6}>
        <TextField
          field={field.lastName}
          variant="outlined"
          onChange={onInputChange(field.lastName)}
        />
      </Grid>
      <Grid item xs={12} md={4}>
        <TextField
          field={{
            ...field.birthDateAt,
            value: moment(field.birthDateAt.value).format('MM/DD/YYYY'),
          }}
          variant="outlined"
          onChange={onInputChange(field.birthDateAt)}
        />
      </Grid>
      <Grid item xs={12} md={4}>
        <TextField
          field={field.email}
          variant="outlined"
          type="email"
          onChange={onEmailChange(field.email)}
        />
      </Grid>
      <Grid item xs={12} md={4}>
        <TextField
          field={field.employerCode}
          variant="outlined"
          onChange={onInputChange(field.employerCode)}
        />
      </Grid>

      <Grid item xs={12} md={6}>
        <TextField
          field={field.phoneNumber}
          variant="outlined"
          onChange={onInputChange(field.phoneNumber)}
        />
      </Grid>
      <Grid item xs={12} md={6}>
        <SelectField
          field={field.gender}
          options={[
            { name: '', value: '' },
            { name: 'Male', value: '12000' },
            { name: 'Female', value: '12001' },
          ]}
          disabled
          variant="outlined"
          onChange={onInputChange(field.gender)}
        />
      </Grid>
      <Grid item xs={12} md={6}>
        <TextField
          field={field.address1}
          variant="outlined"
          onChange={onInputChange(field.address1)}
        />
      </Grid>
      <Grid item xs={12} md={6}>
        <TextField
          field={field.address2}
          variant="outlined"
          onChange={onInputChange(field.address2)}
        />
      </Grid>
      <Grid item xs={12} md={6}>
        <TextField
          field={field.city}
          variant="outlined"
          onChange={onInputChange(field.city)}
        />
      </Grid>
      <Grid item xs={12} md={6}>
        <StatesSelect
          field={field.state}
          variant="outlined"
          onChange={onInputChange(field.state)}
        />
      </Grid>
      <Grid item xs={12} md={6}>
        <SelectField
          field={field.country}
          options={countries}
          disabled
          variant="outlined"
        />
      </Grid>
      <Grid item xs={12} md={6}>
        <TextField
          field={field.zipCode}
          variant="outlined"
          onChange={onInputChange(field.zipCode)}
        />
      </Grid>
      <Grid item xs={12} md={6}>
        <SelectField
          field={field.ethnicity}
          options={ethnicities}
          variant="outlined"
          onChange={onInputChange(field.ethnicity)}
        />
      </Grid>
      <Grid item xs={12} md={6}>
        <SelectField
          field={field.race}
          options={races}
          variant="outlined"
          onChange={onInputChange(field.race)}
        />
      </Grid>
    </Grid>
  );

  const handleShowDependentToggle = () => {
    setShowDependentForm(!showDepdenentForm);
  };

  const handleAddDependentSubmit = (fields, updateFunc) => {
    if (!fields.completed) {
      highlightFormErrors(fields, updateFunc);
      enqueueSnackbar('Please fill out all fields marked with asterisk *', {
        variant: 'error',
      });
    } else {
      const params = extractFormValues(fields);
      const { dateOfBirth, ...rest } = params;
      const formattedFarams = {
        ...rest,
        dateOfBirth,
      };
      dispatch(
        doAddDependent({
          params: formattedFarams,
          memberGroupCode: auth.data.patient.memberGroupCode,
        }),
      );
    }
  };

  const handleFamilyInviteSubmit = familyFields => {
    if (!familyFields.completed) {
      enqueueSnackbar('Please fill out all fields marked with asterisk *', {
        variant: 'error',
      });
    }
    if (familyFields.completed) {
      const params = extractFormValues(familyFields);
      dispatch(doFamilyInvite({ patientId: auth.data.patient.id, params }));
    }
  };
  const handleFamilyInviteToggle = () => {
    setShowInviteForm(!showInviteForm);
  };

  const handleMemberModalClose = () => {
    setMemberApproval({ ...memberApproval, open: false });
  };

  const handleMemberModalOpen = (member, approve) => () => {
    setMemberApproval({ open: true, member, approve });
  };

  const handleMemberApproval = () => {
    dispatch(
      doApproveFamilyMember({
        approve: memberApproval.approve ? 'approve' : 'deny',
        id: memberApproval.member.id,
      }),
    );
  };

  const renderAddDependentForm = () => (
    <AddDependentModal
      onToggle={handleShowDependentToggle}
      open={showDepdenentForm}
      onSubmit={handleAddDependentSubmit}
    />
  );

  const renderInviteMemberModal = () => (
    <InviteMemberModal
      onToggle={handleFamilyInviteToggle}
      open={showInviteForm}
      onSubmit={handleFamilyInviteSubmit}
    />
  );

  const handleFamilyMemberClick = dependent => {
    const fieldsWithValues = setValuesFrom(
      { ...dependent, gender: dependent.genderId.toString() },
      profile,
      true,
    );
    setProfile({ ...fieldsWithValues });
  };

  const pendingMembers =
    (familyMembers &&
      familyMembers.data.length &&
      familyMembers.data.filter(({ status }) => status === 'Pending')) ||
    [];

  const approvedMembers =
    (familyMembers &&
      familyMembers.data.length &&
      familyMembers.data.filter(({ status }) => status === 'Approved')) ||
    [];

  const allDependents = [...(dependents.data || []), ...approvedMembers];
  const isRegularMember =
    auth.data && auth.data.patient.membershipTypeId === MEMBER_TYPES.individual;
  const isVdpcLiteMember =
    auth.data && auth.data.patient.membershipTypeId === MEMBER_TYPES.VDPC_Lite;
  const isMedClinicMember =
    auth.data && auth.data.patient.membershipTypeId === MEMBER_TYPES.MedClinic;
  const canAddDependents =
    isRegularMember ||
    isMedClinicMember ||
    (isVdpcLiteMember &&
      [
        VDPC_LITE_PLAN_TYPES.family,
        VDPC_LITE_PLAN_TYPES.employeeWithChildren,
      ].includes(auth.data.patient.planMemberPepmType));
  const canInviteDependents =
    isRegularMember ||
    isMedClinicMember ||
    (isVdpcLiteMember &&
      [
        VDPC_LITE_PLAN_TYPES.family,
        VDPC_LITE_PLAN_TYPES.employeeWithSpouse,
      ].includes(auth.data.patient.planMemberPepmType));

  const renderDependents = () => (
    <List component="nav" dense aria-label="secondary mailbox folder">
      <ListItem
        button
        onClick={() => handleFamilyMemberClick(auth.data.patient)}
      >
        <ListItemText primary="You (Principal)" />
      </ListItem>
      {allDependents.map(dependent => (
        <ListItem
          key={`dependent-${dependent.id}`}
          button
          onClick={() => handleFamilyMemberClick(dependent)}
        >
          <ListItemText
            primary={`${dependent.firstName} ${dependent.lastName}`}
          />
        </ListItem>
      ))}
      {canAddDependents && (
        <ListItem button onClick={handleShowDependentToggle}>
          <ListItemAvatar>
            <Avatar>
              <AccessibilityIcon color="primary" />
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary="Add Dependent"
            secondary="18 yrs old and below"
          />
        </ListItem>
      )}
      {canInviteDependents && (
        <ListItem button onClick={handleFamilyInviteToggle}>
          <ListItemAvatar>
            <Avatar>
              <AccessibilityIcon color="secondary" />
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary="Add Family member"
            secondary="above 18 yrs old"
          />
        </ListItem>
      )}
    </List>
  );

  const renderFamilyMembersForApproval = () => (
    <div className={classes.familyMemberDiv}>
      <Typography variant="h6" color="primary">
        Family Members for Approval
      </Typography>
      <List component="nav" dense aria-label="secondary mailbox folder">
        {pendingMembers.map(member => (
          <ListItem
            key={`member-${member.id}`}
            button
            onClick={() => handleFamilyMemberClick(member)}
          >
            <ListItemText primary={`${member.firstName} ${member.lastName}`} />
            <ListItemSecondaryAction>
              <Tooltip title="Approve">
                <IconButton
                  color="primary"
                  edge="start"
                  onClick={handleMemberModalOpen(member, true)}
                >
                  <ApproveIcon />
                </IconButton>
              </Tooltip>
              <Tooltip title="Deny">
                <IconButton
                  color="secondary"
                  edge="end"
                  onClick={handleMemberModalOpen(member, false)}
                >
                  <DenyIcon />
                </IconButton>
              </Tooltip>
            </ListItemSecondaryAction>
          </ListItem>
        ))}
      </List>
    </div>
  );

  return (
    <Fragment>
      <Grid container spacing={2} direction="row">
        <Grid item xs={12} md={6} className={classes.formContainer}>
          <Typography variant="h6" color="primary">
            Profile Information
          </Typography>
          {renderInfo(profile)}
        </Grid>
        <Grid item xs={12} md={2}>
          <Typography variant="h6" color="primary">
            Dependents / Family members
          </Typography>
          {renderDependents()}
          {!!pendingMembers.length && renderFamilyMembersForApproval()}
        </Grid>
      </Grid>
      {showDepdenentForm && renderAddDependentForm()}
      {showInviteForm && renderInviteMemberModal()}
      <ApproveMemberModal
        {...memberApproval}
        onClose={handleMemberModalClose}
        onApprove={handleMemberApproval}
        details={approveFamilyMember}
      />
    </Fragment>
  );
}

const { object, func } = PropTypes;
MyAccount.propTypes = {
  enqueueSnackbar: func.isRequired,
  dispatch: func.isRequired,
  doLogoutUser: func.isRequired,
  doResetUpdatedPatient: func.isRequired,
  auth: object.isRequired,
  updatedPatient: object.isRequired,
  dependents: object.isRequired,
  doFamilyInvite: func.isRequired,
  familyInvite: object.isRequired,
  doResetFamilyInvite: func.isRequired,
  doAddDependent: func.isRequired,
  addDependent: object.isRequired,
  doResetAddDependent: func.isRequired,
  doGetPatientDependents: func.isRequired,
  doGetFamilyMembers: func.isRequired,
  familyMembers: object.isRequired,
  approveFamilyMember: object.isRequired,
  doApproveFamilyMember: func.isRequired,
  doClearMemberApprove: func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  auth: makeSelectVerifyAuth(),
  updatedPatient: makeSelectUpdatedPatient(),
  dependents: makePatientDependents(),
  familyInvite: makeSelectFamilyInvite(),
  addDependent: makeSelectAddDependent(),
  familyMembers: makeSelectFamilyMembers(),
  approveFamilyMember: makeSelectApproveFamilyMember(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doGetPatientDetail: getPatientDetail,
    doUpdatePatient: updatePatient,
    doLogoutUser: logoutUser,
    doResetUpdatedPatient: resetUpdatedPatient,
    doGetPatientDependents: getPatientDependents,
    doFamilyInvite: postFamilyInvite,
    doResetFamilyInvite: resetFamilyInviteData,
    doAddDependent: postAddDependent,
    doResetAddDependent: resetAddDependent,
    doGetFamilyMembers: getFamilyMembers,
    doApproveFamilyMember: putFamilyMemberApproval,
    doClearMemberApprove: resetFamilyMemberApproval,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(MyAccount);
