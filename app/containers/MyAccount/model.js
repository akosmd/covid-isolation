import { initialState, initialField } from 'utils/formHelper';

export const medicalHistoryModel = {
  medications: {
    ...initialField,
    id: 'medications',
    caption: 'Medications',
    required: false,
  },
  preExistingConditions: {
    ...initialField,
    id: 'preExistingConditions',
    caption: 'Pre-existing medical conditions',
    required: false,
  },
  allergies: {
    ...initialField,
    id: 'allergies',
    caption: 'Allergies',
    required: false,
  },
};

export const dependentModel = {
  firstName: {
    ...initialField,
    id: 'firstName',
    caption: 'First Name',
  },
  middleName: {
    ...initialField,
    id: 'firstName',
    caption: 'First Name',
  },
  lastName: {
    ...initialField,
    id: 'lastName',
    caption: 'Last Name',
  },
  birthDateAt: {
    ...initialField,
    id: 'birthDateAt',
    caption: 'Date of Birth',
    value: null,
  },

  gender: {
    ...initialField,
    id: 'gender',
    caption: 'Gender',
    variant: 'outlined',
  },
  address1: { ...initialField, id: 'address1', caption: 'Address 1' },
  address2: { ...initialField, id: 'address2', caption: 'Address 2' },
  city: { ...initialField, id: 'city', caption: 'City' },
  state: { ...initialField, id: 'state', caption: 'State' },
  country: { ...initialField, id: 'state', caption: 'Country' },
  zipCode: { ...initialField, id: 'zipCode', caption: 'Zip Code' },
  ...initialState,
};

export default {
  firstName: {
    ...initialField,
    id: 'firstName',
    caption: 'First Name',
  },
  middleName: {
    ...initialField,
    id: 'firstName',
    caption: 'First Name',
  },
  lastName: {
    ...initialField,
    id: 'lastName',
    caption: 'Last Name',
  },
  employerCode: {
    ...initialField,
    id: 'employerCode',
    caption: 'Employer Code',
    required: false,
    error: false,
  },
  birthDateAt: {
    ...initialField,
    id: 'birthDateAt',
    caption: 'Date of Birth',
    value: null,
  },
  email: {
    ...initialField,
    id: 'email',
    caption: 'Email Address',
  },
  password: {
    ...initialField,
    id: 'password',
    caption: 'Password',
    type: 'password',
  },
  confirmPassword: {
    ...initialField,
    id: 'confirmPassword',
    caption: 'Confirm Password',
    type: 'password',
  },
  phoneNumber: {
    ...initialField,
    id: 'phoneNumber',
    caption: 'Phone Number',
  },
  gender: {
    ...initialField,
    id: 'gender',
    caption: 'Gender',
    variant: 'outlined',
  },
  address1: { ...initialField, id: 'address1', caption: 'Address 1' },
  address2: { ...initialField, id: 'address2', caption: 'Address 2' },
  city: { ...initialField, id: 'city', caption: 'City' },
  state: { ...initialField, id: 'state', caption: 'State' },
  zipCode: { ...initialField, id: 'zipCode', caption: 'Zip Code' },
  country: { ...initialField, id: 'state', caption: 'Country' },
  ethnicity: {
    ...initialField,
    id: 'ethnicity',
    caption: 'Ethnicity',
  },
  race: {
    ...initialField,
    id: 'race',
    caption: 'Race',
  },
  ...initialState,
};

export const familyInviteModel = {
  firstName: {
    ...initialField,
    id: 'firstName',
    caption: 'First Name',
  },
  lastName: {
    ...initialField,
    id: 'lastName',
    caption: 'Last Name',
  },
  email: {
    ...initialField,
    id: 'email',
    caption: 'Email Address',
  },
  ...initialState,
};
