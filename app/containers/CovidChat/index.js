/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { push } from 'connected-react-router';
import { createStructuredSelector } from 'reselect';

import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import uuid from 'react-uuid';

import {
  Grid,
  CardHeader,
  Tooltip,
  IconButton,
  Menu,
  MenuItem,
} from '@material-ui/core';

import HelpIcon from '@material-ui/icons/Help';

import {
  sendCovidMessage,
  resetGiMessage,
  notifyNurse,
} from 'containers/App/actions';

import { saveFPI } from 'containers/App/legacyActions';

import {
  makeSelectCovidMessage,
  makeSelectActualPatient,
} from 'containers/App/selectors';

import { makeSelectPatientCallId } from 'containers/App/legacySelectors';
import 'typeface-lato';

import logo from 'images/akos-logo.png';

import { GOOGLE_CHAT_URL } from 'utils/config';

import MessageControl from './messageControl';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },

  grid: {
    width: '99%',
    margin: '0 auto',
    [theme.breakpoints.down('md')]: {
      height: '95vh',
    },
    [theme.breakpoints.up('md')]: {
      height: '98vh',
    },
  },
  card: {
    minWidth: 375,
    height: 500,
    minHeight: 400,
    position: 'fixed',
    bottom: theme.spacing(10),
    right: theme.spacing(2),
    [theme.breakpoints.down('md')]: {
      minWidth: '96%',
      height: '90%',
      width: '96%',
      right: theme.spacing(1),
    },
    display: 'flex',
    flexDirection: 'column',
  },

  cardFullScreen: {
    display: 'flex',
    height: '97%',
    flexDirection: 'column',
    alignContent: 'stretch',
    justifyContent: 'stretch',
  },
  cardContent: {
    minHeight: 300,
    width: '100%',
    height: '100%',
  },

  cardHeaderTitle: {
    color: theme.palette.secondary.main,
  },

  cardRoot: {
    background: 'linear-gradient(to right, #55bcbe 0%,#85c3a6 100%) !important',
  },
  cardSubHeader: {
    color: theme.palette.primary.main,
  },
  fullContent: {
    display: 'flex',
    height: '90vh',
    overflowY: 'scroll',
    padding: 0,
    paddingBottom: '60px !important',
    [theme.breakpoints.up('md')]: {
      paddingBottom: '0 !important',
    },
  },

  chatAvatar: {
    background: '#12bcc5',
  },
}));

function CovidChat({
  dispatch,
  doSendMessage,
  doResetMessage,
  onConference,
  doSaveFPI,
  doNotifyNurse,
  receivedMessage,
  actualPatient,
  callId,
  roomName,
  showHeader,
}) {
  const [message, setMessage] = useState();
  const [conversation, setConversation] = useState([]);
  const [helpAnchor, setHelpAnchor] = useState(null);
  const [sessionId, setSessionId] = useState();
  const [typing, setTyping] = useState(false);
  const [patient, setPatient] = useState();
  const classes = useStyles();
  const isMenuOpen = Boolean(helpAnchor);

  const startChat = () => {
    setConversation([
      {
        from: 'bot',
        message: `Hi  ${
          patient ? patient.firstName : ''
        }, Please answer the following questions to complete your Covid-19 assessment.`,
        options: [],
        controlId: 's',
      },
    ]);
  };

  const initializeAssessment = () => {
    if (!patient) return;
    const params = {
      query: 'covid assessment',
      userid: patient.id,
      gender: patient.genderId === 12000 ? 'Male' : 'Female',
      bot_name: 'covid19integrated',
      sessionid: `${sessionId}`,
    };
    setTyping(true);
    dispatch(doSendMessage({ params }));
    dispatch(doResetMessage());
  };

  useEffect(() => {
    setSessionId(uuid());
    dispatch(doResetMessage());
  }, []);

  useEffect(() => {
    if (actualPatient.data) {
      setPatient(actualPatient.data.patient);
    }
  }, [actualPatient]);

  useEffect(() => {
    if (patient) {
      startChat();
      initializeAssessment();
    }
  }, [patient]);

  useEffect(() => {
    if (receivedMessage && receivedMessage.data && conversation.length > 0) {
      const {
        ai_response: {
          chat,
          options,
          meta_data: { control_id: controlId },
          summary,
          response_code: responseCode,
        },
      } = receivedMessage.data;
      if (chat)
        setConversation(prevState => [
          ...prevState,
          {
            from: 'bot',
            message: chat,
            options,
            controlId,
            summary,
            responseCode,
          },
        ]);
      if (summary !== '' && responseCode && responseCode !== 0) {
        setConversation(prevState => [
          ...prevState,
          {
            from: 'bot',
            message: '',
            options: ['video conference now'],
            controlId: 's',
            summary,
            responseCode,
          },
        ]);
        onNotifyNurse(summary);
      }
      setTyping(false);
    }
  }, [receivedMessage]);
  const onChange = e => {
    const { value } = e.target;

    setMessage(value);
  };

  const onSubmit = () => {
    if (message && message !== '') {
      setConversation(prevState => [...prevState, { from: 'user', message }]);
      const params = {
        query: message,
        userid: patient.id,
        gender: patient.genderId === 12000 ? 'Male' : 'Female',
        bot_name: 'covid19integrated',
        sessionid: `${sessionId}`,
      };
      setTyping(true);
      dispatch(doSendMessage({ params }));
      dispatch(doResetMessage());
      setMessage('');
    }
  };
  const onResponseSelected = response => {
    if (response && response !== '') {
      setConversation(prevState => [
        ...prevState,
        { from: 'user', message: response },
      ]);
      const params = {
        query: response,
        userid: patient.id,
        gender: patient.genderId === 12000 ? 'Male' : 'Female',
        bot_name: 'covid19integrated',
        sessionid: `${sessionId}`,
      };
      setTyping(true);
      dispatch(doSendMessage({ params }));
      dispatch(doResetMessage());
      setMessage('');
    }
  };
  const handleHelpOpen = event => {
    setHelpAnchor(event.target);
  };

  const handleQuit = () => {
    const params = {
      query: 'exit',
      userid: patient.id,
      gender: patient.genderId === 12000 ? 'Male' : 'Female',
      bot_name: 'covid19integrated',
      sessionid: `${sessionId}`,
    };

    dispatch(doSendMessage({ params }));
    dispatch(push('/'));
  };

  const hanleHelpClose = () => {
    setHelpAnchor(null);
  };

  const handleStartOver = () => {
    window.location.reload();
  };

  const renderHelp = (
    <Menu
      anchorEl={helpAnchor}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={hanleHelpClose}
    >
      <MenuItem onClick={handleStartOver}>Start over</MenuItem>
      <MenuItem onClick={handleQuit}>Quit</MenuItem>
    </Menu>
  );

  const onNotifyNurse = msg => {
    dispatch(
      doNotifyNurse({
        url: GOOGLE_CHAT_URL,
        text: `Patient Name: ${patient.firstName} ${
          patient.lastName
        }\nChat Summary: ${msg}`,
      }),
    );
    doSendFPIData(msg);
  };

  const doSendFPIData = msg => {
    const formData = new FormData();
    formData.set(
      'doctorInstruction',
      JSON.stringify({
        progressNote: { hpi: msg },
        reviewofsystem: {
          Constitutional: {},
          ENT: {},
          Cardiovascular: {},
          Respiratory: {},
          Gastrointestinal: {},
          Genitourinary: {},
          Musculoskeletal: {},
          Integumentary: {},
          Neurologic: {},
          Psychiatric: {},
          Endocrine: {},
          Hematologic: {},
          AllergicImmunologic: {},
        },
        operation: 'save',
        diagnosticImpression: [],
        cpt_code: [],
        physicalExam: {},
        patientName: `${patient.firstName} ${patient.lastName}`,
        doctorFirstName: 'covid',
        doctorLastName: 'assessment',
        callId: callId.data.result.call_id,
        patientEmail: patient.email,
        patientId: patient.patientLegacyId,
      }),
    );
    formData.set('operation', 'save');
    formData.set('patientEmail', patient.email);
    formData.set('callId', callId.data.result.call_id);
    formData.set('patientId', patient.patientLegacyId);

    dispatch(doSaveFPI(formData));
  };

  return (
    <div className={classes.root}>
      <Grid
        container
        direction="row"
        justify="space-around"
        alignItems="stretch"
        className={classes.grid}
      >
        <Grid item xs={12}>
          <Card elevation={1} className={classes.cardFullScreen}>
            {showHeader && (
              <CardHeader
                className={classes.cardRoot}
                subheader={
                  <div style={{ display: 'flex' }}>
                    <figure
                      style={{
                        display: 'flex',
                        width: 210,
                        height: 35,
                        margin: '0 10px',
                      }}
                    >
                      <img src={logo} alt="Akos" style={{ width: '100%' }} />
                    </figure>
                  </div>
                }
                classes={{
                  title: classes.cardHeaderTitle,
                  subheader: classes.cardSubHeader,
                }}
                action={
                  <div>
                    <Tooltip title="Help">
                      <IconButton
                        aria-label="Help"
                        color="secondary"
                        onClick={handleHelpOpen}
                      >
                        <HelpIcon />
                      </IconButton>
                    </Tooltip>

                    {renderHelp}
                  </div>
                }
              />
            )}
            {!showHeader && (
              <CardHeader
                action={
                  <div>
                    <Tooltip title="Help">
                      <IconButton
                        aria-label="Help"
                        color="secondary"
                        onClick={handleHelpOpen}
                      >
                        <HelpIcon />
                      </IconButton>
                    </Tooltip>

                    {renderHelp}
                  </div>
                }
              />
            )}
            <CardContent className={classes.fullContent}>
              <MessageControl
                onSendMessage={onSubmit}
                message={message}
                dispatch={dispatch}
                push={push}
                onChange={onChange}
                onResponse={onResponseSelected}
                onConversationEnd={onNotifyNurse}
                messages={conversation}
                typing={typing}
                user="user"
                roomName={roomName}
                onConference={onConference}
                uuid={actualPatient.data.patient.uuid}
              />
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </div>
  );
}

const { object, func, string, bool } = PropTypes;
CovidChat.propTypes = {
  dispatch: func.isRequired,
  receivedMessage: object.isRequired,
  actualPatient: object.isRequired,
  doSendMessage: func.isRequired,
  doResetMessage: func.isRequired,
  doNotifyNurse: func.isRequired,
  doSaveFPI: func.isRequired,
  onConference: func.isRequired,
  callId: object.isRequired,
  roomName: string,
  showHeader: bool,
};

CovidChat.defaultProps = {
  roomName: 'agileurgentcare',
  showHeader: true,
};
const mapStateToProps = createStructuredSelector({
  receivedMessage: makeSelectCovidMessage(),
  actualPatient: makeSelectActualPatient(),
  callId: makeSelectPatientCallId(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doSendMessage: sendCovidMessage,
    doResetMessage: resetGiMessage,
    doNotifyNurse: notifyNurse,
    doSaveFPI: saveFPI,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(CovidChat);
