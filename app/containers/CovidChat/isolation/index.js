import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Typography, Button } from '@material-ui/core';

import IsolationChat from 'containers/CovidChat/isolation/chat';
import { makeStyles } from '@material-ui/core/styles';
import Instructions from './instructions';

const useStyles = makeStyles(() => ({
  para: {
    margin: '1rem 0',
  },
  header: {
    margin: '1rem 0',
  },
  index: {
    margin: '0 2rem',
  },
  subIndex: {
    margin: '0 4rem',
  },
}));
function Consent({ onClick }) {
  const classes = useStyles();
  return (
    <div>
      <Typography variant="h6">Consent</Typography>
      <Typography className={classes.para}>
        During the next 14-days you should minimize physical contact with the
        “outside world” as much as possible. We will guide you with daily
        assessments and offer you the option to speak with a healthcare
        professional at any time. By proceeding, you’re giving us consent to
        monitor your health status and offer necessary intervention.
      </Typography>
      <Button onClick={onClick} variant="contained" color="primary">
        Continue to Questionnaire
      </Button>
    </div>
  );
}

Consent.propTypes = {
  onClick: PropTypes.func.isRequired,
};
function Isolation() {
  const [showConsent, setShowConsent] = useState(false);
  const [showChat, setShowChat] = useState(false);
  const [showInstructions, setShowInstructions] = useState(false);
  const classes = useStyles();

  const handleNext = () => {
    setShowConsent(!showConsent);
  };

  const handleShowInstructions = () => {
    setShowConsent(false);
    setShowInstructions(!showInstructions);
  };
  const handleDoChat = () => {
    setShowConsent(false);
    setShowInstructions(false);
    setShowChat(!showChat);
  };

  if (showConsent) return <Consent onClick={handleDoChat} />;

  if (showInstructions)
    return <Instructions classes={classes} onClick={handleNext} />;

  if (showChat) return <IsolationChat showHeader={false} />;

  return (
    <div>
      <Typography className={classes.para}>
        We’re sorry to learn that you were diagnosed with COVID-19. We hope you
        will remain healthy and would like to offer assistance with monitoring
        your progress while you embark on this unplanned journey for the next 2
        weeks.{' '}
      </Typography>
      <Typography className={classes.para}>
        The recommended self-isolation period by the CDC for persons who tested
        positive for the COVID-19 is 14 days. If you develop no symptoms during
        the 14 days, or only developed mild symptoms and 7 days have passed
        since the onset of the symptoms, you may terminate the self-isolation.
        If you develop mild symptoms towards the end of 14 days, self-isolation
        needs to continue until your symptoms are mostly resolved, you no longer
        have a fever without medications for 72 hours (3 full days), and at
        least 7 days have passed since you developed first symptoms.
        Alternatively, if you have access to further testing, 2 negative tests
        24 hours apart are necessary in order to be considered non-infectious
        and terminate the self-isolation (more to follow). If you develop
        significant symptoms during this period, please call our Nurse Hotline
        ASAP for further guidance as you may need prompt medical attention.
      </Typography>
      <Typography className={classes.para}>
        We encourage you to maintain a healthy routine which is explained in
        detail in the instructions that follow. Adhering to a daily routine
        helps us keep “sanity” and makes it easier to adhere to healthy habits.
        At any time, should there be questions or concerns, we encourage you to
        call our Nurse Hotline and our experienced nurses will answer your
        questions and, if necessary, will bring a provider who can offer you
        further guidance and perspective on how your condition is likely to
        progress.{' '}
      </Typography>
      <Typography className={classes.para}>
        If you and your significant other are both positive, you don’t need to
        worry about passing it to each other, therefore, you can behave normally
        between the two but follow the precautions with others as described
        below.
      </Typography>
      <Typography className={classes.para}>Stay Healthy!</Typography>
      <Button
        onClick={handleShowInstructions}
        variant="contained"
        color="primary"
      >
        Continue
      </Button>
    </div>
  );
}

export default Isolation;
