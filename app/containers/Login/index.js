/**
 *
 * Login
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { push } from 'connected-react-router';

import { useCookies } from 'react-cookie';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';

import KeyboardEventHandler from 'react-keyboard-event-handler';

import { Grid, Typography, CircularProgress } from '@material-ui/core';
import Link from '@material-ui/core/Link';
import TextField from 'components/TextField';
import GradientButton from 'components/GradientButton';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import WarningIcon from '@material-ui/icons/Warning';

import {
  signIn,
  resetAuthFields,
  setNotificationType,
  resetSignin,
} from 'containers/App/actions';
import { makeSelectSignin } from 'containers/App/selectors';

import {
  initialField,
  initialState,
  handleChange,
  highlightFormErrors,
  extractFormValues,
  formatPhone,
} from 'utils/formHelper';

import { INXITE_SSO_URL } from 'utils/config';
import { FORGOT_PASS_URL } from 'utils/constants';

const employeeValidator = {
  ...initialField,
  id: 'employeeValidator',
  caption: 'Phone Number or Email',
  errorMessage: 'Phone Number or Email is required',
};
const password = {
  ...initialField,
  id: 'password',
  caption: 'Password',
};
export function Login({
  dispatch,
  signin,
  doPatientSignin,
  doResetAuthFields,
  enqueueSnackbar,
  doSetNotificationType,
  doResetSignin,
}) {
  // eslint-disable-next-line no-unused-vars
  const [cookies, setCookie] = useCookies(['name']);
  const [login, setLogin] = useState({
    employeeValidator,
    password,
    ...initialState,
  });
  const [browserError, setBrowserError] = useState(false);

  useEffect(() => {
    // eslint-disable-next-line no-useless-escape
    const isSafari = !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);
    const isIOS = /(iPhone|iPod|iPad)/i.test(navigator.platform);

    if (isIOS && !isSafari) {
      setBrowserError(true);
    }

    if (
      isSafari &&
      !document.cookie.match(/^(.*;)?\s*fixed\s*=\s*[^;]+(.*)?$/)
    ) {
      setCookie('fixed', 'fixed', {
        path: '/',
        expires: new Date('Tue, 19 Jan 2038 03:14:07 UTC'),
      });
      window.location.replace(`${INXITE_SSO_URL}safari/iframe`);
    }
  }, []);

  useEffect(() => {
    if (signin.error && signin.error.status === 401) {
      enqueueSnackbar('Invalid Phone Number or Email or Password', {
        variant: 'error',
      });
      dispatch(doResetSignin());
    } else if (signin.data) {
      dispatch(push('/2fa'));
      dispatch(doResetAuthFields());
    }
  }, [signin]);

  const onInputChange = field => event => {
    handleChange({
      field: field.id,
      state: login,
      event,
      saveStepFunc: setLogin,
    });
  };

  const handleSubmit = () => {
    if (!login.completed) {
      enqueueSnackbar('Please input your Email and Password', {
        variant: 'error',
      });
      highlightFormErrors(login, setLogin);
    }
    if (login.completed) {
      const params = extractFormValues(login);
      const isEmail = params.employeeValidator.includes('@');
      dispatch(doSetNotificationType(isEmail));
      dispatch(
        doPatientSignin({
          ...params,
          employeeValidator: parseInt(params.employeeValidator, 10)
            ? formatPhone(params.employeeValidator)
            : params.employeeValidator,
        }),
      );
    }
  };

  const renderAlert = () => (
    <Dialog
      open={browserError}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">Unsupported Browser</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          <WarningIcon color="error" /> <strong>Warning! </strong>Unfortunately
          the browser you are using is currently not supported. We only support
          latest Safari browser at this time, please open the link using Safari.
        </DialogContentText>
      </DialogContent>
    </Dialog>
  );

  if (browserError) return renderAlert();

  return (
    <div>
      <Grid container spacing={2} direction="column">
        <Grid item xs={12}>
          <Typography variant="h4" color="primary">
            Sign In
            <Typography variant="body1" color="textPrimary">
              Sign in with your registered phone number or email address and
              password
            </Typography>
          </Typography>
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          <Grid container spacing={1}>
            <Grid item xs={12} sm={12} md={8}>
              <KeyboardEventHandler
                handleKeys={['enter']}
                onKeyEvent={handleSubmit}
              >
                <TextField
                  field={login.employeeValidator}
                  variant="outlined"
                  onChange={onInputChange(login.employeeValidator)}
                />
              </KeyboardEventHandler>
            </Grid>

            <Grid item xs={12} sm={12} md={8}>
              <KeyboardEventHandler
                handleKeys={['enter']}
                onKeyEvent={handleSubmit}
              >
                <TextField
                  field={login.password}
                  type="password"
                  variant="outlined"
                  onChange={onInputChange(login.password)}
                />
              </KeyboardEventHandler>
            </Grid>
          </Grid>
        </Grid>

        <Grid item xs={12} md={6}>
          <Link href={FORGOT_PASS_URL} color="secondary">
            Forgot Password
          </Link>
        </Grid>
        <Grid item xs={12} md={3}>
          <GradientButton
            variant="contained"
            size="large"
            onClick={handleSubmit}
          >
            {signin.loading && <CircularProgress color="secondary" />}
            {!signin.loading && 'Proceed'}
          </GradientButton>
        </Grid>
      </Grid>
      <iframe
        id="inxiteFrame"
        title="Inxite"
        src={`${INXITE_SSO_URL}logout`}
        style={{ display: 'none' }}
      />
    </div>
  );
}

const { func, object } = PropTypes;

Login.propTypes = {
  dispatch: func.isRequired,
  doPatientSignin: func.isRequired,
  doResetAuthFields: func.isRequired,
  signin: object,
  enqueueSnackbar: func.isRequired,
  doSetNotificationType: func.isRequired,
  doResetSignin: func.isRequired,
};
const mapStateToProps = createStructuredSelector({
  signin: makeSelectSignin(),
});
function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doPatientSignin: signIn,
    doResetAuthFields: resetAuthFields,
    doSetNotificationType: setNotificationType,
    doResetSignin: resetSignin,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Login);
