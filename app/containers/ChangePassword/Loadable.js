/**
 *
 * Asynchronously loads the component for Change Passwo
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
