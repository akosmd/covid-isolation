import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Helmet } from 'react-helmet';

import {
  changePassword as changePasswordAction,
  resetChangePassword,
  logoutUser,
} from 'containers/App/actions';
import {
  makeSelectSignin,
  makeSelectChangePassword,
} from 'containers/App/selectors';

import TextField from 'components/TextField';
import GradientButton from 'components/GradientButton';

import {
  initialState,
  initialField,
  handleChange,
  handleEqualityChange,
  highlightFormErrors,
} from 'utils/formHelper';

const defaultState = {
  currentPassword: {
    ...initialField,
    id: 'currentPassword',
    caption: 'Current Password',
  },
  newPassword: { ...initialField, id: 'newPassword', caption: 'New Password' },
  confirmPassword: {
    ...initialField,
    id: 'confirmPassword',
    caption: 'Confirm Password',
  },
  samePassword: false,
  ...initialState,
};

function ChangePassword(props) {
  const {
    changePasswordDetails,
    enqueueSnackbar,
    dispatch,
    doPasswordChange,
    userInfo,
    doResetChangePassword,
    doLogOutUser,
  } = props;

  const [changePassword, setChangePassword] = useState({ ...defaultState });

  useEffect(() => {
    if (
      changePasswordDetails.error &&
      changePasswordDetails.error.status === 403
    ) {
      enqueueSnackbar(changePasswordDetails.error.data, {
        variant: 'error',
      });
      dispatch(doResetChangePassword());
    } else if (changePasswordDetails.data) {
      enqueueSnackbar('Your password has been successfully updated', {
        variant: 'success',
      });

      dispatch(doResetChangePassword());
      dispatch(doLogOutUser());
    }
  }, [changePasswordDetails]);

  const onInputChange = field => event => {
    handleChange({
      field: field.id,
      state: changePassword,
      event,
      saveStepFunc: setChangePassword,
    });
  };

  const onPasswordChange = () => event => {
    handleEqualityChange({
      field1: changePassword.newPassword,
      field2: changePassword.confirmPassword,
      state: changePassword,
      event,
      saveStepFunc: setChangePassword,
    });
  };

  const handleChangePassword = () => {
    const {
      currentPassword,
      newPassword,
      confirmPassword,
      completed,
    } = changePassword;

    if (!completed) {
      enqueueSnackbar(
        'Please fill out all required fields marked with asterisk *',
        {
          variant: 'error',
        },
      );
      highlightFormErrors(changePassword, setChangePassword);
    } else if (
      currentPassword.value === confirmPassword.value &&
      currentPassword.value === newPassword.value
    ) {
      enqueueSnackbar(
        'Current password and New Password should not be the same',
        {
          variant: 'error',
        },
      );
    } else {
      dispatch(
        doPasswordChange({
          userId: userInfo.data.patient.id,
          oldPassword: currentPassword.value,
          newPassword: newPassword.value,
          newPasswordConfirmation: confirmPassword.value,
        }),
      );
    }
  };

  return (
    <>
      <Helmet>
        <title>Change Password</title>
        <meta name="description" content="Change Password" />
      </Helmet>
      <Grid container direction="column" spacing={2}>
        <Grid item xs={12}>
          <Typography variant="h4" color="primary">
            Change Password
            <Typography variant="body1" color="textPrimary">
              Input your current Password and new Password below
            </Typography>
          </Typography>
        </Grid>
        <Grid item xs={12} md={3}>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                fullWidth
                field={changePassword.currentPassword}
                onChange={onInputChange(changePassword.currentPassword)}
                type="password"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                field={changePassword.newPassword}
                onChange={onPasswordChange(changePassword.newPassword)}
                type="password"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                field={changePassword.confirmPassword}
                onChange={onPasswordChange(changePassword.confirmPassword)}
                type="password"
              />
            </Grid>
            <Grid item xs={12}>
              <GradientButton
                variant="contained"
                size="large"
                onClick={handleChangePassword}
              >
                {changePasswordDetails.loading ? (
                  <CircularProgress color="secondary" />
                ) : (
                  'Change my Password'
                )}
              </GradientButton>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}

const { func, object } = PropTypes;

ChangePassword.propTypes = {
  dispatch: func.isRequired,
  enqueueSnackbar: func.isRequired,
  doPasswordChange: func.isRequired,
  changePasswordDetails: object.isRequired,
  userInfo: object.isRequired,
  doResetChangePassword: func.isRequired,
  doLogOutUser: func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  changePasswordDetails: makeSelectChangePassword(),
  userInfo: makeSelectSignin(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doPasswordChange: changePasswordAction,
    doResetChangePassword: resetChangePassword,
    doLogOutUser: logoutUser,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(ChangePassword);
