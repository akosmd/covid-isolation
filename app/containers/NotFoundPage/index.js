/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 *
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Typography } from '@material-ui/core';

import messages from './messages';

export default function NotFound() {
  return (
    <div>
      <Typography align="center" variant="h1">
        <FormattedMessage {...messages.header} />
      </Typography>
      <Typography align="center" variant="h4">
        The page you are trying to access cannot be found.
        <br />
        Click <a href="/">here</a> to go back to the Home Page
      </Typography>
    </div>
  );
}
