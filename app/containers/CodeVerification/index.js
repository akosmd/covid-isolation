/**
 *
 * Login
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';

import KeyboardEventHandler from 'react-keyboard-event-handler';
import { createStructuredSelector } from 'reselect';

import { compose } from 'redux';
import { Grid, Typography, CircularProgress } from '@material-ui/core';
import Link from '@material-ui/core/Link';
import MyLink from 'components/MyLink';
import TextField from 'components/TextField';
import GradientButton from 'components/GradientButton';

import {
  verifyAuthentication,
  reSendVerificationCode,
  resetVerificationCode,
} from 'containers/App/actions';

import {
  makeAccountVerification,
  makeSelectVerifyAuth,
  makeSelectVerificationCode,
  makeIsSentToEmail,
} from 'containers/App/selectors';

export function CodeVerification({
  dispatch,
  doVerifyAuth,
  doResendVerificationCode,
  doResetVerificationCode,
  verificationCode,
  account,
  auth,
  isSentToEmail,

  enqueueSnackbar,
}) {
  const [code, setCode] = useState('');

  useEffect(() => {
    if (!account.data) {
      dispatch(push('/login'));
    }
  }, [account]);

  useEffect(() => {
    if (verificationCode.data) {
      let message = `New Authentication code sent to your email ${
        account.data.member.email
      }`;
      if (!isSentToEmail.data)
        message = `New Authentication code sent to your Phone ${
          account.data.member.phone
        }`;

      enqueueSnackbar(message, {
        variant: 'success',
      });
      dispatch(doResetVerificationCode());
    }
  }, [verificationCode, isSentToEmail]);

  useEffect(() => {
    if (auth.error && auth.error.status === 403) {
      enqueueSnackbar('Invalid Authentication Code', {
        variant: 'error',
      });
    }
  }, [auth]);

  useEffect(() => {
    if (auth.data && auth.data.patient) {
      dispatch(push('/info'));
    }
  }, [auth]);
  const handleVerify = () => {
    if (!code || code === '') {
      enqueueSnackbar('Please provide 6 digit Authentication code', {
        variant: 'error',
      });
    } else
      dispatch(
        doVerifyAuth({
          patientId: account.data.member.id,
          verificationCode: code,
        }),
      );
  };
  const onCodeChange = e => {
    const { value } = e.target;
    setCode(value);
  };

  const handleResendCode = () => {
    dispatch(
      doResendVerificationCode({
        employeeValidator: account.data.member.email,
        employerCode: account.data.member.employer_code,
      }),
    );
  };

  const sentTo = isSentToEmail.data
    ? account.data.member.email
    : account.data.member.phone;

  return (
    <div>
      <Grid container spacing={2} direction="column">
        <Grid item xs={12}>
          <Typography variant="h4" color="primary">
            Code Verification
            <Typography variant="body1" color="textPrimary">
              For your security, please enter the <strong>6-digit</strong>{' '}
              verfication code sent to <strong>{sentTo}</strong>
            </Typography>
          </Typography>
        </Grid>
        <Grid item xs={12} md={3} style={{ width: '100%' }}>
          <KeyboardEventHandler
            handleKeys={['enter']}
            onKeyEvent={handleVerify}
          >
            <TextField
              field={{
                id: 'verificationCode',
                caption: 'Enter Verification Code',
                required: true,
                pristine: true,
                error: true,
                fullWidth: true,
              }}
              variant="outlined"
              onChange={onCodeChange}
            />
          </KeyboardEventHandler>
        </Grid>
        <Grid item xs={12} md={3}>
          <Typography variant="body1">
            Did not receive the code?{' '}
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <Link
              onClick={handleResendCode}
              component={MyLink}
              variant="body1"
              color="secondary"
              to="#"
            >
              <strong>Resend Code</strong>
            </Link>
          </Typography>
        </Grid>
        <Grid item xs={12} md={3}>
          <GradientButton
            variant="contained"
            size="large"
            onClick={handleVerify}
          >
            {auth.loading && <CircularProgress color="secondary" />}
            {!auth.loading && 'Proceed'}
          </GradientButton>
        </Grid>
      </Grid>
    </div>
  );
}

const { func, object } = PropTypes;
CodeVerification.propTypes = {
  account: object.isRequired,
  isSentToEmail: object.isRequired,
  auth: object.isRequired,
  verificationCode: object.isRequired,

  dispatch: func.isRequired,
  enqueueSnackbar: func.isRequired,
  doVerifyAuth: func.isRequired,
  doResendVerificationCode: func.isRequired,
  doResetVerificationCode: func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  account: makeAccountVerification(),
  auth: makeSelectVerifyAuth(),
  verificationCode: makeSelectVerificationCode(),
  isSentToEmail: makeIsSentToEmail(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doVerifyAuth: verifyAuthentication,
    doResendVerificationCode: reSendVerificationCode,
    doResetVerificationCode: resetVerificationCode,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(CodeVerification);
