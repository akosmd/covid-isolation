/* eslint-disable camelcase */
import React, { useState, useEffect } from 'react';
import ImageUploader from 'components/ImageUploader';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import moment from 'moment';
import ImageViewer from 'components/ImageViewer';
import DialogBox from 'components/DialogBox';
import WebCam from 'components/WebCam';

import { createStructuredSelector } from 'reselect';
import { Grid, Typography, CircularProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import TextField from 'components/TextField';
import GradientButton from 'components/GradientButton';
import DatePickerField from 'components/DatePickerField';

import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';

import {
  handleChange,
  highlightFormErrors,
  handleDateChange,
  setValuesFrom,
  extractFormValues,
  checkStatus,
} from 'utils/formHelper';

import {
  getInsuranceEligibility,
  getPatientInsurance,
  findInsurance,
  resetEligibilityCodes,
  setPaymentAmount,
  savePatientInsurance,
  saveInsuraceToPs,
  savePatientToHG,
} from 'containers/App/legacyActions';

import {
  setFrontIdDocument,
  setBackIdDocument,
  setFrontInsuranceDocument,
  setBackInsuranceDocument,
  saveIdDocument,
  getSavedImages,
} from 'containers/App/actions';

import {
  makeSelectInsuranceEligibility,
  makeSelectPatientInsurance,
  makeSelectInsuranceList,
  makeSelectPaymentAmount,
  makeSelectSavedPatientInsurance,
} from 'containers/App/legacySelectors';

import {
  makeSelectIdImage,
  makeSelectInsuranceImage,
  makeSelectGetSavedImages,
} from 'containers/App/selectors';

import { covidInsuranceDetails } from './model';

const useStyles = makeStyles({
  formContainer: {
    width: '100%',
  },
  loginLink: {
    marginLeft: '1rem',
  },
  error: {
    border: '2px solid red',
    borderRadius: '5px',
    padding: '1rem',
  },
});

export function Insurance({
  dispatch,
  patient,
  amount,
  savedImages,
  savedInsurance,
  insuranceList,
  enqueueSnackbar,
  insuranceEligibility,
  patientInsurance,
  doSaveIdDocument,
  doGetPatientInsurance,
  doSavePatientInsurance,
  doSavePatientToHG,
  doSetPaymentAmount,
  onConferenceClick,
  doResetEligibilityCodes,
  doSaveInsurancetoPs,
  doGetSavedImages,
  buttonCaption,
}) {
  const [insurance, setInsurance] = useState({ ...covidInsuranceDetails });

  const [openCam, setOpenCam] = useState({
    status: false,
    Action: false,
    title: '',
  });

  const showPayment = false;
  const [insuranceData, setInsuranceData] = useState([]);
  const classes = useStyles();

  useEffect(() => {
    dispatch(doGetPatientInsurance(patient.patientLegacyId));
    dispatch(doResetEligibilityCodes());
    dispatch(doGetSavedImages(patient.id));
  }, []);

  useEffect(() => {
    if (patientInsurance.data && patientInsurance.data.code === 200) {
      const {
        member_id: memberId,
        group_number: groupNumber,
        payer_first_name: firstName,
        payer_last_name: lastName,
        dateofbirth,
        insurance_provider,
      } = patientInsurance.data.result;
      setInsurance(prevState => {
        const newState = {
          ...prevState,
          provider: {
            ...insurance.provider,
            value: insurance_provider,
            error: false,
          },
          providerId: {
            ...insurance.providerId,
            value: insurance_provider,
          },
          memberId: { ...insurance.memberId, value: memberId, error: false },
          groupNumber: {
            ...insurance.groupNumber,
            value: groupNumber,
            error: false,
          },
          firstName: {
            ...insurance.firstName,
            value: firstName,
            error: false,
          },
          lastName: {
            ...insurance.lastName,
            value: lastName,
            error: false,
          },
          birthDateAt: {
            ...insurance.birthDateAt,
            value: dateofbirth,
            error: false,
          },
        };
        const formCompleted = !checkStatus({
          source: newState,
          currentField: '',
        });

        return {
          ...newState,
          completed: formCompleted,
        };
      });
      // savePatientToHg();
      dispatch(doSetPaymentAmount(parseFloat(amount)));
    }
  }, [patientInsurance, insuranceData]);

  useEffect(() => {
    if (
      (patient &&
        (patientInsurance.data && patientInsurance.data.code !== 200)) ||
      !patientInsurance.data
    ) {
      const fieldsWithValues = setValuesFrom(patient, insurance);

      setInsurance({
        ...fieldsWithValues,
      });
    }
  }, [patient]);

  useEffect(() => {
    if (insuranceList.data && insuranceList.data.result) {
      if (!insuranceList.data.result.msg)
        setInsuranceData(insuranceList.data.result);
    }
  }, [insuranceList]);

  useEffect(() => {
    if (savedInsurance.data) {
      dispatch(doSaveInsurancetoPs({ patientId: patient.uuid }));

      onConferenceClick();
    }
  }, [savedInsurance]);

  useEffect(() => {
    if (
      savedInsurance.data &&
      (patientInsurance.data && patientInsurance.data.code === 404)
    ) {
      const formValues = extractFormValues(insurance);

      const { birthDateAt, firstName, lastName } = formValues;

      const params = {
        firstName,
        lastName,
        gender: patient.genderId === 12000 ? 'male' : 'female',
        birthDate: moment(birthDateAt).format('YYYY-MM-DD'),
        identifier: patient.patientLegacyId,
        email: patient.email,
      };
      dispatch(doSavePatientToHG(params));
    }
  }, [patientInsurance, savedInsurance]);

  useEffect(() => {
    if (savedImages.data) {
      const {
        front_insurance_card_path,
        back_insurance_card,
        front_photo_id,
        back_photo_id,
      } = savedImages.data;

      setInsurance(prevState => {
        const newState = {
          ...prevState,
          insuranceImageFront: {
            ...insurance.insuranceImageFront,
            base64: front_insurance_card_path,
            error: false,
          },
          insuranceImageBack: {
            ...insurance.insuranceImageBack,
            base64: back_insurance_card,
            error: false,
          },
          idImageFront: {
            ...insurance.idImageFront,
            base64: front_photo_id,
            error: false,
          },
          idImageBack: {
            ...insurance.idImageBack,
            base64: back_photo_id,
            error: false,
          },
        };
        const formCompleted = !checkStatus({
          source: newState,
          currentField: '',
        });
        return {
          ...newState,
          completed: formCompleted,
        };
      });
    }
  }, [savedImages]);

  const handleSaveInsurance = () => {
    if (!insurance.completed) {
      highlightFormErrors(insurance, setInsurance);
      enqueueSnackbar('Please fill out all fields marked with asterisk *', {
        variant: 'error',
      });
    } else {
      saveImages();
      saveInsuranceDetails();
    }
  };

  const saveImages = () => {
    const { uuid, id } = patient;
    const payload = [];
    const {
      idImageFront,
      idImageBack,
      insuranceImageFront,
      insuranceImageBack,
    } = extractFormValues(insurance);

    if (idImageFront.base64 && !idImageFront.base64.includes('https://')) {
      payload.push({
        patientUuid: uuid,
        documentType: 'ID',
        title: `front_photo_id`,
        dataType: 'JPG/PDF',
        filename: 'front_id_card.jpg',
        data: idImageFront.base64,
      });
    }
    if (idImageBack.base64 && !idImageBack.base64.includes('https://')) {
      payload.push({
        patientUuid: uuid,
        documentType: 'ID',
        title: `back_photo_id`,
        dataType: 'JPG/PDF',
        filename: 'id_back_card.jpg',
        data: idImageBack.base64,
      });
    }
    if (
      insuranceImageFront.base64 &&
      !insuranceImageFront.base64.includes('https://')
    ) {
      payload.push({
        patientUuid: uuid,
        documentType: 'INSURANCE',
        title: `front_insurance_card`,
        dataType: 'JPG/PDF',
        filename: 'front_INSURANCE_card.jpg',
        data: insuranceImageFront.base64,
      });
    }
    if (
      insuranceImageBack.base64 &&
      !insuranceImageBack.base64.includes('https://')
    ) {
      payload.push({
        patientUuid: uuid,
        documentType: 'INSURANCE',
        title: `back_insurance_card`,
        dataType: 'JPG/PDF',
        filename: 'INSURANCE_back_card.jpg',
        data: insuranceImageBack.base64,
      });
    }

    const params = {
      id,
      payload,
    };
    if (payload.length > 0) dispatch(doSaveIdDocument(params));
  };
  const saveInsuranceDetails = () => {
    const formValues = extractFormValues(insurance);

    const {
      memberId,
      groupNumber,
      birthDateAt,
      firstName,
      lastName,
      provider,
      providerId,
    } = formValues;

    const formData = new FormData();
    formData.set('patient_id', patient.patientLegacyId);
    formData.set('plan_name', '');
    formData.set('payer_first_name', firstName);
    formData.set('payer_last_name', lastName);
    formData.set('member_id', memberId);
    formData.set('group_number', groupNumber);
    formData.set('effective_from_date', '');
    formData.set('effective_to_date', '');
    formData.set('dateofbirth', moment(birthDateAt).format('MM/DD/YYYY'));
    formData.set('insurance_provider', provider);
    formData.set('trading_partner_id', providerId);
    formData.set('transaction_id', insuranceEligibility.data.transactionId);

    dispatch(doSavePatientInsurance(formData));
    // dispatch(doResetEligibilityCodes());
  };
  const onInputChange = field => event => {
    handleChange({
      field: field.id,
      state: insurance,
      event,
      saveStepFunc: setInsurance,
    });
  };
  const onDateChange = field => value => {
    handleDateChange({
      field: field.id,
      state: insurance,
      value,
      saveStepFunc: setInsurance,
    });
  };

  const handleSetInsuranceImageFront = val => {
    const error = val === undefined;
    const formCompleted = !(
      error ||
      checkStatus({
        source: insurance,
        currentField: 'insuranceImageFront',
      })
    );
    setInsurance({
      ...insurance,
      insuranceImageFront: {
        ...insurance.insuranceImageFront,
        base64: val || undefined,
        filename: val ? `insurangeImageFront-${Date.now()}.png` : undefined,
        filetype: val ? 'image/png' : undefined,
        pristine: false,
        error,
      },
      completed: formCompleted,
    });
  };
  const handleSetInsuranceImageBack = val => {
    const error = val === undefined;
    const formCompleted = !(
      error ||
      checkStatus({
        source: insurance,
        currentField: 'insuranceImageBack',
      })
    );
    setInsurance({
      ...insurance,
      insuranceImageBack: {
        ...insurance.insuranceImageBack,
        base64: val || undefined,
        filename: val ? `insurangeImageBack-${Date.now()}.png` : undefined,
        filetype: val ? 'image/png' : undefined,
        pristine: false,
        error,
      },
      completed: formCompleted,
    });
  };

  const handleSetIdIFront = val => {
    const error = val === undefined;
    const formCompleted = !(
      error ||
      checkStatus({
        source: insurance,
        currentField: 'idImageFront',
      })
    );
    setInsurance({
      ...insurance,
      idImageFront: {
        ...insurance.idImageFront,
        base64: val || undefined,
        filename: val ? `idImageFront-${Date.now()}.png` : undefined,
        filetype: val ? 'image/png' : undefined,
        pristine: false,
        error,
      },
      completed: formCompleted,
    });
  };
  const handleSetIdBack = val => {
    const error = val === undefined;
    const formCompleted = !(
      error ||
      checkStatus({
        source: insurance,
        currentField: 'idImageBack',
      })
    );
    setInsurance({
      ...insurance,
      idImageBack: {
        ...insurance.idImageBack,
        base64: val || undefined,
        filename: val ? `IdImageBack-${Date.now()}.png` : undefined,
        filetype: val ? 'image/png' : undefined,
        pristine: false,
        error,
      },
      completed: formCompleted,
    });
  };

  return (
    <div>
      {!showPayment && (
        <Grid container spacing={2} direction="column">
          <Grid item xs={12}>
            <Typography variant="h4" color="primary">
              Insurance Details
            </Typography>
          </Grid>
          <Grid item xs={12} className={classes.formContainer}>
            <Grid container spacing={3} justify="center" alignItems="center">
              <Grid item xs={12} md={6}>
                <TextField
                  field={insurance.provider}
                  variant="outlined"
                  loading={patientInsurance.loading}
                  onChange={onInputChange(insurance.provider)}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={insurance.firstName}
                  variant="outlined"
                  loading={patientInsurance.loading}
                  onChange={onInputChange(insurance.firstName)}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={insurance.lastName}
                  variant="outlined"
                  loading={patientInsurance.loading}
                  onChange={onInputChange(insurance.lastName)}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={insurance.memberId}
                  variant="outlined"
                  loading={patientInsurance.loading}
                  focus
                  onChange={onInputChange(insurance.memberId)}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={insurance.groupNumber}
                  variant="outlined"
                  loading={patientInsurance.loading}
                  onChange={onInputChange(insurance.groupNumber)}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <DatePickerField
                    format="MM/dd/yyyy"
                    openTo="year"
                    clearable
                    fullWidth
                    field={insurance.birthDateAt}
                    onChange={onDateChange(insurance.birthDateAt)}
                    disableFuture
                    loading={patientInsurance.loading}
                  />
                </MuiPickersUtilsProvider>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <Grid container spacing={3} justify="center" alignItems="center">
              <Grid item xs={12} md={6}>
                <div
                  className={
                    !insurance.insuranceImageFront.pristine &&
                    insurance.insuranceImageFront.error
                      ? classes.error
                      : ''
                  }
                >
                  <Typography>Front Photo of Insurance Card*</Typography>
                  {insurance.insuranceImageFront.base64 ? (
                    <ImageViewer
                      src={insurance.insuranceImageFront.base64}
                      height={248}
                      handleDelete={() => handleSetInsuranceImageFront()}
                      handleRetake={() =>
                        setOpenCam({
                          status: true,
                          action: val => handleSetInsuranceImageFront(val),
                          title: insurance.insuranceImageFront.caption,
                        })
                      }
                      loaderHeight={364}
                      loading={savedImages.loading}
                    />
                  ) : (
                    <ImageUploader
                      handleDrop={val => handleSetInsuranceImageFront(val)}
                      initCamera={() =>
                        setOpenCam({
                          status: true,
                          action: val => handleSetInsuranceImageFront(val),
                          title: insurance.insuranceImageFront.caption,
                        })
                      }
                      loaderHeight={364}
                      loading={savedImages.loading}
                    />
                  )}
                </div>
              </Grid>
              <Grid item xs={12} md={6}>
                <div
                  className={
                    !insurance.insuranceImageBack.pristine &&
                    insurance.insuranceImageBack.error
                      ? classes.error
                      : ''
                  }
                >
                  <Typography>Back Photo of Insurance Card*</Typography>
                  {insurance.insuranceImageBack.base64 ? (
                    <ImageViewer
                      src={insurance.insuranceImageBack.base64}
                      height={248}
                      handleDelete={() => handleSetInsuranceImageBack()}
                      handleRetake={() =>
                        setOpenCam({
                          status: true,
                          action: val => handleSetInsuranceImageBack(val),
                          title: insurance.insuranceImageBack.caption,
                        })
                      }
                      loaderHeight={364}
                      loading={savedImages.loading}
                    />
                  ) : (
                    <ImageUploader
                      handleDrop={val => handleSetInsuranceImageBack(val)}
                      initCamera={() =>
                        setOpenCam({
                          status: true,
                          action: val => handleSetInsuranceImageBack(val),
                          title: insurance.insuranceImageBack.caption,
                        })
                      }
                      loaderHeight={364}
                      loading={savedImages.loading}
                    />
                  )}
                </div>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <Grid container spacing={3} justify="center" alignItems="center">
              <Grid item xs={12} md={6}>
                <div
                  className={
                    !insurance.idImageFront.pristine &&
                    insurance.idImageFront.error
                      ? classes.error
                      : ''
                  }
                >
                  <Typography>Front Photo of ID*</Typography>
                  {insurance.idImageFront.base64 ? (
                    <ImageViewer
                      src={insurance.idImageFront.base64}
                      height={248}
                      handleDelete={() => handleSetIdIFront()}
                      handleRetake={() =>
                        setOpenCam({
                          status: true,
                          action: val => handleSetIdIFront(val),
                          title: 'Capture Front Photo of your ID',
                        })
                      }
                      loaderHeight={364}
                      loading={savedImages.loading}
                    />
                  ) : (
                    <ImageUploader
                      handleDrop={val => handleSetIdIFront(val)}
                      initCamera={() =>
                        setOpenCam({
                          status: true,
                          action: val => handleSetIdIFront(val),
                          title: 'Capture Front Photo of your ID',
                        })
                      }
                      loaderHeight={364}
                      loading={savedImages.loading}
                    />
                  )}
                </div>
              </Grid>
              <Grid item xs={12} md={6}>
                <div
                  className={
                    !insurance.idImageBack.pristine &&
                    insurance.idImageBack.error
                      ? classes.error
                      : ''
                  }
                >
                  <Typography>Back Photo of ID*</Typography>
                  {insurance.idImageBack.base64 ? (
                    <ImageViewer
                      src={insurance.idImageBack.base64}
                      height={248}
                      handleDelete={() => handleSetIdBack()}
                      handleRetake={() => {
                        setOpenCam({
                          status: true,
                          action: val => handleSetIdBack(val),
                          title: 'Capture Back Photo of your ID',
                        });
                      }}
                      loaderHeight={364}
                      loading={savedImages.loading}
                    />
                  ) : (
                    <ImageUploader
                      handleDrop={val => handleSetIdBack(val)}
                      initCamera={() =>
                        setOpenCam({
                          status: true,
                          action: val => handleSetIdBack(val),
                          title: 'Capture Back Photo of your ID',
                        })
                      }
                      loaderHeight={364}
                      loading={savedImages.loading}
                    />
                  )}
                </div>
              </Grid>
              <DialogBox
                status={openCam.status}
                handleClose={() => setOpenCam({ status: false })}
                title={openCam.title}
              >
                <WebCam
                  handleCapture={openCam.action}
                  handleClose={() => setOpenCam({ status: false })}
                  containerHeight="100vh"
                />
              </DialogBox>
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <Typography color="secondary">
              We will send you a bill if your insurance information is incorrect
              or rejected.
            </Typography>
          </Grid>
          <Grid item xs={12} md={6}>
            <GradientButton
              variant="contained"
              size="large"
              onClick={handleSaveInsurance}
            >
              {savedInsurance.loading ? (
                <CircularProgress color="secondary" />
              ) : (
                'Continue to take assessment'
              )}
            </GradientButton>
          </Grid>
        </Grid>
      )}
      <Grid container spacing={2}>
        <Grid item xs={12}>
          {showPayment && (
            <GradientButton size="large" onClick={onConferenceClick}>
              {buttonCaption}
            </GradientButton>
          )}
        </Grid>
      </Grid>
    </div>
  );
}

const { func, object, number, string } = PropTypes;
Insurance.propTypes = {
  patient: object.isRequired,
  amount: number,
  enqueueSnackbar: func.isRequired,
  dispatch: func.isRequired,
  doGetPatientInsurance: func.isRequired,
  doSavePatientInsurance: func.isRequired,
  doResetEligibilityCodes: func.isRequired,
  onConferenceClick: func.isRequired,
  doSetPaymentAmount: func.isRequired,
  doSaveIdDocument: func.isRequired,
  insuranceEligibility: object.isRequired,
  patientInsurance: object.isRequired,
  insuranceList: object.isRequired,
  buttonCaption: string,
  savedInsurance: object.isRequired,
  savedImages: object.isRequired,
  doGetSavedImages: func.isRequired,
  doSaveInsurancetoPs: func.isRequired,
  doSavePatientToHG: func.isRequired,
};

Insurance.defaultProps = {
  buttonCaption: 'Video Conference Now',
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doGetInsuranceEligibility: getInsuranceEligibility,
    doGetPatientInsurance: getPatientInsurance,
    doFindInsurance: findInsurance,
    doSavePatientInsurance: savePatientInsurance,
    doSetPaymentAmount: setPaymentAmount,
    doResetEligibilityCodes: resetEligibilityCodes,
    doSetFrontIdDocument: setFrontIdDocument,
    doSetBackIdDocument: setBackIdDocument,
    doSetFrontInsuranceDocument: setFrontInsuranceDocument,
    doSetBackInsuranceDocument: setBackInsuranceDocument,
    doGetSavedImages: getSavedImages,
    doSaveIdDocument: saveIdDocument,
    doSaveInsurancetoPs: saveInsuraceToPs,

    doSavePatientToHG: savePatientToHG,
  };
}

const mapStateToProps = createStructuredSelector({
  insuranceEligibility: makeSelectInsuranceEligibility(),
  patientInsurance: makeSelectPatientInsurance(),
  insuranceList: makeSelectInsuranceList(),
  grandTotal: makeSelectPaymentAmount(),
  idImage: makeSelectIdImage(),
  insuranceImage: makeSelectInsuranceImage(),
  savedInsurance: makeSelectSavedPatientInsurance(),
  savedImages: makeSelectGetSavedImages(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Insurance);
