/* eslint-disable react/no-unescaped-entities */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';

import DropIn from 'braintree-web-drop-in-react';

import { Grid, Typography } from '@material-ui/core';
import PadLockIcon from '@material-ui/icons/Lock';

import GradientButton from 'components/GradientButton';
import { checkoutBraintree } from 'containers/App/legacyActions';
import {
  makeSelectBrainTreeToken,
  makeSelectPatientCallId,
  makeSelectPaymentAmount,
} from 'containers/App/legacySelectors';

function BraintreeWidget({
  paymentMethod,
  patient,
  callId,
  braintree,
  amount,
  grandTotal,
  doBraintreeCheckout,
  dispatch,
  enqueueSnackbar,
}) {
  const [braintreeInstance, setInstance] = useState(undefined);
  const [canPay, setCanPay] = useState(false);
  const [showMe, setShowMe] = useState(true);

  useEffect(() => {
    setShowMe(grandTotal > 0);
  }, [grandTotal]);

  const token = braintree.data ? braintree.data.result : undefined;

  const handleBuy = () => {
    braintreeInstance.requestPaymentMethod((err, payload) => {
      if (err) {
        // Handle errors in requesting payment method
        enqueueSnackbar(
          'Credit card authorization failed. Please correct and try again.',
          {
            variant: 'error',
          },
        );
      }

      if (!err) {
        const { nonce } = payload;
        // eslint-disable-next-line camelcase
        const { call_id } = callId.data.result;
        const params = {
          amount,
          payment_method_nonce: nonce,
          patient_id: patient.patientLegacyId,
          call_id,
          payment_mode: paymentMethod,
        };
        dispatch(doBraintreeCheckout(params));

        enqueueSnackbar(
          'Payment successful. Please wait while we check you in to our virtual waiting room...',
          {
            variant: 'success',
          },
        );
      }
    });
  };
  if (!showMe) return null;
  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Typography variant="h6">
          Please use the payment options below to complete the transaction.
        </Typography>
        <Typography>
          <PadLockIcon /> Payment details are encrypted and securely provided by
          our payment processor. We don't store any sensitive data on our
          servers.
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <DropIn
          options={{
            authorization: token,
            paypal: {
              flow: 'checkout',
              amount,
              currency: 'USD',
            },
            applePay: {
              displayName: 'checkout',
              paymentRequest: {
                total: {
                  label: 'checkout',
                  amount,
                },
              },
            },
            googlePay: {
              googlePayVersion: 2,
              merchantInfo: {
                merchantId: '17369998254377824045',
              },
              transactionInfo: {
                totalPriceStatus: 'FINAL',
                totalPrice: amount,
                currencyCode: 'USD',
              },
              allowedPaymentMethods: [
                {
                  type: 'CARD',
                  parameters: {},
                },
              ],
            },
            paypalCredit: {
              flow: 'checkout',
              amount,
              currency: 'USD',
            },
          }}
          onInstance={instance => {
            setInstance(instance);
          }}
          onNoPaymentMethodRequestable={() => setCanPay(false)}
          onPaymentMethodRequestable={() => setCanPay(true)}
          // onPaymentOptionSelected={() => onCanPay(true)}
        />
      </Grid>
      <Grid item xs={12} md={6}>
        <GradientButton
          variant="contained"
          size="large"
          disabled={!canPay}
          onClick={handleBuy}
        >
          Pay Now $ {amount ? amount.toFixed(2) : 0.0}
        </GradientButton>
      </Grid>
    </Grid>
  );
}

const { object, number, func, string } = PropTypes;
BraintreeWidget.propTypes = {
  braintree: object.isRequired,
  dispatch: func.isRequired,
  doBraintreeCheckout: func.isRequired,
  amount: number.isRequired,
  grandTotal: number,
  patient: object.isRequired,
  callId: object.isRequired,
  enqueueSnackbar: func.isRequired,
  paymentMethod: string,
};

BraintreeWidget.defaultProps = {
  paymentMethod: 'selfPay',
};
const mapStateToProps = createStructuredSelector({
  braintree: makeSelectBrainTreeToken(),
  callId: makeSelectPatientCallId(),
  grandTotal: makeSelectPaymentAmount(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doBraintreeCheckout: checkoutBraintree,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(BraintreeWidget);
