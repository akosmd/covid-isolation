import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import moment from 'moment';
import _ from 'lodash';

import { createStructuredSelector } from 'reselect';
import { Grid, Typography, CircularProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import TextField from 'components/TextField';
import GradientButton from 'components/GradientButton';
import DatePickerField from 'components/DatePickerField';

import Autocomplete from '@material-ui/lab/Autocomplete';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import MuiTextField from '@material-ui/core/TextField';
import { REGULAR_CONFERENCE_URL } from 'utils/config';

import {
  handleChange,
  highlightFormErrors,
  handleDateChange,
  setValuesFrom,
  extractFormValues,
  checkStatus,
} from 'utils/formHelper';

import {
  getInsuranceEligibility,
  getPatientInsurance,
  findInsurance,
  resetEligibilityCodes,
  setPaymentAmount,
  savePatientInsurance,
} from 'containers/App/legacyActions';

import {
  makeSelectInsuranceEligibility,
  makeSelectPatientInsurance,
  makeSelectInsuranceList,
  makeSelectPaymentAmount,
} from 'containers/App/legacySelectors';

import { insuranceDetails } from './model';
import CouponCode from './coupon-code';
import Braintree from './braintree';

const useStyles = makeStyles({
  formContainer: {
    width: '100%',
  },
  loginLink: {
    marginLeft: '1rem',
  },
});

export function Eligibility({
  dispatch,
  patient,
  amount,
  grandTotal,
  insuranceList,
  enqueueSnackbar,
  insuranceEligibility,
  patientInsurance,
  doGetPatientInsurance,
  doSavePatientInsurance,
  doSetPaymentAmount,
  onConferenceClick,
  doFindInsurance,
  doResetEligibilityCodes,
  doGetInsuranceEligibility,
  buttonCaption,
}) {
  const [insurance, setInsurance] = useState({ ...insuranceDetails });
  const [showPayment, setShowPayment] = useState(false);
  const [insuranceData, setInsuranceData] = useState([]);
  const [selectedInsurance, setSelectedInsurance] = useState(undefined);

  const classes = useStyles();

  useEffect(() => {
    dispatch(doGetPatientInsurance(patient.patientLegacyId));
  }, []);

  useEffect(() => {
    if (patientInsurance.data && patientInsurance.data.code === 200) {
      if (insuranceData.length === 0) {
        const firstWord = patientInsurance.data.result.insurance_provider.split(
          ' ',
        )[0];
        dispatch(doFindInsurance(firstWord));
      }
      const selected = insuranceData.find(
        i => i.name === patientInsurance.data.result.insurance_provider,
      );

      const {
        member_id: memberId,
        group_number: groupNumber,
      } = patientInsurance.data.result;
      if (selected) {
        setInsurance({
          ...insurance,
          provider: {
            ...insurance.provider,
            value: selected.name,
            error: false,
          },
          providerId: {
            ...insurance.providerId,
            value: selected.trading_partner_id,
          },
          memberId: { ...insurance.memberId, value: memberId, error: false },
          groupNumber: {
            ...insurance.groupNumber,
            value: groupNumber,
            error: false,
          },
          completed: true,
        });
        dispatch(doSetPaymentAmount(parseFloat(selected.copay)));
        setSelectedInsurance(selected);
      }
    }
  }, [patientInsurance, insuranceData]);

  useEffect(() => {
    if (patient) {
      const fieldsWithValues = setValuesFrom(patient, insurance);

      setInsurance({
        ...fieldsWithValues,
      });
    }
  }, [patient]);

  useEffect(() => {
    if (insuranceList.data && insuranceList.data.result) {
      if (!insuranceList.data.result.msg)
        setInsuranceData(insuranceList.data.result);
    }
  }, [insuranceList]);

  useEffect(() => {
    if (!insuranceEligibility.error && insuranceEligibility.data) {
      saveInsuranceDetails();
    }
  }, [insuranceEligibility]);

  const handleCheckEligibility = () => {
    if (!insurance.completed) {
      highlightFormErrors(insurance, setInsurance);
      enqueueSnackbar('Please fill out all fields marked with asterisk *', {
        variant: 'error',
      });
    } else {
      getEligibility();
    }
  };

  const getEligibility = () => {
    const formValues = extractFormValues(insurance);

    const {
      memberId,
      groupNumber,
      birthDateAt,
      firstName,
      lastName,
      providerId,
    } = formValues;
    const params = {
      memberId,
      groupNumber,
      eligibilityCheck: true,
      date_of_birth: moment(birthDateAt).format('MM/DD/YYYY'),
      fullName: `${firstName} ${lastName}`,
      tradingPartnerId: providerId,
      insuranceCardFrontImage: '',
      insuranceCardBackImage: '',
    };
    dispatch(doGetInsuranceEligibility(params));
  };
  const saveInsuranceDetails = () => {
    const formValues = extractFormValues(insurance);

    const {
      memberId,
      groupNumber,
      birthDateAt,
      firstName,
      lastName,
      provider,
      providerId,
    } = formValues;

    const formData = new FormData();
    formData.set('patient_id', patient.patientLegacyId);
    formData.set('plan_name', '');
    formData.set('payer_first_name', firstName);
    formData.set('payer_last_name', lastName);
    formData.set('member_id', memberId);
    formData.set('group_number', groupNumber);
    formData.set('effective_from_date', '');
    formData.set('effective_to_date', '');
    formData.set('dateofbirth', moment(birthDateAt).format('MM/DD/YYYY'));
    formData.set('insurance_provider', provider);
    formData.set('trading_partner_id', providerId);
    formData.set('transaction_id', insuranceEligibility.data.transactionId);

    dispatch(doSavePatientInsurance(formData));
    dispatch(doResetEligibilityCodes());
    setShowPayment(insuranceEligibility.data && amount > 0);
  };
  const onInputChange = field => event => {
    handleChange({
      field: field.id,
      state: insurance,
      event,
      saveStepFunc: setInsurance,
    });
  };

  const onInsuranceChange = field => event => {
    const { value } = event.target;
    const debounced = _.debounce(() => dispatch(doFindInsurance(value)), 1000);
    if (value) debounced();
    handleChange({
      field: field.id,
      state: insurance,
      event,
      saveStepFunc: setInsurance,
    });
  };

  const onInsuranceSelected = e => {
    const error = e === null;
    const formCompleted =
      !error && !checkStatus({ source: insurance, currentField: 'provider' });
    setInsurance({
      ...insurance,
      provider: {
        ...insurance.provider,
        value: e !== null ? e.name : undefined,
        error,
      },
      providerId: {
        ...insurance.providerId,
        value: e !== null ? e.trading_partner_id : undefined,
      },
      completed: formCompleted,
    });
    setSelectedInsurance(e);
    dispatch(doSetPaymentAmount(parseFloat(e.copay)));
  };

  const onDateChange = field => value => {
    handleDateChange({
      field: field.id,
      state: insurance,
      value,
      saveStepFunc: setInsurance,
    });
  };

  const confAddress = `${REGULAR_CONFERENCE_URL}${
    patient.uuid
  }&location=AkosChat360&auto=1`;

  const renderSearch = defaultValue => (
    <Autocomplete
      autoComplete={insuranceData !== undefined}
      id="combo-box-demo"
      options={insuranceData}
      getOptionLabel={option => option.name}
      onChange={(o, val) => onInsuranceSelected(val)}
      noOptionsText={
        insurance.provider.value
          ? `Could not find Insurance ${insurance.provider.value}`
          : 'Type Insurance name to search'
      }
      loadingText={`Searching for ${insurance.provider.value}`}
      defaultValue={defaultValue}
      loading={insuranceList.loading}
      renderInput={params => (
        <MuiTextField
          {...params}
          label={
            !insurance.provider.pristine && insurance.provider.error
              ? 'Insurance Provider is required'
              : 'Type to search'
          }
          variant="outlined"
          error={!insurance.provider.pristine && insurance.provider.error}
          onChange={onInsuranceChange(insurance.provider)}
          fullWidth
        />
      )}
    />
  );

  return (
    <div>
      {!showPayment && (
        <Grid container spacing={2} direction="column">
          <Grid item xs={12}>
            <Typography variant="h4" color="primary">
              Insurance Details
            </Typography>
          </Grid>
          <Grid item xs={12} className={classes.formContainer}>
            <Grid container spacing={3} justify="center" alignItems="center">
              <Grid item xs={12} md={6}>
                {!selectedInsurance && renderSearch()}
                {selectedInsurance && renderSearch(selectedInsurance)}
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={insurance.firstName}
                  variant="outlined"
                  onChange={onInputChange(insurance.firstName)}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={insurance.lastName}
                  variant="outlined"
                  onChange={onInputChange(insurance.lastName)}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={insurance.memberId}
                  variant="outlined"
                  focus
                  onChange={onInputChange(insurance.memberId)}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={insurance.groupNumber}
                  variant="outlined"
                  onChange={onInputChange(insurance.groupNumber)}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <DatePickerField
                    format="MM/dd/yyyy"
                    openTo="year"
                    clearable
                    fullWidth
                    field={insurance.birthDateAt}
                    onChange={onDateChange(insurance.birthDateAt)}
                    disableFuture
                  />
                </MuiPickersUtilsProvider>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12} md={6}>
            <GradientButton
              variant="contained"
              size="large"
              onClick={handleCheckEligibility}
            >
              {insuranceEligibility.loading ? (
                <CircularProgress color="secondary" />
              ) : (
                'Check Eligibility'
              )}
            </GradientButton>
          </Grid>
        </Grid>
      )}
      <Grid container spacing={2}>
        {showPayment && (
          <Grid item xs={12}>
            <CouponCode
              patient={patient}
              enqueueSnackbar={enqueueSnackbar}
              amount={
                selectedInsurance ? parseFloat(selectedInsurance.copay) : amount
              }
            />
          </Grid>
        )}
        {showPayment && (
          <Grid item xs={12}>
            <Typography variant="h4" color="primary">
              Copay Amount: $ {grandTotal.toFixed(2)}
            </Typography>
          </Grid>
        )}
        <Grid item xs={12}>
          {showPayment && (
            <Braintree
              patient={patient}
              amount={
                selectedInsurance ? parseFloat(selectedInsurance.copay) : amount
              }
              enqueueSnackbar={enqueueSnackbar}
              confAddress={confAddress}
            />
          )}
          {grandTotal < 1 && showPayment && (
            <GradientButton size="large" onClick={onConferenceClick}>
              {buttonCaption}
            </GradientButton>
          )}
        </Grid>
      </Grid>
    </div>
  );
}

const { func, object, number, string } = PropTypes;
Eligibility.propTypes = {
  patient: object.isRequired,
  amount: number,
  grandTotal: number,
  enqueueSnackbar: func.isRequired,
  dispatch: func.isRequired,
  doGetInsuranceEligibility: func.isRequired,
  doGetPatientInsurance: func.isRequired,
  doFindInsurance: func.isRequired,
  doSavePatientInsurance: func.isRequired,
  doResetEligibilityCodes: func.isRequired,
  onConferenceClick: func.isRequired,
  doSetPaymentAmount: func.isRequired,
  insuranceEligibility: object.isRequired,
  patientInsurance: object.isRequired,
  insuranceList: object.isRequired,
  buttonCaption: string,
};

Eligibility.defaultProps = {
  buttonCaption: 'Video Conference Now',
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doGetInsuranceEligibility: getInsuranceEligibility,
    doGetPatientInsurance: getPatientInsurance,
    doFindInsurance: findInsurance,
    doSavePatientInsurance: savePatientInsurance,
    doSetPaymentAmount: setPaymentAmount,
    doResetEligibilityCodes: resetEligibilityCodes,
  };
}

const mapStateToProps = createStructuredSelector({
  insuranceEligibility: makeSelectInsuranceEligibility(),
  patientInsurance: makeSelectPatientInsurance(),
  insuranceList: makeSelectInsuranceList(),
  grandTotal: makeSelectPaymentAmount(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Eligibility);
