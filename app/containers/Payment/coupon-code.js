import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { createStructuredSelector } from 'reselect';

import { connect } from 'react-redux';
import { compose } from 'redux';

import { Grid, Typography, CircularProgress, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import TextField from 'components/TextField';
import GradientButton from 'components/GradientButton';
import {
  handleChange,
  highlightFormErrors,
  extractFormValues,
} from 'utils/formHelper';

import {
  checkCouponCode,
  setPaymentAmount,
  resetCouponCode,
} from 'containers/App/legacyActions';

import {
  makeSelectCouponCode,
  makeSelectPaymentAmount,
} from 'containers/App/legacySelectors';

import { couponCodeModel } from './model';

const useStyles = makeStyles({
  formContainer: {
    width: '100%',
  },
});

function CouponCode({
  dispatch,
  couponCode,
  doCheckCouponCode,
  doSetPaymentAmount,
  doResetCouponCode,
  enqueueSnackbar,
  amount,
}) {
  const [code, setCode] = useState({ ...couponCodeModel });
  const [discount, setDiscount] = useState(0);
  const classes = useStyles();
  useEffect(() => {
    dispatch(doSetPaymentAmount(amount));
  }, []);
  useEffect(() => {
    if (couponCode.data && couponCode.data.code === 402) {
      enqueueSnackbar('Please input a valid Offer Code', {
        variant: 'error',
      });
    } else if (couponCode.data && couponCode.data.code === 200) {
      const { discountPercentage } = couponCode.data.result;
      const percent =
        parseFloat(discountPercentage) === 100
          ? 1
          : `.${parseFloat(discountPercentage)}`;
      const discountAmount = amount * parseFloat(percent);
      setDiscount(discountAmount);
      dispatch(doSetPaymentAmount(amount - discountAmount));
    }
  }, [couponCode, amount]);

  const onInputChange = field => event => {
    handleChange({
      field: field.id,
      state: code,
      event,
      saveStepFunc: setCode,
    });
  };

  const onApplyCouponCode = () => {
    if (!code.completed) {
      highlightFormErrors(code, setCode);
      enqueueSnackbar('Please input a valid Offer Code', {
        variant: 'error',
      });
    } else {
      const formValues = extractFormValues(code);
      const formData = new FormData();
      formData.set('promocodeType', 'pay_per_use');
      formData.set('promocodeName', formValues.couponCode);

      dispatch(doCheckCouponCode(formData));
    }
  };

  const handleRemoveDiscount = () => {
    setDiscount(0);
    dispatch(doResetCouponCode());
    dispatch(doSetPaymentAmount(amount));
  };
  return (
    <Grid
      container
      spacing={2}
      direction="row"
      // justify="center"
      alignItems="center"
    >
      <Grid item xs={12}>
        <Typography variant="h4" color="primary">
          Offer Code
        </Typography>
      </Grid>
      <Grid item xs={12} md={7} className={classes.formContainer}>
        <TextField
          field={code.couponCode}
          variant="outlined"
          onChange={onInputChange(code.couponCode)}
        />
      </Grid>

      <Grid item xs={12} md={5} className={classes.formContainer}>
        <GradientButton
          variant="contained"
          size="large"
          onClick={onApplyCouponCode}
        >
          {couponCode.loading ? (
            <CircularProgress color="secondary" />
          ) : (
            'Apply'
          )}
        </GradientButton>
      </Grid>
      <Grid item xs={7} md={5}>
        <Typography>Your Order Total</Typography>
      </Grid>
      <Grid item xs={7} md={5}>
        <Typography>$ {amount ? amount.toFixed(2) : 0.0}</Typography>
      </Grid>
      <Grid item xs={7} md={5}>
        <Typography>
          <em>Promotional Discount</em>
        </Typography>
      </Grid>
      <Grid item xs={7} md={5}>
        <Typography>
          <em>- $ {discount ? discount.toFixed(2) : 0.0}</em>
        </Typography>
        {discount > 0 && <Button onClick={handleRemoveDiscount}>Remove</Button>}
      </Grid>
      <Grid item xs={7} md={5}>
        <Typography>
          <strong>Grand Total</strong>
        </Typography>
      </Grid>
      <Grid item xs={7} md={5}>
        <Typography>
          <strong>$ {(amount - discount).toFixed(2)}</strong>
        </Typography>
      </Grid>
    </Grid>
  );
}

const { object, func, number } = PropTypes;
CouponCode.propTypes = {
  dispatch: func.isRequired,
  couponCode: object.isRequired,
  doCheckCouponCode: func.isRequired,
  doSetPaymentAmount: func.isRequired,
  doResetCouponCode: func.isRequired,
  amount: number,
  enqueueSnackbar: func.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doCheckCouponCode: checkCouponCode,
    doSetPaymentAmount: setPaymentAmount,
    doResetCouponCode: resetCouponCode,
  };
}

const mapStateToProps = createStructuredSelector({
  couponCode: makeSelectCouponCode(),
  grandTotal: makeSelectPaymentAmount(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(CouponCode);
