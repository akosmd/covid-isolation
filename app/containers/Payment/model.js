import { initialState, initialField } from 'utils/formHelper';

export const insuranceDetails = {
  providerId: {
    ...initialField,
    id: 'providerId',
    required: false,
    error: false,
  },
  provider: {
    ...initialField,
    caption: 'Type to search',
    id: 'provider',
  },
  firstName: {
    ...initialField,
    id: 'firstName',
    caption: 'First Name',
  },
  lastName: {
    ...initialField,
    id: 'lastName',
    caption: 'Last Name',
  },
  memberId: {
    ...initialField,
    id: 'memberId',
    caption: 'Member Id',
  },
  groupNumber: {
    ...initialField,
    id: 'groupNumber',
    caption: 'Group Number',
  },
  birthDateAt: {
    ...initialField,
    id: 'birthDateAt',
    caption: 'Date of Birth',
    value: null,
  },
  ...initialState,
};

export const covidInsuranceDetails = {
  provider: {
    ...initialField,
    caption: 'Insurance Name',
    id: 'provider',
  },
  firstName: {
    ...initialField,
    id: 'firstName',
    caption: 'First Name',
  },
  lastName: {
    ...initialField,
    id: 'lastName',
    caption: 'Last Name',
  },
  memberId: {
    ...initialField,
    id: 'memberId',
    caption: 'Member Id',
  },
  groupNumber: {
    ...initialField,
    id: 'groupNumber',
    caption: 'Group Number',
    required: false,
    error: false,
  },
  birthDateAt: {
    ...initialField,
    id: 'birthDateAt',
    caption: 'Date of Birth',
    // value: null,
  },
  insuranceImageFront: {
    ...initialField,
    id: 'insuranceImageFront',
    caption: 'Front Photo of Insurance Card',
    base64: null,
    filename: null,
    filetype: null,
    isFile: true,
  },
  insuranceImageBack: {
    ...initialField,
    id: 'insuranceImageBack',
    caption: 'Back Photo of Insurance Card',
    base64: null,
    filename: null,
    filetype: null,
    isFile: true,
  },
  idImageFront: {
    ...initialField,
    id: 'idImageFront',
    caption: 'Front Photo of ID Card',
    base64: null,
    filename: null,
    filetype: null,
    isFile: true,
  },
  idImageBack: {
    ...initialField,
    id: 'idImageBack',
    caption: 'Back Photo of ID Card',
    base64: null,
    filename: null,
    filetype: null,
    isFile: true,
  },
  ...initialState,
};

export const couponCodeModel = {
  couponCode: {
    ...initialField,
    caption: 'Have an offer code?',
    errorMessage: 'Input a valid Offer code',
    id: 'couponCode',
  },
  ...initialState,
};

export const dependentModel = {
  firstName: {
    ...initialField,
    id: 'firstName',
    caption: 'First Name',
  },
  lastName: {
    ...initialField,
    id: 'lastName',
    caption: 'Last Name',
  },
  employerCode: {
    ...initialField,
    id: 'employerCode',
    caption: 'Employer Code',
    required: false,
  },
  dateOfBirth: {
    ...initialField,
    id: 'dateOfBirth',
    caption: 'Date of Birth',
    value: null,
  },
  email: {
    ...initialField,
    id: 'email',
    caption: 'Email Address',
  },
  phone: {
    ...initialField,
    id: 'phone',
    caption: 'Phone Number',
  },
  gender: {
    ...initialField,
    id: 'gender',
    caption: 'Gender',
    variant: 'outlined',
  },
  street: { ...initialField, id: 'street', caption: 'Street Address' },
  city: { ...initialField, id: 'city', caption: 'City' },
  state: { ...initialField, id: 'state', caption: 'State' },
  zipCode: { ...initialField, id: 'zipCode', caption: 'Zip Code' },
  ...initialState,
};
