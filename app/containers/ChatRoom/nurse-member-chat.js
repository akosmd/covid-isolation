import React, { useState, useEffect } from 'react';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { push } from 'connected-react-router';
import { createStructuredSelector } from 'reselect';

import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { Grid, CardHeader, Typography, Button } from '@material-ui/core';

import Echo from 'laravel-echo';

import { GOODBYE_MESSAGE } from 'utils/config';
import {
  sendMessageToMember,
  setNurseConversation,
  leaveChatRoom,
  setSelectedUsertoChat,
  getChatMessages,
  resetChatMessages,
  postEndConversation,
} from 'containers/App/actions';

import {
  makeSelectNurseSignin,
  makeSelectedUserToChat,
  makeSelectChatWaitingInRoom,
  makeSelectChatMessages,
} from 'containers/App/selectors';
import 'pusher-js';
import Login from './login';
import ChatWindow from './nurseChatWindow';

import getOptions, { USER_TYPES } from './config';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },

  grid: {
    width: '99%',
    margin: '0 auto',
    [theme.breakpoints.down('md')]: {
      height: '95vh',
    },
    [theme.breakpoints.up('md')]: {
      height: '98vh',
    },
  },
  card: {
    minWidth: 375,
    height: 500,
    minHeight: 400,
    position: 'fixed',
    bottom: theme.spacing(10),
    right: theme.spacing(2),
    [theme.breakpoints.down('md')]: {
      minWidth: '96%',
      height: '90%',
      width: '96%',
      right: theme.spacing(1),
    },
    display: 'flex',
    flexDirection: 'column',
  },

  cardFullScreen: {
    display: 'flex',
    height: '90%',
    flexDirection: 'column',
    alignContent: 'stretch',
    justifyContent: 'stretch',
  },
  cardContent: {
    minHeight: 300,
    width: '100%',
    height: '100%',
  },

  cardHeaderTitle: {
    color: theme.palette.secondary.main,
  },

  cardRoot: {
    background: 'linear-gradient(to right, #55bcbe 0%,#85c3a6 100%) !important',
  },
  cardSubHeader: {
    color: theme.palette.primary.main,
  },
  fullContent: {
    display: 'flex',
    height: '100vh',
    overflowY: 'scroll',
    padding: 0,
    paddingBottom: '60px !important',
    [theme.breakpoints.up('md')]: {
      paddingBottom: '0 !important',
    },
  },

  chatAvatar: {
    background: '#12bcc5',
  },
  userList: {
    borderRight: '1px solid #f2f2f2',
  },
}));

// eslint-disable-next-line no-unused-vars
function NurseRoom({
  dispatch,
  signin,
  selectedUserToChat,
  doSendMessageToMember,
  doLeaveRoom,
  doSetSelectedUsertoChat,
  enqueueSnackbar,
  doGetChatMessages,
  chatMessages,
  doResetChatMessage,
  doPostEndConversation,
}) {
  const [messageToSend, setMessageToSend] = useState(undefined);
  const [conversations, setConversations] = useState([]);
  // const [echo, setEcho] = useState();

  useEffect(() => {
    dispatch(doGetChatMessages(selectedUserToChat.conferenceNo));
  }, []);

  useEffect(() => {
    if (chatMessages.data) {
      const messages = chatMessages.data.map(chat => ({
        from: chat.from.id,
        message: chat.message,
      }));
      setConversations(messages);
    }
  }, [chatMessages.data]);

  useEffect(() => {
    if (signin.data && !signin.error && selectedUserToChat) {
      createEcho(selectedUserToChat);
    }
  }, [signin, selectedUserToChat]);

  const classes = useStyles();

  const sendMessage = () => {
    const { id: to, conferenceNo } = selectedUserToChat;
    const from = signin.data.user.id;

    if (messageToSend.trim().length > 0) {
      const params = { message: messageToSend, userId: to, conferenceNo };
      dispatch(doSendMessageToMember(params));
      setConversations(prevState => [
        ...prevState,
        {
          message: params.message,
          from,
          to,
          conferenceNo,
        },
      ]);

      setMessageToSend('');
    }
  };

  const sendGoodbyMessage = () => {
    const { id: to, conferenceNo } = selectedUserToChat;

    const params = {
      message: GOODBYE_MESSAGE,
      userId: signin.data.user.id,
      conferenceNo,
    };
    dispatch(doSendMessageToMember(params));
    setConversations(prevState => [
      ...prevState,
      {
        message: params.message,
        from: signin.data.user.id,
        to,
        conferenceNo,
      },
    ]);
  };

  const createEcho = userToChat => {
    const tmpEcho = new Echo(getOptions(signin.data, USER_TYPES.nurse));

    const { conferenceNo, id: userId, userChannelName } = userToChat;
    tmpEcho.private(signin.data.userChannelName).listen('UserChatEvent', e => {
      if (userId === e.senderId) {
        setConversations(prevState => [
          ...prevState,
          {
            from: e.senderId,
            message: e.message,
            conferenceNo,
          },
        ]);
      }
    });

    tmpEcho
      .join(userChannelName)
      .here(() => {})
      .joining(() => {
        // Might need to handle this later
      })
      .leaving(() => {
        handleEndConversation();
      });
  };

  const onChange = e => {
    const { value } = e.target;

    setMessageToSend(value);
  };

  const handleBackToRoom = () => {
    dispatch(push('/chatroom/nurse'));
    dispatch(doResetChatMessage());
  };

  const handleEndConversation = () => {
    sendGoodbyMessage();
    // dispatch(doLeaveRoom(selectedUserToChat.id));
    dispatch(doLeaveRoom(signin.data.user.id));
    dispatch(doSetSelectedUsertoChat());
    dispatch(doResetChatMessage());
    dispatch(doPostEndConversation(selectedUserToChat.conferenceNo));
    handleBackToRoom();
  };

  if (!signin.data || signin.error) {
    return <Login enqueueSnackbar={enqueueSnackbar} />;
  }
  const userName =
    selectedUserToChat && selectedUserToChat.id
      ? `${selectedUserToChat.firstName}\n ${selectedUserToChat.lastName}`
      : '';
  const userEmail =
    selectedUserToChat && selectedUserToChat.id ? selectedUserToChat.email : '';
  const userBday =
    selectedUserToChat && selectedUserToChat.id
      ? selectedUserToChat.birthDate
      : '';
  return (
    <Grid container>
      <Grid item xs={12}>
        <Card elevation={1} className={classes.cardFullScreen}>
          <CardHeader
            className={classes.cardRoot}
            subheader={
              <>
                <Button onClick={handleBackToRoom}>Back to Waiting Room</Button>
                <Button onClick={handleEndConversation}>
                  End Conversation
                </Button>
              </>
            }
            action={
              <>
                <Typography variant="h6">{userName}</Typography>
                <Typography variant="caption" display="block">
                  {userEmail}
                </Typography>
                <Typography variant="caption" display="block">
                  {userBday}
                </Typography>
              </>
            }
            classes={{
              title: classes.cardHeaderTitle,
              subheader: classes.cardSubHeader,
            }}
          />
          <CardContent className={classes.fullContent}>
            <ChatWindow
              onSendMessage={sendMessage}
              user={selectedUserToChat}
              onChange={onChange}
              message={messageToSend}
              conversation={conversations}
            />
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
}

const { object, func } = PropTypes;
NurseRoom.propTypes = {
  dispatch: func.isRequired,
  signin: object.isRequired,
  selectedUserToChat: object.isRequired,
  doSendMessageToMember: func.isRequired,
  doSetSelectedUsertoChat: func.isRequired,
  doLeaveRoom: func.isRequired,
  enqueueSnackbar: func.isRequired,
  doGetChatMessages: func.isRequired,
  chatMessages: object.isRequired,
  doResetChatMessage: func.isRequired,
  doPostEndConversation: func.isRequired,
};
const mapStateToProps = createStructuredSelector({
  signin: makeSelectNurseSignin(),
  selectedUserToChat: makeSelectedUserToChat(),
  usersInRoom: makeSelectChatWaitingInRoom(),
  chatMessages: makeSelectChatMessages(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doSendMessageToMember: sendMessageToMember,
    doSetNurseConversations: setNurseConversation,
    doSetSelectedUsertoChat: setSelectedUsertoChat,
    doLeaveRoom: leaveChatRoom,
    doGetChatMessages: getChatMessages,
    doResetChatMessage: resetChatMessages,
    doPostEndConversation: postEndConversation,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(NurseRoom);
