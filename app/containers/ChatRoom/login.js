/**
 *
 * Login
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';

import KeyboardEventHandler from 'react-keyboard-event-handler';

import { Grid, Typography, CircularProgress } from '@material-ui/core';
import Link from '@material-ui/core/Link';
import TextField from 'components/TextField';
import GradientButton from 'components/GradientButton';

import {
  loginNurse,
  resetAuthFields,
  resetSignin,
  setNotificationType,
} from 'containers/App/actions';
import { makeSelectNurseSignin } from 'containers/App/selectors';

import {
  initialField,
  initialState,
  handleChange,
  highlightFormErrors,
  extractFormValues,
} from 'utils/formHelper';

import { INXITE_SSO_URL } from 'utils/config';
import { FORGOT_PASS_URL } from 'utils/constants';

const email = {
  ...initialField,
  id: 'email',
  caption: 'Phone Number or Email',
  errorMessage: 'Phone Number or Email is required',
};
const password = {
  ...initialField,
  id: 'password',
  caption: 'Password',
};
export function Login({
  dispatch,
  signin,
  doNurseSignin,
  doResetSignin,
  enqueueSnackbar,
}) {
  const [login, setLogin] = useState({
    email,
    password,
    ...initialState,
  });

  useEffect(() => {
    if (signin.error || signin.error.status === 401) {
      enqueueSnackbar('Invalid Phone Number or Email or Password', {
        variant: 'error',
      });
      dispatch(doResetSignin());
      // eslint-disable-next-line no-empty
    } else if (signin.data) {
    }
  }, [signin]);

  const onInputChange = field => event => {
    handleChange({
      field: field.id,
      state: login,
      event,
      saveStepFunc: setLogin,
    });
  };

  const handleSubmit = () => {
    if (!login.completed) {
      enqueueSnackbar('Please input your Email and Password', {
        variant: 'error',
      });
      highlightFormErrors(login, setLogin);
    }
    if (login.completed) {
      const params = extractFormValues(login);
      dispatch(doNurseSignin(params));
    }
  };

  return (
    <div>
      <Grid container spacing={2} direction="column">
        <Grid item xs={12}>
          <Typography variant="h4" color="primary">
            Sign In
            <Typography variant="body1" color="textPrimary">
              Sign in with your email address and password
            </Typography>
          </Typography>
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          <Grid container spacing={1}>
            <Grid item xs={12} sm={12} md={8}>
              <KeyboardEventHandler
                handleKeys={['enter']}
                onKeyEvent={handleSubmit}
              >
                <TextField
                  field={login.email}
                  variant="outlined"
                  onChange={onInputChange(login.email)}
                />
              </KeyboardEventHandler>
            </Grid>

            <Grid item xs={12} sm={12} md={8}>
              <KeyboardEventHandler
                handleKeys={['enter']}
                onKeyEvent={handleSubmit}
              >
                <TextField
                  field={login.password}
                  type="password"
                  variant="outlined"
                  onChange={onInputChange(login.password)}
                />
              </KeyboardEventHandler>
            </Grid>
          </Grid>
        </Grid>

        <Grid item xs={12} md={6}>
          <Link href={FORGOT_PASS_URL} color="secondary">
            Forgot Password
          </Link>
        </Grid>
        <Grid item xs={12} md={3}>
          <GradientButton
            variant="contained"
            size="large"
            onClick={handleSubmit}
          >
            {signin.loading && <CircularProgress color="secondary" />}
            {!signin.loading && 'Proceed'}
          </GradientButton>
        </Grid>
      </Grid>
      <iframe
        id="inxiteFrame"
        title="Inxite"
        src={`${INXITE_SSO_URL}logout`}
        style={{ display: 'none' }}
      />
    </div>
  );
}

const { func, object } = PropTypes;

Login.propTypes = {
  dispatch: func.isRequired,
  doNurseSignin: func.isRequired,
  signin: object,
  enqueueSnackbar: func.isRequired,
  doResetSignin: func.isRequired,
};
const mapStateToProps = createStructuredSelector({
  signin: makeSelectNurseSignin(),
});
function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doNurseSignin: loginNurse,
    doResetAuthFields: resetAuthFields,
    doSetNotificationType: setNotificationType,
    doResetSignin: resetSignin,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Login);
