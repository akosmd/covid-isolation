/**
 *
 * Chat
 *
 */

import React from 'react';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { compose } from 'redux';

import { makeStyles } from '@material-ui/core/styles';

import KeyboardEventHandler from 'react-keyboard-event-handler';

import 'typeface-lato';

import {
  makeSelectVerifyAuth,
  makePatientImpersonation,
} from 'containers/App/selectors';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    width: '100%',
    height: '100%',
  },

  grid: {
    width: '99%',
    margin: '0 auto',
    [theme.breakpoints.down('md')]: {
      height: '98vh',
    },
    [theme.breakpoints.up('md')]: {
      height: '98vh',
    },
  },
  card: {
    minWidth: 375,
    height: 500,
    minHeight: 400,
    position: 'fixed',
    bottom: theme.spacing(10),
    right: theme.spacing(2),
    [theme.breakpoints.down('md')]: {
      minWidth: '96%',
      height: '90%',
      width: '96%',
      right: theme.spacing(1),
    },
    display: 'flex',
    flexDirection: 'column',
  },

  cardFullScreen: {
    display: 'flex',
    height: '97%',
    flexDirection: 'column',
    alignContent: 'stretch',
    justifyContent: 'stretch',
  },
  cardContent: {
    minHeight: 300,
    width: '100%',
    height: '100%',
  },

  cardHeaderTitle: {
    color: theme.palette.secondary.main,
  },

  cardRoot: {
    background: 'linear-gradient(to right, #55bcbe 0%,#85c3a6 100%) !important',
  },
  cardSubHeader: {
    color: theme.palette.primary.main,
  },
  fullContent: {
    display: 'flex',
    height: '100vh',
    overflowY: 'scroll',
    padding: 0,
    paddingBottom: '60px !important',
    [theme.breakpoints.up('md')]: {
      paddingBottom: '0 !important',
    },
  },

  chatAvatar: {
    background: '#12bcc5',
  },
  inputBox: {
    backgroundColor: 'white',
    color: 'black',
    fontFamily: 'inherit',
    fontSize: 'inherit',
    height: '100%',
    borderWidth: '0px',
    borderStyle: 'initial',
    borderColor: 'initial',
    borderImage: 'initial',
    outline: '0px',
    padding: '0px',
    flex: '1 1 0%',
    width: '100%',
    overflow: 'visible',
  },
  sendButton: {
    backgroundColor: 'transparent',
    height: '100%',
    width: '40px',
    borderWidth: '0px',
    borderStyle: 'initial',
    borderColor: 'initial',
    borderImage: 'initial',
    outline: '0px',
    padding: '0px',
    '& svg': {
      fill: 'rgb(118, 118, 118);',
    },
    '&:hover svg': {
      fill: 'rgb(51, 51, 51);',
    },
  },
  userMessage: {
    backgroundColor: '#0078d7',
    border: '#0078d7',
    color: '#fff',
    borderRadius: '5px',
    padding: '.5rem',
    marginRight: '1rem',
  },
  senderMessage: {
    backgroundColor: '#ECEFF6',
    border: '#ECEFF6',
    borderRadius: '5px',
    padding: '.5rem',
    marginLeft: '1rem',
  },
  message: {
    display: 'flex',
    // flexDirection: 'row-reverse',
  },
  messageHolder: {
    display: 'flex',
    flexDirection: 'row-reverse',
  },
  sender: {
    textAlign: 'right',
    marginRight: '1rem',
    fontStyle: 'italic',
  },
  receiver: {
    textAlign: 'left',
    marginLeft: '1rem',
    fontStyle: 'italic',
  },
  senderOl: {
    padding: '1rem',
    paddingInlineStart: '1rem',
  },
  chatContainer: {
    display: 'flex',
    flexDirection: 'column',
    height: '95%',
    [theme.breakpoints.down('md')]: {
      height: '100%',
    },
    overflowY: 'auto',
    width: '100%',
  },
}));

function NurseChatWindow({
  onChange,
  onSendMessage,
  message,
  user,
  conversation,
  disabled,
}) {
  const classes = useStyles();

  const renderMessages = () => (
    <ul className={classes.senderOl} id="message-container">
      {conversation.map((msg, idx) => {
        const msgIsFromUser = user.id === msg.from;
        return (
          <li
            key={`msg-${idx + 1}`}
            className={msgIsFromUser ? classes.message : classes.messageHolder}
          >
            <div>
              <div
                className={
                  msgIsFromUser ? classes.senderMessage : classes.userMessage
                }
              >
                {msg.message}
              </div>
              <div
                className={msgIsFromUser ? classes.receiver : classes.sender}
              >
                {msgIsFromUser ? `${user.firstName} ${user.lastName}` : 'You'}
              </div>
            </div>
          </li>
        );
      })}
    </ul>
  );

  const handleAutoScroll = () => {
    const container = document.querySelector('#message-container');
    if (container) {
      container.scrollTop += 100;
    }
  };

  return (
    <div className={classes.root}>
      <div style={{ flex: '1 1 0%' }}>
        <div className={classes.chatContainer}>
          <div style={{ flex: '1 1 0%' }} />
          <ul style={{ margin: 0, padding: 0 }}>
            <li>
              <div style={{ marginTop: 10, marginBottom: 10 }}>
                {renderMessages()}
              </div>
            </li>
          </ul>
        </div>
        <div style={{ flexShrink: 0 }}>
          <div
            style={{
              backgroundColor: 'white',
              minHeight: 40,
              alignItems: 'stretch',
              borderTop: '1px solid rgb(230, 230, 230)',
              display: 'flex',
            }}
          >
            <div
              style={{
                alignItems: 'center',
                padding: 10,
                flex: '10000 1 0%',
                display: 'flex',
              }}
            >
              <KeyboardEventHandler
                handleKeys={['enter']}
                onKeyEvent={onSendMessage}
                style={{ width: '100%' }}
              >
                <input
                  aria-label="Sendbox"
                  data-id="webchat-sendbox-input"
                  placeholder="Type your message"
                  type="text"
                  onChange={onChange}
                  className={classes.inputBox}
                  readOnly={disabled}
                  value={message}
                />
              </KeyboardEventHandler>
            </div>
            <div>
              <button
                className={classes.sendButton}
                title="Send"
                type="button"
                disabled={disabled}
                onClick={onSendMessage}
              >
                <svg height="28" viewBox="0 0 45.7 33.8" width="28">
                  <path
                    clipRule="evenodd"
                    d="M8.55 25.25l21.67-7.25H11zm2.41-9.47h19.26l-21.67-7.23zm-6 13l4-11.9L5 5l35.7 11.9z"
                  />
                </svg>
              </button>
            </div>
          </div>
        </div>
      </div>

      {handleAutoScroll()}
    </div>
  );
}

const { func, string, object, bool, array } = PropTypes;
NurseChatWindow.propTypes = {
  onSendMessage: func.isRequired,
  onChange: func.isRequired,
  message: string,
  user: object,
  conversation: array,
  disabled: bool,
};

const mapStateToProps = createStructuredSelector({
  patient: makeSelectVerifyAuth(),
  impersonatedPatient: makePatientImpersonation(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(NurseChatWindow);
