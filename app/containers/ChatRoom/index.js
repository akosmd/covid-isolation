/**
 *
 * Chat
 *
 */

import React, { useState, useEffect } from 'react';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { push } from 'connected-react-router';

import { connect } from 'react-redux';
import { compose } from 'redux';

import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import { Grid, CardHeader, Button } from '@material-ui/core';

import 'typeface-lato';

import akosLogo from 'images/akos-logo.png';
import AUCLogo from 'images/auc-logo.png';

import Echo from 'laravel-echo';
import 'pusher-js';

import {
  makeSelectActualPatient,
  makeSelectMemberJoinRoom,
  makeSelectMemberJoinRoomStatus,
  makeSelectChatMessages,
} from 'containers/App/selectors';

import {
  memberJoinRoom,
  sendMessageToNurse,
  setMemberJoiningStatus,
  getChatMessages,
  notifyNurse,
  leaveChatRoom,
  setChatMessagesSize,
} from 'containers/App/actions';

import {
  GOODBYE_MESSAGE,
  GOOGLE_CHAT_URL,
  PROVIDER_PORTAL_URL,
  MEMBER_TYPES,
} from 'utils/config';

import MessageControl from './messageControl';

import getOptions, { USER_TYPES } from './config';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },

  grid: {
    width: '99%',
    margin: '0 auto',
    [theme.breakpoints.down('md')]: {
      height: '95vh',
    },
    [theme.breakpoints.up('md')]: {
      height: '98vh',
    },
  },
  card: {
    minWidth: 375,
    height: 500,
    minHeight: 400,
    position: 'fixed',
    bottom: theme.spacing(10),
    right: theme.spacing(2),
    [theme.breakpoints.down('md')]: {
      minWidth: '96%',
      height: '90%',
      width: '96%',
      right: theme.spacing(1),
    },
    display: 'flex',
    flexDirection: 'column',
  },

  cardFullScreen: {
    display: 'flex',
    height: '100vh',
    flexDirection: 'column',
    alignContent: 'stretch',
    justifyContent: 'stretch',
  },
  cardContent: {
    minHeight: 300,
    width: '100%',
    height: '100%',
  },

  cardHeaderTitle: {
    color: theme.palette.secondary.main,
  },

  cardRoot: {
    background: 'linear-gradient(to right, #55bcbe 0%,#85c3a6 100%) !important',
  },
  cardSubHeader: {
    color: theme.palette.primary.main,
  },
  fullContent: {
    display: 'flex',
    height: '100%',
    overflowY: 'scroll',
    padding: 0,
    paddingBottom: '60px !important',
    [theme.breakpoints.up('md')]: {
      paddingBottom: '0 !important',
    },
  },

  chatAvatar: {
    background: '#12bcc5',
  },
}));

function IndividualChat({
  dispatch,
  memberRoom,
  doJoinRoom,
  patient: {
    data: { patient: patientData, token },
  },
  doSendMessageToNurse,
  doGetChatMessages,
  doSetChatMessageSize,
  doNotifyNurse,
  doLeaveChatroom,
  chatMessages,
}) {
  const classes = useStyles();
  const [conversations, setConversations] = useState([]);
  const [sendToUserId, setSendToUserId] = useState();
  const [message, setMessage] = useState();
  const [hasJoined, setHasJoned] = useState(false);
  const [typing] = useState(false);

  useEffect(() => {
    if (!memberRoom.data && !hasJoined) {
      dispatch(doJoinRoom());

      setHasJoned(true);
    }
  }, []);
  useEffect(() => {
    if (memberRoom.data && patientData) {
      dispatch(doGetChatMessages(memberRoom.data.conference_no));
      dispatch(
        doNotifyNurse({
          url: GOOGLE_CHAT_URL,
          text: `A new member is waiting on the chatroom
      Name: ${patientData.firstName} ${patientData.lastName}
      Visit this link to start chatting ${PROVIDER_PORTAL_URL}chatroom`,
        }),
      );

      joinRoom();
    }
  }, [memberRoom, patientData]);

  useEffect(() => {
    if (memberRoom.data && chatMessages.data && chatMessages.data.length) {
      dispatch(doSetChatMessageSize(0));
      const messages = chatMessages.data.map(chat => ({
        id: chat.from.id,
        from: `${chat.from.first_name} ${chat.from.last_name}`,
        message: chat.message,
      }));
      setConversations(messages);

      if (patientData && patientData.id) {
        const nurseMessage = messages.filter(msg => msg.id !== patientData.id);
        if (nurseMessage.length) {
          setSendToUserId(nurseMessage[nurseMessage.length - 1].id);
        }
      }
    }
  }, [chatMessages.data]);

  useEffect(() => {
    if (patientData) {
      setConversations([
        ...conversations,
        {
          message: `Hi ${
            patientData.firstName
          }! Please wait, our team will be in touch with you shortly`,
        },
      ]);
    }
  }, [patientData]);

  const handleQuit = () => {
    dispatch(doLeaveChatroom(patientData.id));
    setTimeout(() => dispatch(push('/')), 5000);
  };

  const joinRoom = () => {
    const echo = new Echo(getOptions(token, USER_TYPES.member));
    const roomEcho = new Echo(getOptions(token, USER_TYPES.member));
    roomEcho.join('MemberWaitingRoom');

    echo.join(patientData.userChannelName);
    echo.private(patientData.userChannelName).whisper('typing', {
      name: patientData.email,
    });

    echo.private(patientData.userChannelName).listen('UserChatEvent', e => {
      setConversations(prevState => [
        ...prevState,
        { from: e.fullName, message: e.message },
      ]);
      if (e.message === GOODBYE_MESSAGE) handleQuit();
      if (!sendToUserId) setSendToUserId(e.senderId);
    });
  };

  const sendMessage = () => {
    const from = `${patientData.firstName} ${patientData.lastName}`;
    setConversations(prevState => [
      ...prevState,
      { from, id: patientData.id, message },
    ]);

    const params = {
      message,
      conferenceNo: memberRoom.data.conference_no,
      userId: sendToUserId,
    };

    dispatch(doSendMessageToNurse(params));
    setMessage('');
  };

  const onChange = e => {
    const { value } = e.target;

    setMessage(value);
  };

  const disabled = !sendToUserId;
  const logo =
    patientData.membershipTypeId === MEMBER_TYPES.Covid ? AUCLogo : akosLogo;

  const renderFullScreen = () => (
    <div className={classes.root}>
      <Grid
        container
        direction="row"
        justify="space-around"
        alignItems="stretch"
        className={classes.grid}
      >
        <Grid item xs={12}>
          <Card elevation={1} className={classes.cardFullScreen}>
            <CardHeader
              className={classes.cardRoot}
              subheader={
                <div style={{ display: 'flex' }}>
                  <figure
                    style={{
                      display: 'flex',
                      width: 210,
                      height: 35,
                      margin: '0 10px',
                    }}
                  >
                    <img src={logo} alt="Akos" style={{ width: '100%' }} />
                  </figure>
                </div>
              }
              action={
                <Button onClick={() => dispatch(push('/'))}>
                  Back to Home
                </Button>
              }
              classes={{
                title: classes.cardHeaderTitle,
                subheader: classes.cardSubHeader,
              }}
            />
            <CardContent className={classes.fullContent}>
              <MessageControl
                onSendMessage={sendMessage}
                message={message}
                onChange={onChange}
                disabled={disabled}
                messages={conversations}
                typing={typing}
                user={patientData}
              />
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </div>
  );

  return renderFullScreen();
}

const { object, func } = PropTypes;
IndividualChat.propTypes = {
  patient: object.isRequired,
  memberRoom: object.isRequired,
  history: object.isRequired,
  dispatch: func.isRequired,
  doSendMessageToNurse: func.isRequired,
  doSetMemberJoiningStatus: func.isRequired,
  doNotifyNurse: func.isRequired,
  doGetChatMessages: func.isRequired,
  doSetChatMessageSize: func.isRequired,
  doLeaveChatroom: func.isRequired,
  chatMessages: object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  patient: makeSelectActualPatient(),
  memberRoom: makeSelectMemberJoinRoom(),
  memberHasJoinedRoom: makeSelectMemberJoinRoomStatus(),
  chatMessages: makeSelectChatMessages(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doJoinRoom: memberJoinRoom,
    doSendMessageToNurse: sendMessageToNurse,
    doSetMemberJoiningStatus: setMemberJoiningStatus,
    doGetChatMessages: getChatMessages,
    doSetChatMessageSize: setChatMessagesSize,
    doNotifyNurse: notifyNurse,
    doLeaveChatroom: leaveChatRoom,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(IndividualChat);
