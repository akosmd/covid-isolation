/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { classes } from 'istanbul-lib-coverage';
import { Grid } from '@material-ui/core';

import {
  makeSelectVerifyAuth,
  makeSelectFirstTimeLogin,
} from 'containers/App/selectors';

import IsolatedCovid from './isolatedCovid';
import Consent from './consent';

function HomePage({ account, firstTimeLogin }) {
  if (!account.data) return null;

  if (firstTimeLogin) {
    return <Consent />;
  }

  return (
    <div className={classes.root}>
      <Grid container spacing={1} justify="center" alignItems="stretch">
        <Grid item xs={12} xl={10}>
          <IsolatedCovid />
        </Grid>
      </Grid>
    </div>
  );
}

const { object, bool } = PropTypes;
HomePage.propTypes = {
  account: object.isRequired,
  firstTimeLogin: bool,
};

const mapStateToProps = createStructuredSelector({
  account: makeSelectVerifyAuth(),
  firstTimeLogin: makeSelectFirstTimeLogin(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(HomePage);
