import React from 'react';
import PropTypes from 'prop-types';
import { Typography, Button } from '@material-ui/core';

function Instructions({ classes, onClick }) {
  return (
    <div>
      <Typography variant="h5" align="center">
        Self-Isolation Instructions
      </Typography>
      <Typography variant="h6" className={classes.header}>
        Index
      </Typography>
      <Typography>
        <a href="#introduction">Introduction</a>
      </Typography>
      <Typography>
        <a href="#duration"> Duration</a>
      </Typography>
      <Typography>
        <a href="#process">Process</a>
      </Typography>
      <Typography className={classes.index}>
        <a href="#supplies">Supplies</a>
      </Typography>
      <Typography className={classes.index}>
        <a href="#self-monitoring">Self-Monitoring</a>
      </Typography>
      <Typography className={classes.index}>
        <a href="#personal-hygiene">Personal Hygiene</a>
      </Typography>
      <Typography className={classes.index}>
        <a href="#how-to">How To</a>
      </Typography>
      <Typography className={classes.index}>
        <a href="#stress-management">Stress Management</a>
      </Typography>
      <Typography className={classes.index}>
        <a href="#indoor-activities">Indoor Activities</a>
      </Typography>
      <Typography className={classes.index}>
        <a href="#pets">Pets</a>
      </Typography>
      <Typography className={classes.index}>
        <a href="#grocery-shopping">Grocery Shopping</a>
      </Typography>
      <Typography className={classes.index}>
        <a href="#go-doctor">Going to the Doctor</a>
      </Typography>
      <Typography className={classes.index}>
        <a href="#outside-activities">Other Outside Activities</a>
      </Typography>
      <Typography className={classes.index}>
        <a href="#personal-items">Personal and “Family/Community” Items</a>
      </Typography>
      <Typography>
        <a href="#self-isolation">Discontinuing Self-Isolation</a>
      </Typography>
      <Typography variant="h6" className={classes.header} id="introduction">
        Introduction
      </Typography>
      <Typography className={classes.para}>
        Exposure to COVID-19 can occur when we are in close contact with someone
        with the disease. Close contact is being within 6 feet from other
        person(s) and spending a fair amount of time—several minutes or
        longer—socializing with them or “breathing the same air” as it can
        happen in a theater, restaurant, stadium, church, or family gathering,
        etc. Casually passing by someone in the street, a store, or an office
        building, where the “contact” lasted a few seconds without direct
        interaction poses minimal to no risk, unless the other person coughs,
        sneezes or yells while facing you as you’re right next to them and you
        are facing them and “breathing in,” an unlikely scenario. If, on the
        other hand, you are in a park or street walking or jogging behind
        another person, even if you’re more than 6 feet away, and the person in
        front of you coughs or sneezes and you breathe the air “downwind,” your
        exposure is more than just casual although less than during socializing.
      </Typography>
      <Typography className={classes.para}>
        Symptoms of COVID-19 are similar to flu symptoms and include fever,
        cough, shortness of breath, fatigue, body aches, headache, sore throat,
        nausea, vomiting or diarrhea, and occasionally, confusion or other
        neurologic symptoms such as difficulty speaking. Greater than 80 percent
        (this percentage will go up as epidemiologists are able to more
        accurately assess the real incidence of COVID-19 in the population) of
        people who acquire the virus have no or mild symptoms only, and may not
        even be aware that they have the virus. Of those with significant
        symptoms, a proportion will be sick enough to go to the hospital. Of
        those who go to the hospital, some will be admitted to the hospital due
        to the high risk of complications. Of those admitted to the hospital, a
        small group will be transferred to the ICU due to the severity of their
        symptoms. Of those admitted to the ICU, a proportion will need to be on
        a ventilator. Of those needing a ventilator, a significant percentage
        will not make it out of the hospital.
      </Typography>
      <Typography className={classes.para}>
        As you can see, most people who acquire the virus will be okay. The real
        death rate is likely to be significantly lower than what is being
        mentioned now, once true estimates of the incidence is available through
        random population surveillance using antibody testing.
      </Typography>
      <Typography className={classes.para}>
        Unfortunately, a small group of people, especially the elderly and those
        with underlying conditions, and even some healthy young people, come
        down with the serious form of the disease and may not survive.
      </Typography>
      <Typography className={classes.para}>
        Arguably, the most important measure to minimize or even eliminate the
        risk of giving the COVID-19 to others is self-isolation, the extreme
        form of social distancing. Self-isolation is strongly recommended for
        those with known or suspected COVID-19 but not sick, or sick enough to
        go to the hospital, so they don’t pass it to others without the disease.
      </Typography>
      <Typography className={classes.para}>
        Social distancing (keeping oneself at least 6 feet away from others), on
        the other hand, will minimize the chance of those without the disease
        getting the disease if they come near someone with the disease; however,
        it will also decrease the chances of giving the disease to others if a
        person has the disease but is unaware they have it.
      </Typography>
      <Typography className={classes.para}>
        The terms isolation and quarantine are used interchangeably, although
        quarantine implies stricter isolation measures, usually imposed by
        authorities, while under close monitoring.
      </Typography>
      <Typography variant="h6" className={classes.header} id="duration">
        Duration
      </Typography>
      <Typography className={classes.para}>
        Based on the known incubation period (from exposure to development of
        symptoms) of COVID-19, the self-isolation period is 14 days. There are
        rare instances where an infected individual may develop symptoms beyond
        14 days from exposure having remained asymptomatic (without symptoms)
        for 14 days, however, these situations are rare and not expected to be a
        significant factor, therefore, 14 days without symptoms is considered
        enough time for the isolated person to become non-contagious, provided
        they don’t develop symptoms. If they develop symptoms during this
        period, continuation of isolation is necessary (see last topic “
        <a href="#self-isolation">Discontinuing Self-Isolation</a>”).
      </Typography>
      <Typography variant="h6" className={classes.header} id="process">
        Process
      </Typography>
      <Typography className={classes.para}>
        Self-isolation should be done if you fall into one of the following 3
        categories:
        <ul>
          <li>
            You tested positive for COVID-19 but are not sick enough to go to
            the hospital
          </li>
          <li>
            You tested negative; however, you were in close contact with someone
            who tested positive or is highly suspected of having the COVID-19
            based on their symptoms and also exposure history
          </li>
          <li>
            You haven’t been tested but you have a <strong>high risk</strong>{' '}
            exposure history and/or symptoms that are highly suggestive of
            COVID-19 but not severe
          </li>
        </ul>
      </Typography>
      <Typography className={classes.para}>
        If any of the above situation applies to you, self-isolation is strongly
        recommended to minimize the risk of transmission of COVID-19 to others
        without it, especially those with weaker immune systems such as the
        elderly and those with significant comorbidities such as diabetes, high
        blood pressure, heart disease, lung disease, cancer, or those on
        medications for treatment of cancer or that suppress their immune
        system.
      </Typography>
      <Typography variant="h6" className={classes.header} id="supplies">
        Supplies
      </Typography>
      <Typography className={classes.para}>
        <ul>
          <li>
            While self-isolating, you will need certain supplies. Gather the
            items you already own and purchase the items you don’t yet own ASAP.
            The essential supplies are:
            <ul>
              <li>
                Digital thermometer – a simple and inexpensive model (suitable
                for oral temperature) will suffice; your local pharmacy will
                carry them, or it can be ordered online
              </li>
              <li>
                Disinfectant wipes – sufficient quantity of disinfectant wipes
                should be available as you are likely to use them often,
                especially if you need to share home and living quarters with
                family and/or other tenants
              </li>
              <li>Hand sanitizer</li>
              <li>Facial tissue</li>
              <li>Books</li>
              <li>
                Face cover – scarfs, bandanas, commercially available facemasks,
                or anything that will cover your nose and mouth well and stay in
                place, in case you need to go outside; facial cover will provide
                protection to others that may come in contact with you
              </li>
              <li>Smartphone or tablet</li>
              <li>Enough food and drinks</li>
            </ul>
          </li>
        </ul>
      </Typography>
      <Typography variant="h6" className={classes.header} id="self-monitoring">
        Self-monitoring
      </Typography>
      <Typography className={classes.para}>
        <ul>
          <li>
            Be alert for symptoms, especially fever, cough and shortness of
            breath.
          </li>
          <li>
            Take your temperature twice daily 12 hours apart around the same
            time, and also when you feel “feverish,” or if you develop cough
            and/or shortness of breath.
          </li>
          <li>
            Keep a log or a diary of your symptoms with date and time of
            occurrence, duration, and temperature when it was taken and the
            value.
          </li>
          <li>
            Shortness of breath is a serious symptom, especially during the
            COVID-19 pandemic, and you should be seen by a doctor via
            telemedicine if symptoms are mild, or at the ER if symptoms are
            moderate to severe. If shortness of breath is severe, call 911.
          </li>
        </ul>
      </Typography>
      <Typography variant="h6" className={classes.header} id="personal-hygiene">
        Personal Hygiene
      </Typography>
      <Typography className={classes.para}>
        <ul>
          <li>
            Handwashing – wash your hands often, especially after handling
            objects that came from outside (packages, groceries, etc.); wash
            with enough soap to create sufficient foam and rub all surfaces of
            both hands paying particular attention to fingertips (under the
            fingernails), all sides of fingers, web spaces, palms and back of
            the hands. Spend just a little more time on the fingertips/nails,
            fingers and palms as these areas touch objects more often than the
            back of the hands. Make sure the towel is relatively clean (used
            only to dry freshly-washed hands), and wash/replace regularly before
            they get soiled.
          </li>
          <li>
            Refrain from touching the face – this isn’t stressed enough; if we
            never touch the face with contaminated hands, we will eliminate this
            route of infection, which is a significant way of getting an
            infectious disease; make a lifetime habit of only touching the face
            with clean hands at your home, then not touching it, especially when
            away from home.
          </li>
          <li>
            Cough etiquette – if you develop a cough, make a habit of cover your
            mouth and nose by coughing into a tissue (or your shirt sleeve if no
            tissue) and discard the tissue into trash immediately, then wash
            your hands with soap and water for 20 seconds (you may use an
            alcohol-containing hand sanitizer with at least 60% alcohol if
            unable to wash hands).
          </li>
          <li>
            Both handwashing and refraining from touching the face are extremely
            important habits that we all should adopt from here on into the
            future.{' '}
          </li>
        </ul>
      </Typography>
      <Typography variant="h6" className={classes.header} id="how-to">
        How to Self-isolate
      </Typography>
      <Typography className={classes.para}>
        <ul>
          <li>
            If you live alone
            <ul>
              <li>
                No visitors should be allowed to enter your home during the
                isolation period
              </li>
              <li>
                If you have package or food delivery, ask them to leave it in
                front of the door and pick it up as soon as they leave (are at
                least 6 feet from you and walking away)
              </li>
              <li>Observe all personal hygiene recommendations </li>
              <li>
                Maintain a daily routine which will help you stay “level-headed”
                and healthier (more below)
              </li>
              <li>
                Observe strict guidelines if you need to go out of your home
                (see below)
              </li>
            </ul>
          </li>
        </ul>
      </Typography>
      <Typography className={classes.para}>
        <ul>
          <li>
            If you live with family and/or other tenants
            <ul>
              <li>
                Select a <u>room</u> where you will stay most of the time and
                don’t share this space with others
              </li>
              <li>
                If possible, designate one <u>bathroom</u> for your exclusive
                use; if this is not possible, make sure you do not share any
                towels or other bathroom items, and disinfect the areas used
                such as sink faucets, toilet (seat, handle, etc.), and shower
                (any area that you touched) with disinfectant wipes after each
                use
              </li>
              <li>
                <u>Kitchen</u> will need to be shared, therefore, set a schedule
                so different members of the household can use the kitchen at
                different times; at least you and “them” should not use it at
                the same time; give your family members (or other tenants) the
                “right of the way” and let them use it first (whenever
                possible), then use after they vacate the area; after using,
                make sure all utensils, plates, cups and appliances are washed
                with soap and dish detergent, and wipe the kitchen surfaces and
                appliance handles with disinfectant wipes; wear a face cover
                while in the kitchen except when you’re eating
              </li>
              <li>
                Have a <u>designated water bottle</u> that you use daily (wash
                with soap and water at least once daily or replace if
                disposable) and stick with it till the end of the isolation
                period; make sure to drink enough water to stay well-hydrated
              </li>
              <li>
                Do your <u>laundry</u> separately from others (if you have a
                laundry room at home) and make sure you are the only person
                using it at that time, and wipe the surfaces you touched with
                disinfectant wipes after each use; if you normally do the
                laundry in a public laundromat, do NOT go to the laundromat as
                you will expose others to the disease; if possible, hand-wash
                the essentials and cycle through your other clothes for 2 weeks,
                then do the laundry after the isolation period ends
              </li>
              <li>
                Disinfect items such as remote controls and{' '}
                <u>computer keyboard/mouse</u>, if shared with others, as well
                as <u>doorknobs</u> and <u>light switches</u>; it may appear to
                be a chore but will soon become second nature
              </li>
              <li />
            </ul>
          </li>
          <li>
            If someone else (a caregiver) must help you with essential tasks,
            they must wear a face mask and preferably disposable gloves (or wash
            hands thoroughly immediately after assisting you); if they need to
            clean and disinfect the bathroom after you use it, have them wait at
            least 5-10 minutes before cleaning and have them wear a face mask
            and gloves; if you had diarrhea, close the toilet seat lid prior to
            flushing and others must wait at least 30 minutes before using the
            bathroom and they should wear face cover as aerosolized particles
            that could contain the virus could be inhaled.
          </li>
        </ul>
      </Typography>
      <Typography variant="h6" className={classes.header} id="healthy-eating">
        Healthy Eating
      </Typography>
      <Typography className={classes.para}>
        <ul>
          <li>
            Whenever you’re ill with an infection, eating a healthy and balanced
            diet becomes more important to give your body the best chance of
            fighting the infection
          </li>
          <li>Eat vegetables and fruits daily</li>
          <li>
            Decrease or moderate intake of junk food and processed food
            including snacks
          </li>
          <li>
            If you don’t have much of an appetite, eat small but healthy meals;
            small portions of protein shake and concentrated vegetable juice are
            healthier and much easier to digest than regular meals; alternate
            these types of “liquid diet” with other solid food
          </li>
        </ul>
      </Typography>
      <Typography
        variant="h6"
        className={classes.header}
        id="stress-management"
      >
        Stress management
      </Typography>
      <Typography className={classes.para}>
        <ul>
          <li>
            We live in a complex society and stress is a “normal” part of our
            daily lives
          </li>
          <li>
            During an illness, we need to manage the physical stress in addition
            to the stress of being ill and financial uncertainty, let alone many
            other life’s stresses{' '}
          </li>
          <li>
            It is critical that we “take a deep breath” and “take a couple steps
            back,” and try to not get caught up on smaller issues by focusing on
            staying healthy and getting better
          </li>
          <li>
            Think about those who are less fortunate, those who are battling for
            their lives, those who lost loved ones, and be grateful for what you
            have – a positive and grateful thinking will give you a new
            perspective in life and will help you stay healthier
          </li>
          <li>Being thankful is a great healer!</li>
        </ul>
      </Typography>
      <Typography
        variant="h6"
        className={classes.header}
        id="indoor-activities"
      >
        Indoor Activities
      </Typography>
      <Typography className={classes.para}>
        <ul>
          <li>Exercise</li>
          <li>
            <ul>
              <li>
                Please check with your doctor first to make sure your
                comorbidities (if any) allow for the type of exercise you want
                to do
              </li>
              <li>
                If you have no symptoms or mild symptoms only (no shortness of
                breath, chest discomfort or any neurologic symptom) and would
                like to continue your indoor exercise routine, and you have some
                equipment or your routine does not require equipment, you can
                continue your previous routine; if you develop unusual symptoms
                (not previously experienced during similar exercise), stop the
                exercise and call your provider or Akos Nurse Hotline to speak
                with a nurse and a provider ASAP
              </li>
              <li>
                Do NOT start a new strenuous exercise routine while you are sick
              </li>
              <li>
                If you have not exercised in a while and would like to start a
                new exercise routine, you may do so if cleared by your provider
                or by an Akos provider
              </li>
              <li>
                It’s important that you do NOT overdo, therefore, do everything
                in moderation{' '}
              </li>
            </ul>
          </li>
          <li>
            Engage in enjoyable (indoor) activities such as manual hobbies,
            knitting, sewing (many are making cloth face masks and donating it
            to friends and others in need); if you intend to make items for
            others, use a facial cover while doing it (cloth items can be washed
            with water and detergent, therefore, virus particles will be killed)
          </li>
          <li>
            Get plenty of sleep – modern adults are often sleep-deprived; try to
            sleep at least 7-8 hours (more if you can while sick); if you’re
            chronically sleep-deprived due to busy lifestyle, this is an
            opportunity to catch up; sleep strengthens our immune system and
            will make infections less severe and allow you to recover faster
          </li>
          <li>
            Use the opportunity to read a few books and catch up with some other
            intellectual activities you didn’t have the time to do before
          </li>
          <li>
            If you’re fortunate to be able to carry on with your work remotely
            (telecommute), you may continue to work so long as you don’t overdo
            it and leave plenty of time for relaxing activities, some exercise,
            eating and sleeping{' '}
          </li>
          <li>
            Stay informed by checking reliable sites such as
            <a href="https://www.coronavirus.gov/" target="_blank">
              https://www.coronavirus.gov/
            </a>{' '}
            at least once daily
          </li>
          <li>
            Refrain from being glued to the TV, tablet or laptop, and do other
            activities away from electronics – follow a daily schedule
          </li>
          <li>
            Two weeks is a long time if you don’t have anything in particular to
            accomplish, but it will fly by quickly if you’re trying to read a
            few interesting books and/or learn a few new subjects online such as
            astronomy, and keep a “busy” schedule such as reading, eating,
            exercising, watching the news, more reading, another meal, etc.
          </li>
        </ul>
      </Typography>
      <Typography variant="h6" className={classes.header} id="pets">
        Pets
      </Typography>
      <Typography className={classes.para}>
        <ul>
          <li>
            If you have pets, do your best to have someone take care of it until
            after the isolation period. If you have a dog and no one else can
            take care of it, wear a face cover while you’re around the dog.{' '}
          </li>
          <li>
            According to the CDC, “At this time, there is no evidence that pets
            can spread the virus that causes COVID-19 to people or that they
            might be a source of infection in the United States.”
            <a
              href="https://www.cdc.gov/coronavirus/2019-ncov/daily-life-coping/animals.html"
              target="_blank"
            >
              https://www.cdc.gov/coronavirus/2019-ncov/daily-life-coping/animals.html
            </a>
          </li>
          <li>
            If you must walk the dog, observe social distancing guidelines
            strictly (at least 6 feet away from anyone else) and wear a facial
            cover.{' '}
          </li>
        </ul>
      </Typography>
      <Typography variant="h6" className={classes.header} id="grocery-shopping">
        Grocery Shopping
      </Typography>
      <Typography className={classes.para}>
        <ul>
          <li>
            It is likely that you will need to go out to buy groceries (unless
            you can arrange for delivery by the store or family/friends). When
            leaving for groceries, strictly follow the guidance below:
            <ul>
              <li>
                Wear a face cover as your respiratory secretions will be
                infectious and you could potentially transmit the disease to
                others just by talking near them
              </li>
              <li>
                Be brief with grocery shopping and return to your home promptly
                and keep the face cover until you are inside your home
              </li>
              <li>
                Once inside your home, unpack the groceries, wash what can be
                washed, disinfect what should be disinfected, and wash your
                hands thoroughly to minimize catching any other disease
              </li>
              <li>
                Always keep at least 6 feet distance from anyone else whenever
                possible; passing by someone quickly is okay as long as there’s
                no interaction, coughing, sneezing or talking
              </li>
            </ul>
          </li>
          <li>
            If you run into someone you know in the grocery store, make sure you
            keep at least 6 feet of distance and tell then you will call them
            later
          </li>
        </ul>
      </Typography>
      <Typography variant="h6" className={classes.header} id="go-doctor">
        Going to the Doctor
      </Typography>
      <Typography className={classes.para}>
        <ul>
          <li>
            If you develop the following symptoms, you MUST seek medical
            attention ASAP:
          </li>
          <li>
            <ul>
              <li>Shortness of breath</li>
              <li>Chest pain or pressure</li>
              <li>Confusion or difficulty speaking</li>
              <li>Bluish discoloration of your lips</li>
              <li>
                If you are sick and don’t feel like you can drive yourself to
                the hospital, call Akos Nurse Hotline and let the nurse know
                that you have the COVID-19 and are having serious symptoms
                (describe your symptoms) so they can call 911 on your behalf
              </li>
              <li>
                Make sure you cover your mouth and nose with a face cover before
                help arrives
              </li>
              <li>
                Call ahead if you’re driving yourself to the doctor or hospital
                and let them know that you have the COVID-19 so they’re prepared
                when you arrive
              </li>
            </ul>
          </li>
        </ul>
      </Typography>
      <Typography
        variant="h6"
        className={classes.header}
        id="outside-activities"
      >
        Other Outside Activities
      </Typography>
      <Typography className={classes.para}>
        <ul>
          <li>
            Do NOT go outside for a walk, to jog, or sit in the park where you
            could come in contact with others who may not have the disease and
            you could potentially expose them. You wouldn’t want someone with
            the disease to put you at risk if you didn’t have it. So, be
            considerate and stay home except for absolutely essential activities
            such as going grocery shopping or going to the doctor
          </li>
        </ul>
      </Typography>
      <Typography variant="h6" className={classes.header} id="personal-items">
        Personal and “Family/Community” Items{' '}
      </Typography>
      <Typography className={classes.para}>
        <ul>
          <li>
            Do your best not to share personal or community items during your
            self-isolation period. Items such as smartphones, tablets, laptops,
            or desktop keyboard/mouse or remote controls should be wiped with
            disinfectant wipes if they need to be shared.
          </li>
        </ul>
      </Typography>
      <Typography variant="h6" className={classes.header} id="references">
        References:
      </Typography>
      <Typography className={classes.para}>
        <a
          href="https://www.cdc.gov/coronavirus/2019-ncov/if-you-are-sick/steps-when-sick.html"
          target="_blank"
        >
          https://www.cdc.gov/coronavirus/2019-ncov/if-you-are-sick/steps-when-sick.html
        </a>
      </Typography>
      <Typography variant="h6" className={classes.header} id="self-isolation">
        Discontinuing Self-Isolation
      </Typography>
      <Typography className={classes.para}>
        Home isolation can be stopped after the 14-day period if either of the
        following situations apply:
        <ul>
          <li>
            If <u>you will NOT have another test</u> to determine whether you
            still have the virus, all 3 conditions below MUST be met:
            <ul>
              <li>
                You had no fever for at least 72 hours (3 full days) without the
                use of any medication that lowers the fever such as Tylenol or
                Motrin
              </li>
              <li>
                If you had symptoms, they significantly improved or resolved
              </li>
              <li>
                At least 7 days have passed since you first developed symptoms{' '}
              </li>
            </ul>
          </li>
          <li>
            If <u>you will have another test</u> to determine whether you are
            still contagious, the following 3 conditions MUST be met:
            <ul>
              <li>
                You no longer have a fever without any medications that lowers
                the fever such as Tylenol or Motrin
              </li>
              <li>
                If you had symptoms, they significantly improved or resolved
              </li>
              <li>You had 2 negative tests 24 hours apart</li>
            </ul>
          </li>
        </ul>
      </Typography>
      <Typography className={classes.para}>
        If questions, please call our hotline to speak with a nurse or provider
        for clarification and further guidance.
      </Typography>
      <Typography className={classes.para}>
        <a
          href="https://www.cdc.gov/coronavirus/2019-ncov/hcp/disposition-in-home-patients.html"
          target="_blank"
        >
          https://www.cdc.gov/coronavirus/2019-ncov/hcp/disposition-in-home-patients.html
        </a>
      </Typography>
      <Button onClick={onClick} variant="contained" color="primary">
        Continue to Consent
      </Button>
    </div>
  );
}

Instructions.propTypes = {
  classes: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired,
};
export default Instructions;
