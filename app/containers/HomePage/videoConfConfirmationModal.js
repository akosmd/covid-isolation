import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { push } from 'connected-react-router';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { Button } from '@material-ui/core';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';

import {
  makeSelectVerifyAuth,
  makeSelectVideoConfStatus,
  makeSelectClickedVideoConfConfirmation,
} from 'containers/App/selectors';
import {
  getVideoConfStatus,
  setVideoConfConfirmation,
} from 'containers/App/actions';

function VideoConfConfirmation({
  account,
  videoConfStatus,
  dispatch,
  doGetVideoConfStatus,
  doSetVideoConfConfirmation,
  clickedVideoConfConfirmation,
}) {
  const [open, setOpen] = useState(false);

  const roomsPrefix = '/video-conference';
  const rooms = {
    70002: `${roomsPrefix}/vpdc`,
    70006: `${roomsPrefix}/agileurgentcare`,
    default: `${roomsPrefix}/medical`,
  };

  useEffect(() => {
    if (account.data) {
      dispatch(doGetVideoConfStatus(account.data.patient.patientLegacyId));
    }
  }, [account]);

  useEffect(() => {
    if (videoConfStatus.data) {
      setOpen(true);
    }
  }, [videoConfStatus]);

  const handleConfirm = accepted => {
    setOpen(false);
    if (accepted) {
      dispatch(
        push(rooms[account.data.patient.membershipTypeId] || rooms.default),
      );
    }
    dispatch(doSetVideoConfConfirmation());
  };

  if (clickedVideoConfConfirmation) return null;

  return (
    <Dialog
      open={open}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">Go to Video Call</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          We noticed that you were not able to finish the last call. Do you want
          to resume?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => handleConfirm(false)}>No</Button>
        <Button color="primary" onClick={() => handleConfirm(true)}>
          Yes
        </Button>
      </DialogActions>
    </Dialog>
  );
}

const { object, bool, func } = PropTypes;
VideoConfConfirmation.propTypes = {
  account: object.isRequired,
  videoConfStatus: object.isRequired,
  clickedVideoConfConfirmation: bool.isRequired,
  dispatch: func.isRequired,
  doGetVideoConfStatus: func.isRequired,
  doSetVideoConfConfirmation: func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  account: makeSelectVerifyAuth(),
  videoConfStatus: makeSelectVideoConfStatus(),
  clickedVideoConfConfirmation: makeSelectClickedVideoConfConfirmation(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doGetVideoConfStatus: getVideoConfStatus,
    doSetVideoConfConfirmation: setVideoConfConfirmation,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(VideoConfConfirmation);
