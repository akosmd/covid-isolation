import React, { useState, useEffect } from 'react';

import PropTypes from 'prop-types';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { compose } from 'redux';

import AkosCard from 'components/AkosCard';

import ProviderIcon from 'components/icons/provider';
import { Grid } from '@material-ui/core';

const dashboard = ({ dispatch }) => (
  <Grid container spacing={2} alignItems="stretch">
    <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
      <AkosCard
        title="Chat"
        icon={ProviderIcon}
        onClick={() => dispatch(push('/chat'))}
      />
    </Grid>
  </Grid>
);

const { func, object } = PropTypes;
dashboard.propTypes = {
  dispatch: func.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  null,
  mapDispatchToProps,
);

export default compose(withConnect)(dashboard);
