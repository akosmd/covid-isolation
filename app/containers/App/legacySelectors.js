import { createSelector } from 'reselect';

import { initialState } from './legacyReducer';
const portalSelector = state => state.legacy || initialState;

const makeSelectInsuranceEligibility = () =>
  createSelector(
    portalSelector,
    subState => subState.insuranceEligibility,
  );

const makeSelectPatientInsurance = () =>
  createSelector(
    portalSelector,
    subState => subState.patientInsurance,
  );

const makeSelectInsuranceList = () =>
  createSelector(
    portalSelector,
    subState => subState.insuranceList,
  );

const makeSelectSavedPatientInsurance = () =>
  createSelector(
    portalSelector,
    subState => subState.savedPatientInsurance,
  );

const makeSelectCouponCode = () =>
  createSelector(
    portalSelector,
    subState => subState.couponCode,
  );

const makeSelectBrainTreeToken = () =>
  createSelector(
    portalSelector,
    subState => subState.brainTreeToken,
  );
const makeSelectPaymentAmount = () =>
  createSelector(
    portalSelector,
    subState => subState.paymentAmount,
  );
const makeSelectPaymentCheckout = () =>
  createSelector(
    portalSelector,
    subState => subState.paymentCheckout,
  );
const makeSelectPatientCallId = () =>
  createSelector(
    portalSelector,
    subState => subState.patientCallId,
  );
const makeSelectAddUserToWaitingRoom = () =>
  createSelector(
    portalSelector,
    subState => subState.addUserToWaitingRoom,
  );
const makeSelectNotifyProviders = () =>
  createSelector(
    portalSelector,
    subState => subState.notifyProviders,
  );
const makeSelectWaitingRoomStatus = () =>
  createSelector(
    portalSelector,
    subState => subState.waitingRoomStatus,
  );
const makeSelectOpenTokRoomKeys = () =>
  createSelector(
    portalSelector,
    subState => subState.openTokRoomKeys,
  );
const makeSelectDocAlias = () =>
  createSelector(
    portalSelector,
    subState => subState.docAlias,
  );

const makeSelectCallEndByPatient = () =>
  createSelector(
    portalSelector,
    subState => subState.callEndByPatient,
  );

const makeSelectMedication = () =>
  createSelector(
    portalSelector,
    subState => subState.medications,
  );

const makeSelectAllergies = () =>
  createSelector(
    portalSelector,
    subState => subState.allergies,
  );

const makeSelectMedicationList = () =>
  createSelector(
    portalSelector,
    subState => subState.medicationList,
  );

const makeSelectAllegyList = () =>
  createSelector(
    portalSelector,
    subState => subState.allergyList,
  );
const makeSelectMedicalConditionList = () =>
  createSelector(
    portalSelector,
    subState => subState.medicalConditionList,
  );
const makeSelectSymptom = () =>
  createSelector(
    portalSelector,
    subState => subState.symptom,
  );

export {
  makeSelectInsuranceEligibility,
  makeSelectPatientInsurance,
  makeSelectInsuranceList,
  makeSelectSavedPatientInsurance,
  makeSelectCouponCode,
  makeSelectBrainTreeToken,
  makeSelectPaymentAmount,
  makeSelectPaymentCheckout,
  makeSelectPatientCallId,
  portalSelector,
  makeSelectAddUserToWaitingRoom,
  makeSelectNotifyProviders,
  makeSelectWaitingRoomStatus,
  makeSelectOpenTokRoomKeys,
  makeSelectDocAlias,
  makeSelectCallEndByPatient,
  makeSelectMedication,
  makeSelectAllergies,
  makeSelectMedicationList,
  makeSelectAllegyList,
  makeSelectMedicalConditionList,
  makeSelectSymptom,
};
