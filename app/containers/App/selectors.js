import { createSelector } from 'reselect';

import { initialState } from './reducer';
const portalSelector = state => state.patient || initialState;

const selectRouter = state => state.router;

const makeSelectLocation = () =>
  createSelector(
    selectRouter,
    routerState => routerState.location,
  );

const makeSelectSignin = () =>
  createSelector(
    portalSelector,
    subState => subState.signin,
  );

const makeSelectVerificationCode = () =>
  createSelector(
    portalSelector,
    subState => subState.verification,
  );

const makeSelectInxiteUrl = () =>
  createSelector(
    portalSelector,
    subState => subState.inxiteUrl,
  );

const makeNIHSearchResult = () =>
  createSelector(
    portalSelector,
    subState => subState.nihSearchResult,
  );

const makeSelectCheckBenefits = () =>
  createSelector(
    portalSelector,
    subState => subState.checkBenefits,
  );

const makeSelectRegister = () =>
  createSelector(
    portalSelector,
    subState => subState.register,
  );

const makeAccountVerification = () =>
  createSelector(
    portalSelector,
    subState => subState.accountVerification,
  );
const makeSelectVerifyAuth = () =>
  createSelector(
    portalSelector,
    subState => subState.verifyAuth,
  );

const makeSelectPatientDetail = () =>
  createSelector(
    portalSelector,
    subState => subState.patientDetail,
  );

const makeSelectAppointment = () =>
  createSelector(
    portalSelector,
    subState => subState.appointment,
  );

const makeSelectUpdatedPatient = () =>
  createSelector(
    portalSelector,
    subState => subState.updatedPatient,
  );

const makeSelectUpdatedPatientComms = () =>
  createSelector(
    portalSelector,
    subState => subState.updatePatientComms,
  );
const makeSelectChangePassword = () =>
  createSelector(
    portalSelector,
    subState => subState.changePassword,
  );

const makeSelectInxiteSso = () =>
  createSelector(
    portalSelector,
    subState => subState.inxiteSso,
  );

const makeSelectAdvinowSso = () =>
  createSelector(
    portalSelector,
    subState => subState.advinowSso,
  );
const makeIsSentToEmail = () =>
  createSelector(
    portalSelector,
    subState => subState.sentToEmail,
  );

const makePatientDependents = () =>
  createSelector(
    portalSelector,
    subState => subState.patientDependents,
  );

const makeShowDependentsModal = () =>
  createSelector(
    portalSelector,
    subState => subState.showDependentsModal,
  );

const makePatientImpersonation = () =>
  createSelector(
    portalSelector,
    subState => subState.patientImpersonation,
  );
const makeSelectActualPatient = () =>
  createSelector(
    portalSelector,
    subState =>
      subState.patientImpersonation.data
        ? subState.patientImpersonation
        : subState.verifyAuth,
  );

const selectAppVersion = () =>
  createSelector(
    portalSelector,
    subState => subState.appVersion,
  );

const makeSelectFamilyInvite = () =>
  createSelector(
    portalSelector,
    subState => subState.familyInvite,
  );

const makeSelectAddDependent = () =>
  createSelector(
    portalSelector,
    subState => subState.addDependent,
  );

const makeSelectChatbotAuth = () =>
  createSelector(
    portalSelector,
    subState => subState.chatbotAuth,
  );

const makeSelectChatWaitingInRoom = () =>
  createSelector(
    portalSelector,
    subState => subState.chatWaitingRoom,
  );

const makeSelectNurseSignin = () =>
  createSelector(
    portalSelector,
    subState => subState.nurseSignin,
  );

const makeSelectMessageToMember = () =>
  createSelector(
    portalSelector,
    subState => subState.messageToMember,
  );

const makeSelectMemberJoinRoom = () =>
  createSelector(
    portalSelector,
    subState => subState.memberJoinRoom,
  );
const makeSelectMemberJoinRoomStatus = () =>
  createSelector(
    portalSelector,
    subState => subState.memberJoinRoomStatus,
  );
const makeSelectedUserToChat = () =>
  createSelector(
    portalSelector,
    subState => subState.selectedUserToChat,
  );
const makeSelecteNurseConverations = () =>
  createSelector(
    portalSelector,
    subState => subState.nurseConversations,
  );
const makeSelectFamilyMembers = () =>
  createSelector(
    portalSelector,
    subState => subState.familyMembers,
  );

const makeSelectApproveFamilyMember = () =>
  createSelector(
    portalSelector,
    subState => subState.approveFamilyMember,
  );

const makeSelectChatMessages = () =>
  createSelector(
    portalSelector,
    subState => subState.chatMessages,
  );

const makeSelectMemberInsuranceInfoList = () =>
  createSelector(
    portalSelector,
    subState => subState.memberInsuranceInfoList,
  );

const makeSelectMemberInsuranceInfo = () =>
  createSelector(
    portalSelector,
    subState => subState.memberInsuranceInfo,
  );

const makeSelectAddMemberInsuranceInfo = () =>
  createSelector(
    portalSelector,
    subState => subState.addMemberInsuranceInfo,
  );

const makeSelectUpdateMemberInsuranceInfo = () =>
  createSelector(
    portalSelector,
    subState => subState.updateMemberInsuranceInfo,
  );

const makeSelectVerifySsn = () =>
  createSelector(
    portalSelector,
    subState => subState.verifySsn,
  );

const makeSelectGiMessage = () =>
  createSelector(
    portalSelector,
    subState => subState.giMessage,
  );

const makeSelectCovidMessage = () =>
  createSelector(
    portalSelector,
    subState => subState.covidMessage,
  );

const makeSelectFindPatient = () =>
  createSelector(
    portalSelector,
    subState => subState.findPatient,
  );

const makeSelectAdvinowPatient = () =>
  createSelector(
    portalSelector,
    subState => subState.advinowPatient,
  );

const makeSelectUserFromMedClinic = () =>
  createSelector(
    portalSelector,
    subState => subState.userInMedClinic,
  );
const makeSelectVideoConfStatus = () =>
  createSelector(
    portalSelector,
    subState => subState.videoConfStatus,
  );
const makeSelectClickedVideoConfConfirmation = () =>
  createSelector(
    portalSelector,
    subState => subState.clickedVideoConfConfirmation,
  );
const makeSelectBraintreePlans = () =>
  createSelector(
    portalSelector,
    subState => subState.braintreePlans,
  );

const makeSelectSubscribeBraintree = () =>
  createSelector(
    portalSelector,
    subState => subState.subscribeBraintree,
  );

const makeSelectBraintreeToken = () =>
  createSelector(
    portalSelector,
    subState => subState.braintreeToken,
  );
const makeSelectBraintreeSubscriptionStatus = () =>
  createSelector(
    portalSelector,
    subState => subState.braintreeSubscriptionStatus,
  );
const makeSelectBraintreeCheckout = () =>
  createSelector(
    portalSelector,
    subState => subState.braintreeCheckout,
  );

const makeSelectIdImage = () =>
  createSelector(
    portalSelector,
    subState => subState.idImage,
  );
const makeSelectInsuranceImage = () =>
  createSelector(
    portalSelector,
    subState => subState.insuranceImage,
  );
const makeSelectGetSavedImages = () =>
  createSelector(
    portalSelector,
    subState => subState.savedImages,
  );
const makeSelectUnauthorizedPath = () =>
  createSelector(
    portalSelector,
    subState => subState.unauthorizedPath,
  );

const makeSelectAppError = () =>
  createSelector(
    portalSelector,
    subState => subState.appError,
  );

const makeSelectFirstTimeLogin = () =>
  createSelector(
    portalSelector,
    subState => subState.firstTimeLogin,
  );
export {
  makeSelectLocation,
  portalSelector,
  makeSelectSignin,
  makeSelectVerificationCode,
  makeSelectInxiteUrl,
  makeNIHSearchResult,
  makeSelectCheckBenefits,
  makeSelectRegister,
  makeAccountVerification,
  makeSelectVerifyAuth,
  makeSelectPatientDetail,
  makeSelectAppointment,
  makeSelectUpdatedPatient,
  makeSelectChangePassword,
  makeSelectInxiteSso,
  makeIsSentToEmail,
  makePatientDependents,
  makeShowDependentsModal,
  makePatientImpersonation,
  selectAppVersion,
  makeSelectFamilyInvite,
  makeSelectAddDependent,
  makeSelectChatbotAuth,
  makeSelectChatWaitingInRoom,
  makeSelectNurseSignin,
  makeSelectMessageToMember,
  makeSelectMemberJoinRoom,
  makeSelectMemberJoinRoomStatus,
  makeSelectedUserToChat,
  makeSelecteNurseConverations,
  makeSelectFamilyMembers,
  makeSelectApproveFamilyMember,
  makeSelectChatMessages,
  makeSelectActualPatient,
  makeSelectMemberInsuranceInfoList,
  makeSelectMemberInsuranceInfo,
  makeSelectAddMemberInsuranceInfo,
  makeSelectUpdateMemberInsuranceInfo,
  makeSelectVerifySsn,
  makeSelectUpdatedPatientComms,
  makeSelectGiMessage,
  makeSelectCovidMessage,
  makeSelectFindPatient,
  makeSelectAdvinowPatient,
  makeSelectUserFromMedClinic,
  makeSelectAdvinowSso,
  makeSelectVideoConfStatus,
  makeSelectClickedVideoConfConfirmation,
  makeSelectBraintreePlans,
  makeSelectSubscribeBraintree,
  makeSelectBraintreeToken,
  makeSelectBraintreeSubscriptionStatus,
  makeSelectBraintreeCheckout,
  makeSelectIdImage,
  makeSelectInsuranceImage,
  makeSelectGetSavedImages,
  makeSelectUnauthorizedPath,
  makeSelectAppError,
  makeSelectFirstTimeLogin,
};
