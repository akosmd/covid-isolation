/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';

import AkosPage from 'components/AkosPage';
import AuthenticatedRoute from 'components/AuthenticatedRoute';

import HomePage from 'containers/HomePage/Loadable';
import VerifyCode from 'containers/CodeVerification';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import Login from 'containers/Login/Loadable';
import AccountVerification from 'containers/AccountVerification';
import TwoFactor from 'containers/2FA';
import ChangePassword from 'containers/ChangePassword';

import Chat from 'containers/Chat';

import {
  makeSelectSignin,
  makeSelectVerifyAuth,
  makeAccountVerification,
} from 'containers/App/selectors';

import GlobalStyle from '../../global-styles';

function App(props) {
  const { signin, auth, history, accountVerification } = props;
  const [isAuthenticated, setAuthenticated] = useState(
    auth.data && auth.data !== false,
  );
  const [isAccountVerified, setAccountVerified] = useState(false);

  useEffect(() => {
    const { runtime } = props;
    if (
      process.env.NODE_ENV === 'production' ||
      process.env.NODE_ENV === 'staging'
    ) {
      runtime.install({
        onUpdating: () => {
          // eslint-disable-next-line no-console
          console.log('SW Event:', 'updating resources');
        },
        onUpdateReady: () => {
          // eslint-disable-next-line no-console
          console.log('SW Event:', 'applying file updates');

          runtime.applyUpdate();
        },
        onUpdated: () => {
          // eslint-disable-next-line no-console
          console.log('SW Event:', 'resources updated successfully');
        },
        onUpdateFailed: () => {
          // eslint-disable-next-line no-console
          console.log(
            'SW Event:',
            'an error occured while trying to update resources',
          );
        },
      });
    }
  }, []);

  useEffect(() => {
    setAuthenticated(auth.data && auth.data !== false);
  }, [auth]);

  useEffect(() => {
    setAccountVerified(
      accountVerification.data && accountVerification.data !== undefined,
    );
  }, [accountVerification]);
  return (
    <div>
      {/* <Router> */}
      <Switch>
        <AuthenticatedRoute
          isAuthenticated={isAuthenticated}
          exact
          path="/"
          unauthorizedPath="/login"
          component={() => <AkosPage component={HomePage} history={history} />}
        />

        <AuthenticatedRoute
          isAuthenticated={isAccountVerified}
          path="/verify-code"
          component={() => (
            <AkosPage component={VerifyCode} history={history} />
          )}
        />

        <AuthenticatedRoute
          isAuthenticated={isAuthenticated}
          path="/chat"
          component={Chat}
        />
        <Route
          path="/login"
          component={() => <AkosPage component={Login} history={history} />}
        />

        <AuthenticatedRoute
          isAuthenticated={signin.data}
          path="/2fa"
          exact
          component={() => <AkosPage component={TwoFactor} history={history} />}
        />
        <AuthenticatedRoute
          isAuthenticated={signin.data}
          path="/2fa/:id"
          component={() => <AkosPage component={TwoFactor} history={history} />}
        />
        <Route
          path="/account-verification"
          exact
          component={() => (
            <AkosPage component={AccountVerification} history={history} />
          )}
        />

        <AuthenticatedRoute
          isAuthenticated={isAuthenticated}
          path="/change-password"
          component={() => (
            <AkosPage component={ChangePassword} history={history} />
          )}
        />

        <Route component={NotFoundPage} />
      </Switch>
      <GlobalStyle />
      {/* </Router> */}
    </div>
  );
}

const { object } = PropTypes;

App.propTypes = {
  signin: object,
  auth: object,
  accountVerification: object,
  history: object,
};

const mapStateToProps = createStructuredSelector({
  auth: makeSelectVerifyAuth(),
  signin: makeSelectSignin(),
  accountVerification: makeAccountVerification(),
});

const mapDispatchToProps = dispatch => ({
  dispatch,
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(App);
