import { takeLatest, call, put, select } from 'redux-saga/effects';
import { restApi, api, newApi, connectApi } from 'utils/api';
import { restapiBasicAuth } from 'utils/config';

import { apiResponseEvaluator } from 'utils/sagaHelper';

import { makeSelectSignin } from 'containers/App/selectors';

import { logAppAppError } from './actions';

import {
  GET_INSURANCE_ELIGIBILITY,
  GET_PATIENT_INSURANCE,
  FIND_INSURANCE,
  SAVE_PATIENT_INSURANCE,
  COUPON_CODE,
  BRAINTREE_TOKEN,
  PAYMENT_CHECKOUT,
  GENERATE_PATIENT_CALL_ID,
  UPDATE_PATIENT_CALL_ID,
  ADD_USER_TO_WAITING_ROOM,
  NOTIFY_PROVIDERS,
  CHECK_WAITING_ROOM_STATUS,
  GET_OPENTOK_ROOM_KEYS,
  GET_DOC_ALIAS,
  CALL_END_BY_PATIENT,
  INVALIDATE_TOKEN,
  PROVIDER_SETTINGS,
  DISCONNECT_REASON,
  GET_MEDICATIONS,
  GET_ALERGIES,
  SAVE_PAYMENT_TO_PS,
  SAVE_PATIENT_TO_PS,
  SAVE_FPI,
  SAVE_PATIENT_TO_HG,
  SAVE_PATIENT_INSURANCE_TO_HG,
} from './legacyConstants';

import {
  getInsuranceEligibilitySuccess,
  getInsuranceEligibilityFailure,
  getPatientInsuranceSuccess,
  getPatientInsuranceFailure,
  findInsuranceSuccess,
  findInsuranceFailure,
  savePatientInsuranceSuccess,
  savePatientInsuranceFailure,
  checkCouponCodeSuccess,
  checkCouponCodeFailure,
  getBrainTreeTokenSuccess,
  getBrainTreeTokenFailure,
  checkoutBraintreeSuccess,
  checkoutBraintreeFailure,
  generatePatientCallIdSuccess,
  generatePatientCallIdFailure,
  addUserToWaitingRoomSuccess,
  addUserToWaitingRoomFailure,
  notifyProvidersSuccess,
  notifyProvidersFailure,
  checkWaitingRoomStatusSuccess,
  checkWaitingRoomStatusFailure,
  getOpenTokRoomKeysSuccess,
  getOpenTokRoomKeysFailure,
  getDocAliasSuccess,
  getDocAliasFailure,
  updatePatientCallIdSuccess,
  updatePatientCallIdFailure,
  callEndByPatientSuccess,
  callEndByPatientFailure,
  invalidateTokenSuccess,
  invalidateTokenFailure,
  providerSettingsSuccess,
  providerSettingsFailure,
  disconnectReasonSuccess,
  disconnectReasonFailure,
  getMedicationsSuccess,
  getMedicationsFailure,
  getAllergiesSuccess,
  getAllergiesFailure,
  savePaymentToPsSuccess,
  savePaymentToPsFailure,
  saveInsuraceToPsSuccess,
  saveInsuraceToPsFailure,
  saveFPISuccess,
  saveFPIFailure,
  savePatientToHGSuccess,
  savePatientToHGFailure,
  savePatientInsuranceToHGSuccess,
  savePatientInsuranceToHGFailure,
} from './legacyActions';

function* getVerifyToken() {
  const auth = yield select(makeSelectSignin());

  return auth.data.newapi_token;
}

const doGetInsuranceEligibility = ({ payload, token }) =>
  restApi.post(`/v1/eligibility`, payload, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-AUTH-TOKEN': token,
      Authorization: `Basic ${btoa(
        `${restapiBasicAuth.username}:${restapiBasicAuth.password}`,
      )}`,
    },
  });

export function* getInsuranceEligibilitySaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doGetInsuranceEligibility, { payload, token });
    yield apiResponseEvaluator(
      response,
      getInsuranceEligibilitySuccess,
      getInsuranceEligibilityFailure,
    );
  } catch (ex) {
    yield put(getInsuranceEligibilityFailure(ex));
  }
}

const doGetPatientInsurance = ({ payload, token }) =>
  api.get(`/patient-insurance/get-patient-insurance/${payload}`, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* getPatientInsuranceSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doGetPatientInsurance, { payload, token });
    yield apiResponseEvaluator(
      response,
      getPatientInsuranceSuccess,
      getPatientInsuranceFailure,
    );
  } catch (ex) {
    yield put(getPatientInsuranceFailure(ex));
  }
}
const doFindInsurance = ({ payload, token }) =>
  api.get(`/patient-insurance/get-copay-amount/${payload}`, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* findInsuranceSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doFindInsurance, { payload, token });
    yield apiResponseEvaluator(
      response,
      findInsuranceSuccess,
      findInsuranceFailure,
    );
  } catch (ex) {
    yield put(findInsuranceFailure(ex));
  }
}
const doSavePatientInsurance = ({ payload, token }) =>
  api.post(`/InsuranceController/savePatientInsurance`, payload, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-AUTH-TOKEN': token,
    },
  });

export function* savePatientInsuranceSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doSavePatientInsurance, { payload, token });
    yield apiResponseEvaluator(
      response,
      savePatientInsuranceSuccess,
      savePatientInsuranceFailure,
    );
  } catch (ex) {
    yield put(savePatientInsuranceFailure(ex));
  }
}
const doCheckCouponCode = ({ payload, token }) =>
  api.post(`/user/isvalidpromocode`, payload, {
    headers: {
      'Content-Type': 'multipart/form-data',
      'X-AUTH-TOKEN': token,
    },
  });

export function* checkCouponCodeSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doCheckCouponCode, { payload, token });
    yield apiResponseEvaluator(
      response,
      checkCouponCodeSuccess,
      checkCouponCodeFailure,
    );
  } catch (ex) {
    yield put(checkCouponCodeFailure(ex));
  }
}

const doGetBrainTreeToken = ({ token }) =>
  connectApi.post(`/braintree/client_token`, undefined, {
    headers: {
      'Content-Type': 'multipart/form-data',
      'X-AUTH-TOKEN': token,
    },
  });

export function* getBrainTreeTokenSaga() {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doGetBrainTreeToken, { token });
    yield apiResponseEvaluator(
      response,
      getBrainTreeTokenSuccess,
      getBrainTreeTokenFailure,
    );
  } catch (ex) {
    yield put(getBrainTreeTokenFailure(ex));
  }
}
const doBraintreeCheckout = ({ payload, token }) =>
  connectApi.post(`/braintree/checkout`, payload, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* braintreeCheckoutSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doBraintreeCheckout, { payload, token });
    yield apiResponseEvaluator(
      response,
      checkoutBraintreeSuccess,
      checkoutBraintreeFailure,
    );
  } catch (ex) {
    yield put(checkoutBraintreeFailure(ex));
  }
}
const doGeneratePatientCallId = ({ payload, token }) =>
  connectApi.post(`/api/pcp/generateCallIdByPatientId`, payload, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* generatePatientCallIdSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doGeneratePatientCallId, { payload, token });
    yield apiResponseEvaluator(
      response,
      generatePatientCallIdSuccess,
      generatePatientCallIdFailure,
    );
  } catch (ex) {
    yield put(generatePatientCallIdFailure(ex));
  }
}
const doUpdatePatientCallId = ({ payload, token }) =>
  connectApi.post(`/api/pcp/updatedataBycallId`, payload, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* updatePatientCallIdSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doUpdatePatientCallId, { payload, token });
    yield apiResponseEvaluator(
      response,
      updatePatientCallIdSuccess,
      updatePatientCallIdFailure,
    );
  } catch (ex) {
    yield put(updatePatientCallIdFailure(ex));
  }
}
const doAddUserToWaitingRoom = ({ payload, token }) =>
  connectApi.post(`/api/pcp/addUserToWaiting`, payload, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* addUserToWaitingRoomSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doAddUserToWaitingRoom, { payload, token });
    yield apiResponseEvaluator(
      response,
      addUserToWaitingRoomSuccess,
      addUserToWaitingRoomFailure,
    );
  } catch (ex) {
    yield put(addUserToWaitingRoomFailure(ex));
  }
}
const doCheckWaitingRoomStatus = ({ payload, token }) =>
  connectApi.post(`/api/pcp/checkstatusinwatingroom`, payload, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* checkWaitingRoomStatusSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doCheckWaitingRoomStatus, { payload, token });
    yield apiResponseEvaluator(
      response,
      checkWaitingRoomStatusSuccess,
      checkWaitingRoomStatusFailure,
    );
  } catch (ex) {
    yield put(checkWaitingRoomStatusFailure(ex));
  }
}

const doGetOpenTokRoomKeys = ({ payload, token }) =>
  connectApi.post(`/api/pcp/getOpentokRoomKeys`, payload, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* getOpenTokRoomKeysSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doGetOpenTokRoomKeys, { payload, token });
    yield apiResponseEvaluator(
      response,
      getOpenTokRoomKeysSuccess,
      getOpenTokRoomKeysFailure,
    );
  } catch (ex) {
    yield put(getOpenTokRoomKeysFailure(ex));
  }
}

const doGetDoctorAlias = ({ payload, token }) =>
  connectApi.post(`/api/pcp/getDocFromAlias`, payload, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* getDocAliasSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doGetDoctorAlias, { payload, token });
    yield apiResponseEvaluator(
      response,
      getDocAliasSuccess,
      getDocAliasFailure,
    );
  } catch (ex) {
    yield put(getDocAliasFailure(ex));
  }
}

const doCallEndByPatient = ({ payload, token }) =>
  connectApi.post(`/api/pcp/callendbypatient`, payload, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* callEndByPatientSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doCallEndByPatient, { payload, token });
    yield apiResponseEvaluator(
      response,
      callEndByPatientSuccess,
      callEndByPatientFailure,
    );
  } catch (ex) {
    yield put(callEndByPatientFailure(ex));
  }
}

const doProviderSettings = ({ payload, token }) =>
  connectApi.get(`/connect/providersettings/${payload}`, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* providerSettingsSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doProviderSettings, { payload, token });
    yield apiResponseEvaluator(
      response,
      providerSettingsSuccess,
      providerSettingsFailure,
    );
  } catch (ex) {
    yield put(providerSettingsFailure(ex));
  }
}

const doDisconnectReason = ({ payload, token }) =>
  connectApi.post(`/api/pcp/updatetcalldisconnectreason`, payload, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* disconnectReasonSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doDisconnectReason, { payload, token });
    yield apiResponseEvaluator(
      response,
      disconnectReasonSuccess,
      disconnectReasonFailure,
    );
  } catch (ex) {
    yield put(disconnectReasonFailure(ex));
  }
}

const doGetMedications = ({ payload, token }) =>
  api.post(`/doctor/medicationquicksearch`, payload, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* getMedicationsSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doGetMedications, { payload, token });
    if (response.data.code === 200) {
      yield apiResponseEvaluator(
        response,
        getMedicationsSuccess,
        getMedicationsFailure,
      );
    } else {
      yield put(getMedicationsFailure(response.data.result));
    }
  } catch (ex) {
    yield put(logAppAppError('Unable to get Medications.'));
  }
}

const doGetAllergies = ({ payload, token }) =>
  api.post(`/patient/allergysearch`, payload, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* getAllergiesSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doGetAllergies, { payload, token });
    if (response.data.code === 200) {
      yield apiResponseEvaluator(
        response,
        getAllergiesSuccess,
        getAllergiesFailure,
      );
    } else {
      yield put(getAllergiesFailure(response.data.result));
    }
  } catch (ex) {
    yield put(logAppAppError('Unable to get Allergies.'));
  }
}

const doSaveFPI = ({ payload, token }) =>
  api.post(`/approveddoctor/sendonlyinstruction`, payload, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-AUTH-TOKEN': token,
    },
  });

export function* saveFPISaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doSaveFPI, { payload, token });
    if (response.data.code === 200) {
      yield apiResponseEvaluator(response, saveFPISuccess, saveFPIFailure);
    } else {
      yield put(saveFPIFailure(response.data.result));
    }
  } catch (ex) {
    yield put(saveFPIFailure(ex));
  }
}

const doSavePatientToHG = ({ payload }) =>
  newApi.post(`/healthgorilla/patients`, payload, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Basic ${btoa(
        `${restapiBasicAuth.username}:${restapiBasicAuth.password}`,
      )}`,
    },
  });

export function* savePatientToHGSaga({ payload }) {
  try {
    const response = yield call(doSavePatientToHG, { payload });
    if (response.data.code === 200) {
      yield apiResponseEvaluator(
        response,
        savePatientToHGSuccess,
        savePatientToHGFailure,
      );
    } else {
      yield put(savePatientToHGSuccess(response.data.result));
    }
  } catch (ex) {
    yield put(savePatientToHGFailure(ex));
  }
}

const doSavePatientInsuranceToHG = ({ payload }) =>
  newApi.put(`/healthgorilla/patients/coverage/${payload}`, undefined, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      Authorization: `Basic ${btoa(
        `${restapiBasicAuth.username}:${restapiBasicAuth.password}`,
      )}`,
    },
  });

export function* savePatientInsuranceToHGSaga({ payload }) {
  try {
    const response = yield call(doSavePatientInsuranceToHG, { payload });
    if (response.data.code === 200) {
      yield apiResponseEvaluator(
        response,
        savePatientInsuranceToHGSuccess,
        savePatientInsuranceToHGFailure,
      );
    } else {
      yield put(savePatientToHGSuccess(response.data.result));
    }
  } catch (ex) {
    yield put(savePatientToHGFailure(ex));
  }
}

const doInvalidateToken = ({ token }) =>
  newApi.post(
    `/v1/tokens/invalidate`,
    { access_token: token },
    {
      headers: {
        'Content-Type': 'application/json',
        'X-AUTH-TOKEN': token,
      },
    },
  );

export function* invalidateTokenSaga() {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doInvalidateToken, { token });
    yield apiResponseEvaluator(
      response,
      invalidateTokenSuccess,
      invalidateTokenFailure,
    );
  } catch (ex) {
    yield put(invalidateTokenFailure(ex));
  }
}

const doNotifiyProviders = ({ payload, token }) =>
  newApi.post(`/connect/notify`, payload, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* notifyProvidersSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doNotifiyProviders, { payload, token });
    yield apiResponseEvaluator(
      response,
      notifyProvidersSuccess,
      notifyProvidersFailure,
    );
  } catch (ex) {
    yield put(notifyProvidersFailure(ex));
  }
}

const doSavePaymentToPs = ({ payload, token }) =>
  newApi.post('/practicesuite/payment', payload, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
      Authorization: `Basic ${btoa(
        `${restapiBasicAuth.username}:${restapiBasicAuth.password}`,
      )}`,
    },
  });

export function* savePaymentToPsSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doSavePaymentToPs, { payload, token });
    yield apiResponseEvaluator(
      response,
      savePaymentToPsSuccess,
      savePaymentToPsFailure,
    );
  } catch (ex) {
    savePaymentToPsFailure(ex);
  }
}

const doSaveInsuranceToPs = ({ payload, token }) =>
  newApi.post(`/practicesuite/patients`, payload, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
      Authorization: `Basic ${btoa(
        `${restapiBasicAuth.username}:${restapiBasicAuth.password}`,
      )}`,
    },
  });

export function* saveInsuranceToPsSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doSaveInsuranceToPs, { payload, token });
    yield apiResponseEvaluator(
      response,
      saveInsuraceToPsSuccess,
      saveInsuraceToPsFailure,
    );
  } catch (ex) {
    yield put(notifyProvidersFailure(ex));
  }
}

export default function* legacySaga() {
  yield takeLatest(GET_INSURANCE_ELIGIBILITY, getInsuranceEligibilitySaga);
  yield takeLatest(GET_PATIENT_INSURANCE, getPatientInsuranceSaga);
  yield takeLatest(FIND_INSURANCE, findInsuranceSaga);
  yield takeLatest(SAVE_PATIENT_INSURANCE, savePatientInsuranceSaga);
  yield takeLatest(COUPON_CODE, checkCouponCodeSaga);
  yield takeLatest(BRAINTREE_TOKEN, getBrainTreeTokenSaga);
  yield takeLatest(PAYMENT_CHECKOUT, braintreeCheckoutSaga);
  yield takeLatest(GENERATE_PATIENT_CALL_ID, generatePatientCallIdSaga);
  yield takeLatest(ADD_USER_TO_WAITING_ROOM, addUserToWaitingRoomSaga);
  yield takeLatest(NOTIFY_PROVIDERS, notifyProvidersSaga);
  yield takeLatest(CHECK_WAITING_ROOM_STATUS, checkWaitingRoomStatusSaga);
  yield takeLatest(GET_OPENTOK_ROOM_KEYS, getOpenTokRoomKeysSaga);
  yield takeLatest(GET_DOC_ALIAS, getDocAliasSaga);
  yield takeLatest(UPDATE_PATIENT_CALL_ID, updatePatientCallIdSaga);
  yield takeLatest(CALL_END_BY_PATIENT, callEndByPatientSaga);
  yield takeLatest(INVALIDATE_TOKEN, invalidateTokenSaga);
  yield takeLatest(PROVIDER_SETTINGS, providerSettingsSaga);
  yield takeLatest(DISCONNECT_REASON, disconnectReasonSaga);
  yield takeLatest(GET_MEDICATIONS, getMedicationsSaga);
  yield takeLatest(GET_ALERGIES, getAllergiesSaga);
  yield takeLatest(SAVE_PAYMENT_TO_PS, savePaymentToPsSaga);
  yield takeLatest(SAVE_PATIENT_TO_PS, saveInsuranceToPsSaga);
  yield takeLatest(SAVE_FPI, saveFPISaga);
  yield takeLatest(SAVE_PATIENT_TO_HG, savePatientToHGSaga);
  yield takeLatest(SAVE_PATIENT_INSURANCE_TO_HG, savePatientInsuranceToHGSaga);
}
