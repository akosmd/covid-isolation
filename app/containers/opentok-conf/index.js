import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { useParams } from 'react-router-dom';

import { connect } from 'react-redux';
import { compose } from 'redux';
import { push } from 'connected-react-router';
import io from 'socket.io-client';
import moment from 'moment';

import { makeStyles } from '@material-ui/core/styles';

import { OTSession, OTStreams, preloadScript } from 'opentok-react';

import {
  Grid,
  Typography,
  IconButton,
  Tooltip,
  Button,
  CircularProgress,
} from '@material-ui/core';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';

import CallEndIcon from '@material-ui/icons/CallEnd';
import VideoCamOn from '@material-ui/icons/Videocam';
import VideoCamOff from '@material-ui/icons/VideocamOff';
import MicOn from '@material-ui/icons/Mic';
import MicOff from '@material-ui/icons/MicOff';

import { makeSelectActualPatient } from 'containers/App/selectors';
import {
  makeSelectAddUserToWaitingRoom,
  makeSelectNotifyProviders,
  makeSelectWaitingRoomStatus,
  makeSelectOpenTokRoomKeys,
  makeSelectDocAlias,
  makeSelectPatientCallId,
} from 'containers/App/legacySelectors';

import {
  addUserToWaitingRoom,
  notifyProviders,
  checkWaitingRoomStatus,
  getOpenTokRoomKeys,
  generatePatientCallId,
  getDocAlias,
  updatePatientCallId,
  callEndByPatient,
  providerSettings,
  disconnectReason,
  resetWaitingRoom,
} from 'containers/App/legacyActions';

import { OPEN_TOK_API, SOCKET_URL } from 'utils/config';

import Publisher from './publisher';
import Subscriber from './subscriber';

import './style.css';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  text: {
    [theme.breakpoints.down('md')]: {
      display: 'none',
    },
  },
  controls: {
    marginTop: '-2.5rem',
    paddingBottom: '2rem',

    zIndex: theme.zIndex.drawer + 1,
    position: 'fixed',
    padding: '1rem',
    display: 'flex',
    left: 0,
    bottom: '0',
    width: '100%',
    color: 'white',

    justifyContent: 'center',
  },
  videoRoot: {
    width: '100%',
    height: '50%',
    [theme.breakpoints.down('md')]: {
      height: '60%',
    },

    '& > div': {
      height: '100%',
    },
    '& > div > div': {
      height: '100%',
    },
    '& > div > div > div': {
      height: '100%',
    },
    padding: '1px',
  },
  videoRootFull: {
    width: '100%',
    height: '100%',
    '& > div': {
      height: '100%',
    },
    '& > div > div': {
      height: '100%',
    },
    '& > div > div > div': {
      height: '100%',
    },
  },
  parentContainer: {
    height: '79vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  parentContainerFull: {
    height: '100vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  quitting: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh',
  },
}));

function OpenTokConf({
  roomName,
  openTokKeys,
  patient,
  docAlias,
  callId,
  waitingRoomStatus,
  addUserToRoom,
  dispatch,
  doAddUserToRoom,
  doNotifyProviders,
  doCheckWaitingRoomStatus,
  doGetOpenTokRoomKeys,
  doGetDocAlias,
  // doUpdatePatientCallId,
  doCallEndByPatient,
  doProviderSettings,
  doDisconnectReason,
  doResetWaitingRoom,
  doGenerateCallId,
}) {
  const [error, setError] = useState();
  // eslint-disable-next-line no-unused-vars
  const [connected, setConnected] = useState(false);
  const [addingToRoom, setAddingToRoom] = useState(false);
  const [subscribed, setSubscribe] = useState(false);
  const sessionRef = React.useRef(null);

  const [audio, setAudio] = useState(true);
  const [video, setVideo] = useState(true);
  const [showAlert, setShowAlert] = useState(false);
  const [showQuitting, setShowQuitting] = useState(false);
  const [withError, setWithError] = useState(false);
  const [roomParam, setRoomParam] = useState();
  const [showDoctorLeave, setShowDoctorLeave] = useState({ open: false });
  const [socket, setSocket] = useState(null);

  const classes = useStyles();

  const { id: paramRoom } = useParams();
  useEffect(() => {
    if (patient.data && !addingToRoom) {
      dispatch(
        doCheckWaitingRoomStatus({
          patient_id: patient.data.patient.patientLegacyId,
        }),
      );

      dispatch(doGetDocAlias({ alias: paramRoom || roomName }));
    }
    if (paramRoom) setRoomParam(paramRoom);
  }, []);

  useEffect(() => {
    if (
      addUserToRoom.data &&
      callId.data &&
      docAlias.data &&
      docAlias.data.result.length > 0
    ) {
      dispatch(
        doGetOpenTokRoomKeys({ id: patient.data.patient.patientLegacyId }),
      );
      const firstDoc = docAlias.data.result[0];

      const soc = io(SOCKET_URL);

      if (!socket) {
        setSocket(soc);
        soc.on('callDisconnectedByDoc', handleDisconnectedByDoctor);
      }

      soc.emit('userjoin', {
        waitingId: callId.data.result.call_id,
        docid: 500,
        groupId: firstDoc.group_id,
      });
    }
  }, [addUserToRoom, callId]);

  useEffect(() => {
    if (
      !callId.data &&
      !callId.loading &&
      !callId.error &&
      patient.data &&
      !showQuitting
    ) {
      const params = {
        patientId: patient.data.patient.patientLegacyId,
        patientEmail: patient.data.patient.email,
        patientDob: patient.data.patient.birthDateAt,
        uuid: patient.data.patient.uuid,
        staffid: 500,
        call_started: moment(Date.now()).format('YYYY/MM/DD'),
        room_name: roomParam || roomName,
        call_type: 'connect',
        device_type: window.navigator.userAgent,
      };
      dispatch(doGenerateCallId(params));
    }
  }, [callId]);

  useEffect(() => {
    setWithError(false);
    if (!openTokKeys.data && openTokKeys.error) setWithError(true);
  }, [openTokKeys]);

  useEffect(() => {
    if (
      !waitingRoomStatus.loading &&
      !addUserToRoom.data &&
      callId.data &&
      docAlias.data &&
      docAlias.data.result.length > 0 &&
      !addingToRoom
    ) {
      const firstDoc = docAlias.data.result[0];
      const params = {
        docId: firstDoc.id,
        groupId: firstDoc.group_id,
        userId: patient.data.patient.patientLegacyId,
        symptom: '[]',
        allergy: '["No Allergies"]',
        medication: '["No Medications"]',
        medical_condition: '["No Pre-existing medical conditions"]',
        call_id: `${callId.data.result.call_id}`,
        date_of_injury: '',
        state_of_injury: '',
        employer_name: '',
        alias: roomParam || roomName,
      };
      dispatch(doAddUserToRoom(params));

      setAddingToRoom(true);
    }
    // if (callId.data && docAlias.data && docAlias.data.result.length > 0) {
    //   const firstDoc = docAlias.data.result[0];
    //   const params = {
    //     docId: firstDoc.id,
    //     groupId: firstDoc.group_id,
    //     userId: patient.data.patient.patientLegacyId,
    //     symptom: '[]',
    //     allergy: '["No Allergies"]',
    //     medication: '["No Medications"]',
    //     medical_condition: '["No Pre-existing medical conditions"]',
    //     call_id: `${callId.data.result.call_id}`,
    //     date_of_injury: '',
    //     state_of_injury: '',
    //     employer_name: '',
    //     alias: roomName,
    //   };
    //   dispatch(doUpdatePatientCallId(params));
    // }
  }, [callId, docAlias, addUserToRoom, waitingRoomStatus]);

  useEffect(() => {
    if (
      (!waitingRoomStatus.loading && waitingRoomStatus.data) ||
      addUserToRoom.data
    ) {
      dispatch(doNotifyProviders({ roomName: paramRoom || roomName }));
    }
  }, [waitingRoomStatus, addUserToRoom]);

  const handleSetAudio = () => {
    setAudio(!audio);
  };

  const handleSetVideo = () => {
    setVideo(!video);
  };

  const sessionEvents = {
    sessionConnected: () => {
      setConnected(true);
    },
    sessionDisconnected: () => {
      setConnected(false);
    },
  };

  const onError = err => {
    setError(`Failed to connect: ${err.message}`);
  };

  const handleDisconnectedByDoctor = data => {
    if (data.callId === callId.data.result.call_id)
      setShowDoctorLeave({ open: true, status: data.status });
  };

  const handleEndCall = () => {
    setShowAlert(true);
  };

  const handleQuit = isNoEmit => {
    if (!isNoEmit) {
      socket.emit('callDisconnectedByDoc', {
        callId: callId.data.result.call_id,
      });
    }
    setShowQuitting(true);
    if (callId.data && callId.data.result.call_id)
      dispatch(doCallEndByPatient({ callid: `${callId.data.result.call_id}` }));
    dispatch(doProviderSettings(roomParam || roomName));
    dispatch(
      doDisconnectReason({
        sessionId: openTokKeys.data.result[0].session,
        reason: 'Call ended by user',
      }),
    );
    handleSetVideo();
    dispatch(doResetWaitingRoom());
    setTimeout(() => {
      // window.location.href = '/';
      dispatch(push('/'));
    }, 5000);
  };

  const handleCloseDoctorLeaveModal = () => {
    if (showDoctorLeave.status !== 'PENDING') {
      handleQuit(true);
    }

    setShowDoctorLeave({ ...showDoctorLeave, open: false });
  };

  const renderDoctorDisconnectModal = () => (
    <Dialog
      open={showDoctorLeave.open}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">
        Call Disconnected By Doctor
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {showDoctorLeave.status === 'PENDING'
            ? 'Please wait, the doctor will be back shortly.'
            : 'The doctor has ended the call. Thank you!'}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCloseDoctorLeaveModal} color="primary">
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );

  if (showQuitting)
    return (
      <Typography variant="h4" align="center" className={classes.quitting}>
        Thank You! Please wait while we are redirecting you to the dashboard...
      </Typography>
    );
  if (withError)
    return (
      <div>
        <Typography component="div" variant="h4" align="center" color="error">
          Unable to connect you to a Provider at this time. Please try again
          later.
        </Typography>
        <Typography align="center" component="div">
          <Button href="/">Back to Dashboard</Button>
        </Typography>
      </div>
    );

  if (!openTokKeys.data) return null;

  return (
    <OTSession
      apiKey={OPEN_TOK_API}
      sessionId={openTokKeys.data.result[0].session}
      token={openTokKeys.data.result[0].token}
      eventHandlers={sessionEvents}
      ref={sessionRef}
      onError={onError}
    >
      <Grid
        container
        alignItems="center"
        spacing={1}
        justify="center"
        className={
          !subscribed ? classes.parentContainerFull : classes.parentContainer
        }
      >
        <Grid item xs={12} lg={8} className={classes.text}>
          <Typography variant="h6" align="center">
            Hello{' '}
            {`${patient.data.patient.firstName} ${
              patient.data.patient.lastName
            } `}
            ! Thanks for joining our consultation room. Our providers are aware
            of your arrival in this waiting room and they will be with you
            shortly.
          </Typography>
          <Typography align="center">
            <em>
              Please make sure you allow the portal to access your camera and
              microphone when prompted.
            </em>
          </Typography>
        </Grid>
        <Grid
          item
          xs={12}
          lg={6}
          className={!subscribed ? classes.videoRootFull : classes.videoRoot}
          style={{ padding: '1px' }}
        >
          {error ? <div id="error">{error}</div> : null}

          <Publisher
            session={
              sessionRef.current !== null ? sessionRef.current : undefined
            }
            handleSetAudio={handleSetAudio}
            handleSetVideo={handleSetVideo}
            audio={audio}
            video={video}
            showAlert={showAlert}
            setShowAlert={setShowAlert}
            onQuit={handleQuit}
          />
        </Grid>

        {!subscribed && (
          <Grid item xs={12} lg={6}>
            <Typography variant="h6" align="center" className={classes.text}>
              Please wait, we will be in touch shortly...
            </Typography>
            <Typography component="div" align="center">
              <CircularProgress color="secondary" />
            </Typography>
            <OTStreams>
              <Subscriber onSubscribed={setSubscribe} />
            </OTStreams>
          </Grid>
        )}
        {subscribed && (
          <Grid
            item
            xs={12}
            lg={6}
            className={classes.videoRoot}
            style={{ padding: '1px' }}
          >
            <OTStreams>
              <Subscriber onSubscribed={setSubscribe} />
            </OTStreams>
          </Grid>
        )}

        <Grid item xs={12} className={classes.controls}>
          <Tooltip title="End Call">
            <IconButton size="medium" onClick={handleEndCall}>
              <CallEndIcon color="error" fontSize="large" />
            </IconButton>
          </Tooltip>

          <Tooltip title="Toggle Video">
            <IconButton size="medium" onClick={handleSetVideo}>
              {!video ? (
                <VideoCamOff color="secondary" fontSize="large" />
              ) : (
                <VideoCamOn color="secondary" fontSize="large" />
              )}
            </IconButton>
          </Tooltip>
          <Tooltip title="Mute/Unmute Audio">
            <IconButton size="medium" onClick={handleSetAudio}>
              {!audio ? (
                <MicOff color="secondary" fontSize="large" />
              ) : (
                <MicOn color="secondary" fontSize="large" />
              )}
            </IconButton>
          </Tooltip>
        </Grid>
      </Grid>
      {renderDoctorDisconnectModal()}
    </OTSession>
  );
}

const { object, func, string } = PropTypes;
OpenTokConf.propTypes = {
  dispatch: func.isRequired,
  roomName: string,
  openTokKeys: object.isRequired,
  addUserToRoom: object.isRequired,
  patient: object.isRequired,
  callId: object.isRequired,
  waitingRoomStatus: object.isRequired,
  docAlias: object.isRequired,
  doAddUserToRoom: func.isRequired,
  doNotifyProviders: func.isRequired,
  doGetOpenTokRoomKeys: func.isRequired,
  doGetDocAlias: func.isRequired,
  // doUpdatePatientCallId: func.isRequired,
  doCallEndByPatient: func.isRequired,
  doProviderSettings: func.isRequired,
  doDisconnectReason: func.isRequired,
  doResetWaitingRoom: func.isRequired,
  doGenerateCallId: func.isRequired,
  doCheckWaitingRoomStatus: func.isRequired,
};
OpenTokConf.defaultProps = {
  roomName: 'medical',
};
const mapStateToProps = createStructuredSelector({
  patient: makeSelectActualPatient(),
  waitingRoomStatus: makeSelectWaitingRoomStatus(),
  openTokKeys: makeSelectOpenTokRoomKeys(),
  notifyProviders: makeSelectNotifyProviders(),
  addUserToRoom: makeSelectAddUserToWaitingRoom(),
  docAlias: makeSelectDocAlias(),
  callId: makeSelectPatientCallId(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doAddUserToRoom: addUserToWaitingRoom,
    doNotifyProviders: notifyProviders,
    doGetOpenTokRoomKeys: getOpenTokRoomKeys,
    doGetDocAlias: getDocAlias,
    doUpdatePatientCallId: updatePatientCallId,
    doCallEndByPatient: callEndByPatient,
    doProviderSettings: providerSettings,
    doDisconnectReason: disconnectReason,
    doResetWaitingRoom: resetWaitingRoom,
    doGenerateCallId: generatePatientCallId,
    doCheckWaitingRoomStatus: checkWaitingRoomStatus,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(preloadScript(OpenTokConf));
