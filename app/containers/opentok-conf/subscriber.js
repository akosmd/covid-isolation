import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { OTSubscriber } from 'opentok-react';

function Subscriber({ onSubscribed }) {
  // eslint-disable-next-line no-unused-vars
  const [error, setError] = useState(null);

  const onError = err => {
    setError(`Failed to subscribe: ${err.message}`);
  };

  const eventHandlers = {
    destroyed: () => {
      onSubscribed(false);
    },
  };
  return (
    <OTSubscriber
      properties={{
        subscribeToAudio: true,
        subscribeToVideo: true,
      }}
      eventHandlers={eventHandlers}
      onSubscribe={() => onSubscribed(true)}
      onError={onError}
    />
  );
}

Subscriber.propTypes = {
  onSubscribed: PropTypes.func.isRequired,
};
export default Subscriber;
