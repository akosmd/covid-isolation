import React, { lazy, Suspense } from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';
import PropTypes from 'prop-types';
import FlexView from 'components/FlexView';
import Typography from '@material-ui/core/Typography';

const ChatComponent = lazy(() => import('./chat'));

const Loader = ({ containerHeight }) => (
  <FlexView direction="column" justify="center" height={containerHeight}>
    <LinearProgress />
    <Typography align="center" variant="body1">
      {'Initializing...'}
    </Typography>
  </FlexView>
);
const Chat = ({ containerHeight, ...props }) => (
  <Suspense fallback={<Loader containerHeight={containerHeight} />}>
    <ChatComponent containerHeight={containerHeight} {...props} />
  </Suspense>
);

const { oneOfType, string, number } = PropTypes;

Loader.propTypes = {
  containerHeight: oneOfType([string, number]),
};

Chat.propTypes = {
  containerHeight: oneOfType([string, number]),
};

export default Chat;
