import React from 'react';
import {
  isIOS,
  isIE,
  isEdge,
  isSafari,
  isMobileSafari,
} from 'react-device-detect';
import Flexview from 'components/FlexView';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import WarningIcon from '@material-ui/icons/Warning';

export default ({ children }) => {
  if (
    (!isIOS && (isEdge || isIE)) ||
    (isIOS && (!isSafari || !isMobileSafari))
  ) {
    return (
      <Flexview height="100vh" width="100vw" justify="center" align="center">
        <Dialog
          open
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">Unsupported Browser</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              <WarningIcon color="error" /> <strong>Warning! </strong>
              Your browser is not supported to view this website. For IOS
              devices, please use Safari browser. For Android devices, please
              use Google Chrome or Firefox browser.
            </DialogContentText>
          </DialogContent>
        </Dialog>
      </Flexview>
    );
  }
  return children;
};
