import React from 'react';
import BaseAutocomplete from '@material-ui/lab/Autocomplete';
import MuiTextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

import _ from 'lodash';

const styles = makeStyles({
  root: {
    marginTop: 16,
    marginBottom: 8,
  },
});

const Autocomplete = ({
  data,
  onChange,
  onSelect,
  label,
  variant,
  error,
  fullWidth,
  formatedData,
  ...rest
}) => {
  const classes = styles();
  return (
    <BaseAutocomplete
      className={classes.root}
      {...rest}
      autoComplete={data !== undefined}
      options={
        formatedData
          ? data
          : data && _.map(data, item => ({ Name: item, value: item }))
      }
      getOptionLabel={option => data && option.Name}
      onChange={(o, val) => onSelect(val)}
      renderInput={params => (
        <MuiTextField
          {...params}
          label={label}
          variant={variant || 'outlined'}
          error={error}
          onChange={e => onChange(e.target.value)}
          fullWidth={fullWidth}
        />
      )}
    />
  );
};
const { oneOfType, object, bool, array, func, string } = PropTypes;
Autocomplete.propTypes = {
  data: oneOfType([array, bool]),
  onChange: func.isRequired,
  onSelect: func.isRequired,
  label: string,
  variant: string,
  error: oneOfType([object, bool]),
  fullWidth: bool,
  formatedData: bool,
};
export default Autocomplete;
