/**
 *
 * DatePickerField
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import TextInput from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import KeyboardEventHandler from 'react-keyboard-event-handler';
import moment from 'moment';
function DatePickerField({
  field,
  margin,
  className,
  onChange,
  variant,
  keyEvents,
  loading,
  maxDate,
  minDate,
  disableFuture,
  disablePast,
}) {
  if (!field) return null;
  const { error, caption, errorMessage, id, pristine, readOnly } = field;
  let message = caption;
  if (error && !pristine && field.required)
    message = !errorMessage ? `${caption} is required` : errorMessage;
  const dummy = Date.now();
  const min =
    minDate || disablePast
      ? moment(minDate || undefined).format('YYYY-MM-DD')
      : undefined;
  const max =
    maxDate || disableFuture
      ? moment(maxDate || undefined).format('YYYY-MM-DD')
      : undefined;
  const handleChange = e => onChange(e.target.value);

  return !keyEvents ? (
    <FormControl
      margin={margin}
      required={field.required}
      fullWidth={field.fullWidth}
      className={className}
    >
      <TextInput
        id={id}
        name={`${dummy}-${id}`}
        error={error && !pristine}
        helperText={loading ? 'Please wait while loading...' : undefined}
        label={field.error && !pristine ? message : caption}
        onChange={handleChange}
        value={
          field.value ? moment(field.value).format('YYYY-MM-DD') : undefined
        }
        type="date"
        fullWidth={field.fullWidth}
        autoComplete="off"
        inputProps={{ readOnly, autoComplete: 'off', min, max }}
        variant={variant}
        InputLabelProps={{
          shrink: true,
        }}
        required={field.required}
      />
    </FormControl>
  ) : (
    <KeyboardEventHandler
      handleKeys={keyEvents.handleKeys}
      onKeyEvent={keyEvents.onKeyEvent}
      style={{ width: '100%' }}
    >
      <FormControl
        margin={margin}
        required={field.required}
        fullWidth={field.fullWidth}
        className={className}
      >
        <TextInput
          id={id}
          name={`${dummy}-${id}`}
          error={error && !pristine}
          helperText={loading ? 'Please wait while loading...' : undefined}
          label={field.error && !pristine ? message : caption}
          onChange={handleChange}
          value={
            field.value ? moment(field.value).format('YYYY-MM-DD') : undefined
          }
          type="date"
          fullWidth={field.fullWidth}
          autoComplete="off"
          inputProps={{ readOnly, autoComplete: 'off', min, max }}
          variant={variant}
          InputLabelProps={{
            shrink: true,
          }}
          required={field.required}
        />
      </FormControl>
    </KeyboardEventHandler>
  );
}
const { string, bool, shape, func, object, instanceOf } = PropTypes;
DatePickerField.propTypes = {
  field: shape({
    id: string.isRequired,
    value: string,
    caption: string.isRequired,
    errorMessage: string,
    placeholder: string,
    error: bool,
    required: bool,
    fullWidth: bool,
    autoFocus: bool,
  }),
  onChange: func,
  margin: string,
  className: string,
  variant: string,
  keyEvents: object,
  loading: bool,
  minDate: PropTypes.oneOfType([instanceOf(Date), instanceOf(moment)]),
  maxDate: PropTypes.oneOfType([instanceOf(Date), instanceOf(moment)]),
  disableFuture: bool,
  disablePast: bool,
};
DatePickerField.defaultProps = {
  margin: 'normal',
  variant: 'outlined',
  onChange: undefined,
  className: undefined,
  disableFuture: false,
  disablePast: false,
};
export default DatePickerField;
