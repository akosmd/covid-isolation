/**
 *
 * Asynchronously loads the component for Selection
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
