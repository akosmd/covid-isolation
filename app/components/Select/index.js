/**
 *
 * Select
 *
 */

import React from 'react';

import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import KeyboardEventHandler from 'react-keyboard-event-handler';

import PropTypes from 'prop-types';

function Selection({
  onChange,
  field,
  className,
  options,
  disabled,
  variant,
  keyEvents,
}) {
  const inputLabel = React.useRef(null);
  const [labelWidth, setLabelWidth] = React.useState(0);

  React.useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth);
  }, []);

  const getOptions = opts =>
    opts.map(({ value, name }) => (
      <option value={value} key={value}>
        {name}
      </option>
    ));
  if (!field) return null;

  const { error, pristine, readOnly } = field;

  return !keyEvents ? (
    <FormControl
      margin="normal"
      required={field.required}
      fullWidth
      variant={variant}
      className={className}
      disabled={disabled || readOnly}
      error={error && !pristine}
    >
      <InputLabel
        htmlFor={field.id}
        ref={inputLabel}
        error={error && !pristine}
      >
        {field.error && !pristine
          ? field.errorMessage || `${field.caption} is required`
          : field.caption}
      </InputLabel>
      <Select
        native
        value={field.value !== null ? field.value : ''}
        id={field.id}
        name={field.name}
        labelWidth={labelWidth}
        onChange={onChange}
      >
        {getOptions(options)}
      </Select>
    </FormControl>
  ) : (
    <KeyboardEventHandler
      handleKeys={keyEvents.handleKeys}
      onKeyEvent={keyEvents.onKeyEvent}
    >
      <FormControl
        margin="normal"
        required={field.required}
        fullWidth
        variant={variant}
        className={className}
        disabled={disabled || readOnly}
        error={error && !pristine}
      >
        <InputLabel
          htmlFor={field.id}
          ref={inputLabel}
          error={error && !pristine}
        >
          {field.error && !pristine
            ? field.errorMessage || `${field.caption} is required`
            : field.caption}
        </InputLabel>
        <Select
          native
          value={field.value !== null ? field.value : ''}
          id={field.id}
          name={field.name}
          labelWidth={labelWidth}
          onChange={onChange}
        >
          {getOptions(options)}
        </Select>
      </FormControl>
    </KeyboardEventHandler>
  );
}

const {
  string,
  object,
  bool,
  shape,
  func,
  arrayOf,
  any,
  oneOfType,
  number,
} = PropTypes;
Selection.propTypes = {
  field: shape({
    id: string.isRequired,
    value: oneOfType([string, number]).isRequired,
    caption: string.isRequired,
    errorMessage: string,
    placeholder: string,
    error: bool,
    required: bool,
    fullWidth: bool,
    autoFocus: bool,
  }),
  onChange: func,
  keyEvents: object,
  className: string,

  options: arrayOf(
    shape({
      value: any.isRequired,
      name: string.isRequired,
    }),
  ),
  disabled: bool,
  variant: string,
};

Selection.defaultProps = {
  onChange: undefined,
  className: undefined,
  disabled: false,
  variant: undefined,
};

export default Selection;
