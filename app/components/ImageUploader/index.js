import React from 'react';
import PropTypes from 'prop-types';
import { useDropzone } from 'react-dropzone';
import FlexView from 'components/FlexView';
import { Typography, Paper, CircularProgress } from '@material-ui/core';
import Icon from '@material-ui/icons/CloudUpload';
import CameraIcon from '@material-ui/icons/CameraAlt';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  camera: {
    display: 'flex',
    [theme.breakpoints.down('md')]: {
      display: `none`,
    },
  },
}));

const Dropzone = ({
  loaderHeight,
  loading,
  initCamera,
  handleDrop,
  disableCamera,
}) => {
  const onDrop = files => {
    const file = files[0];
    const reader = new FileReader();
    reader.onload = event => {
      handleDrop(event.target.result);
    };
    reader.readAsDataURL(file);
  };
  const classes = useStyles();

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
    accept: 'image/jpeg, image/png',
  });

  return (
    <Paper elevation={0}>
      {loading ? (
        <FlexView
          height={loaderHeight}
          padding={0}
          justify="center"
          direction="column"
        >
          <Typography component="div" align="center">
            <CircularProgress />
          </Typography>
        </FlexView>
      ) : (
        <>
          <FlexView {...getRootProps()}>
            <input className="dropzone-input" {...getInputProps()} />
            <FlexView justify="center">
              {isDragActive ? (
                <FlexView direction="row" justify="center">
                  <Icon />
                  <Typography secondary>
                    Release to drop the files here
                  </Typography>
                </FlexView>
              ) : (
                <FlexView direction="row" justify="center">
                  <Icon />
                  <Typography>
                    {"Drag 'n' drop some files here, or click to a select file"}
                  </Typography>
                </FlexView>
              )}
            </FlexView>
          </FlexView>
          {!disableCamera && (
            <FlexView onClick={initCamera} className={classes.camera}>
              <FlexView direction="row" justify="center">
                <CameraIcon />
                <Typography>Click here, to take a photo instead.</Typography>
              </FlexView>
            </FlexView>
          )}
        </>
      )}
    </Paper>
  );
};

const { func, bool, number } = PropTypes;
Dropzone.propTypes = {
  initCamera: func,
  handleDrop: func.isRequired,
  loading: bool,
  loaderHeight: number,
  disableCamera: bool,
};
export default Dropzone;
