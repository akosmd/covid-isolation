import React from 'react';
import PropTypes from 'prop-types';
import FlexView from 'components/FlexView';

import { withStyles } from '@material-ui/core/styles';

import {
  Grid,
  Typography,
  ExpansionPanelSummary as BaseExpansionPanelSummary,
  ExpansionPanel as BaseExpansionPanel,
  ExpansionPanelDetails,
} from '@material-ui/core';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import theme from 'utils/theme';

const ExpansionPanel = withStyles({
  root: {
    border: '1px solid rgba(0, 0, 0, .125)',
    '&:not(:last-child)': {
      borderBottom: 0,
    },
    '&:first-child': {
      borderTopLeftRadius: '.5rem',
      borderTopRightRadius: '.5rem',
    },
    '&:last-child': {
      borderBottomLeftRadius: '.5rem',
      borderBottomRightRadius: '.5rem',
    },
    '&:before': {
      display: 'none',
    },
    '&$expanded': {
      margin: 'auto',
    },
  },
  expanded: {},
})(BaseExpansionPanel);

const ExpansionPanelSummary = withStyles({
  root: {
    marginBottom: -1,
    minHeight: 56,
    '&$expanded': {
      minHeight: 56,
    },
  },
  content: {
    '&$expanded': {
      // margin: '12px 0',
    },
  },
  expanded: {},
})(BaseExpansionPanelSummary);

const Panel = ({ data, id, handleSelect }) => (
  <Grid item xs={12}>
    {data &&
      data.map((item, index) => (
        <ExpansionPanel key={`${id}-${index + 1}`} elevation={0}>
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1bh-content"
            id={`${id}-header-${index + 1}`}
          >
            <Typography color="primary">{item.heading}</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <FlexView padding={0} direction="column">
              {item.values.map((leaf, leafIndex) => (
                <FlexView
                  key={`${id}-leaf-${leafIndex + 1}`}
                  boxshadow={theme.palette.boxShadow.main}
                  padding={15}
                  // margin="8px 0"
                >
                  <FormControlLabel
                    style={{ width: '100%' }}
                    control={
                      <Checkbox
                        onChange={() => handleSelect(leaf)}
                        name={leaf}
                      />
                    }
                    label={leaf}
                  />
                </FlexView>
              ))}
            </FlexView>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      ))}
  </Grid>
);

const { string, func, array } = PropTypes;

Panel.propTypes = {
  data: array.isRequired,
  id: string.isRequired,
  handleSelect: func.isRequired,
};

export default Panel;
