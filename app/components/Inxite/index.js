/**
 *
 * Inxite
 *
 */

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { makeSelectInxiteUrl } from 'containers/App/selectors';
import './inxite.css';
import { CircularProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(() => ({
  center: {
    display: 'flex',
  },
}));

function Inxite({ inxiteUrl }) {
  const classes = useStyles();
  const [isLoaded, setLoaded] = useState(false);

  if (!inxiteUrl) return null;

  const onLoaded = () => {
    if (!isLoaded) setLoaded(true);
  };

  return (
    <div>
      {!isLoaded && (
        <div className={classes.center}>
          <CircularProgress />
        </div>
      )}
      <iframe
        src={inxiteUrl}
        title="Akos Member Portal"
        style={{ width: '100%', height: '100vh', border: 0 }}
        onLoad={onLoaded}
      />
    </div>
  );
}

Inxite.propTypes = {
  inxiteUrl: PropTypes.string.isRequired,
};

const mapStateToProps = createStructuredSelector({
  inxiteUrl: makeSelectInxiteUrl(),
});

const withConnect = connect(
  mapStateToProps,
  null,
);

export default compose(withConnect)(Inxite);
