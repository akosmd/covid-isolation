// import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const GradientButton = withStyles(({ fullWidth, palette }) => ({
  root: {
    color: '#fff',
    background: palette.gradient.main, // 'linear-gradient(to right, #55bcbe 0%,#85c3a6 100%)',
    '&:hover': {
      background: palette.gradient.hover,
    },
    maxHeight: 56,
    width: fullWidth && '100%',
  },
}))(Button);

export default GradientButton;
