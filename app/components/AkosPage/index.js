/**
 *
 * AkosPage
 *
 */

import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';
import {
  makeSelectVerifyAuth,
  makeSelectNurseSignin,
} from 'containers/App/selectors';

import classNames from 'classnames';

import { useSnackbar } from 'notistack';

import { useTheme } from '@material-ui/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';

import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';

import Header from 'components/Header';
import Footer from 'components/Footer';

import MyLink from 'components/MyLink';
import ChatButton from 'containers/ChatRoom/chatButton';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: '0 8px',
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
    [theme.breakpoints.down('md')]: {
      minHeight: `60px`,
    },
    [theme.breakpoints.up('sm')]: {
      minHeight: `44px`,
    },
  },
  drawerHeaderSmall: {
    display: 'flex',
    alignItems: 'center',
    padding: '0 8px',
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
    [theme.breakpoints.down('md')]: {
      minHeight: `59px`,
    },
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    [theme.breakpoints.down('md')]: {
      padding: `${theme.spacing(3)}px 1rem`,
      paddingBottom: theme.spacing(8),
    },
    paddingBottom: theme.spacing(8),
    marginTop: 50,
  },
  contentWide: {
    flexGrow: 1,
    padding: '1px',
    marginTop: 50,
    paddingBottom: theme.spacing(8),
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
  fab: {
    position: 'fixed',
    bottom: theme.spacing(8),
    right: theme.spacing(2),
  },
}));

function useWidth() {
  const theme = useTheme();
  const keys = [...theme.breakpoints.keys].reverse();
  return (
    keys.reduce((output, key) => {
      // eslint-disable-next-line react-hooks/rules-of-hooks
      const matches = useMediaQuery(theme.breakpoints.up(key));
      return !output && matches ? key : output;
    }, null) || 'xs'
  );
}

export function AkosPage(props) {
  const [toggle, setToggle] = useState(false);
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const { account, fullScreen, nurseSignin, ...rest } = props;
  const width = useWidth();

  const isMobile = useMediaQuery(theme => theme.breakpoints.down('sm'));
  const onToggleMenu = () => {
    setToggle(!toggle);
  };

  const { component: Component, history } = props;

  const NO_CHAT_URLS = [
    '/info',
    '/connect',
    '/360',
    '/video-conference',
    '/video-conference/vpdc',
    '/video-conference/covid19',
    '/video-conference/medical',
    '/video-conference/agileurgentcare',
    '/covid-assessment',
    '/chatroom',
    '/medical-history',
    '/medclinic/sso',
    // '/covid-nj',
  ];

  const NO_FOOTER_URLS = [
    '/video-conference',
    '/video-conference/vpdc',
    '/video-conference/covid19',
    '/video-conference/medical',
    '/video-conference/agileurgentcare',
    '/connect',
    '/covid-assessment',
    '/medclinic/sso',
  ];

  let isAuthenticated =
    account.data !== false &&
    !NO_CHAT_URLS.includes(history.location.pathname.toLowerCase());

  if (!isAuthenticated) {
    isAuthenticated =
      nurseSignin.data !== false &&
      (history.location.pathname.toLowerCase() !== '/info' ||
        history.location.pathname.includes('/chatroom/nurse'));

    if (
      isAuthenticated &&
      history.location.pathname.includes('/chatroom/nurse')
    ) {
      isAuthenticated = false;
    }
  }

  const isCovid = history.location.pathname.toLowerCase().includes('covid');

  const renderFooter = () => {
    if (NO_FOOTER_URLS.includes(history.location.pathname.toLowerCase()))
      return null;

    return (
      <Footer
        width={width}
        path={history.location.pathname.toLowerCase()}
        memberType={
          account.data ? account.data.patient.membershipTypeId : undefined
        }
      />
    );
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Header
        {...rest}
        toggle={toggle}
        onToggle={onToggleMenu}
        isAuthenticated={isAuthenticated}
      />

      <main
        className={classNames(
          fullScreen ? classes.contentWide : classes.content,
        )}
      >
        <div
          className={
            fullScreen ? classes.drawerHeaderSmall : classes.drawerHeader
          }
        />
        {Component && (
          <Component
            {...rest}
            enqueueSnackbar={enqueueSnackbar}
            width={width}
          />
        )}
        {isAuthenticated && !isCovid && (
          <ChatButton
            isMobile={isMobile}
            component={MyLink}
            className={classes.fab}
          />
        )}
      </main>
      {renderFooter()}
    </div>
  );
}

const { func, node, oneOfType, object, bool } = PropTypes;
AkosPage.propTypes = {
  component: oneOfType([func, object, node]),
  account: object.isRequired,
  nurseSignin: object.isRequired,
  history: object.isRequired,
  fullScreen: bool,
};

const mapStateToProps = createStructuredSelector({
  account: makeSelectVerifyAuth(),
  nurseSignin: makeSelectNurseSignin(),
});

const withConnect = connect(mapStateToProps);

export default compose(withConnect)(AkosPage);
