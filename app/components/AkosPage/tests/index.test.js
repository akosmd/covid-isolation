/**
 *
 * Tests for AkosPage
 *
 * @see https://github.com/react-boilerplate/react-boilerplate/tree/master/docs/testing
 *
 */

import React from 'react';
import { render } from 'react-testing-library';
// import 'jest-dom/extend-expect'; // add some helpful assertions
import { SnackbarProvider } from 'notistack';
import { AkosPage } from '../index';

const dispatch = jest.fn();
describe('<AkosPage />', () => {
  it('Expect to not log errors in console', () => {
    const spy = jest.spyOn(global.console, 'error');

    render(
      <SnackbarProvider>
        <AkosPage
          dispatch={dispatch}
          userInfo={{}}
          childRoutes={[]}
          selectedApp={{}}
          showSidebar={false}
          handleLogout={dispatch}
          clearBrokerData={dispatch}
        />
      </SnackbarProvider>,
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it('Expect to have additional unit tests specified', () => {
    expect(true).toEqual(true);
  });

  /**
   * Unskip this test to use it
   *
   * @see {@link https://jestjs.io/docs/en/api#testskipname-fn}
   */
  it.skip('Should render and match the snapshot', () => {
    const {
      container: { firstChild },
    } = render(
      <SnackbarProvider>
        <AkosPage
          dispatch={dispatch}
          userInfo={{}}
          childRoutes={[]}
          selectedApp={{}}
          showSidebar
          handleLogout={dispatch}
          clearBrokerData={dispatch}
        />
      </SnackbarProvider>,
    );
    expect(firstChild).toMatchSnapshot();
  });
});
