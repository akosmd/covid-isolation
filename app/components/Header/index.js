/**
 *
 * Header
 *
 */

import React, { useState, useEffect, Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';

import { connect } from 'react-redux';
import { compose } from 'redux';
import { push } from 'connected-react-router';
import { createStructuredSelector } from 'reselect';

import classNames from 'classnames';

import Link from '@material-ui/core/Link';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';

import AccountIcon from '@material-ui/icons/AccountCircle';
import PeopleIcon from '@material-ui/icons/People';
import IconButton from '@material-ui/core/IconButton';
import DashboardIcon from '@material-ui/icons/Dashboard';

import PropTypes from 'prop-types';

import MyLink from 'components/MyLink';

import {
  logoutUser,
  showDependentsModal,
  postEndConversation,
  getAppVersion,
} from 'containers/App/actions';
import {
  makeShowDependentsModal,
  makePatientDependents,
  makeSelectVerifyAuth,
  makeSelectSignin,
  makePatientImpersonation,
  makeSelectMemberJoinRoom,
  selectAppVersion,
  makeSelectNurseSignin,
  makeSelectBraintreeSubscriptionStatus,
} from 'containers/App/selectors';

import { MEMBER_TYPES } from 'utils/config';

import akosLogo from 'images/akos-logo.png';
import AUCLogo from 'images/auc-logo.png';

import DependentsModal from './dependentsModal';

const withDashboardLinks = [
  '/360',
  '/benefits',
  '/create-appointment',
  '/connect',
  '/my-account',
  '/medclinic/sso',
  '/faq',
  '/covid',
];

const useStyles = makeStyles(theme => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    background: 'linear-gradient(to right, #55bcbe 0%,#85c3a6 100%) !important',

    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  ucareAppBar: {
    zIndex: theme.zIndex.drawer + 1,
    background: '#fff !important',
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
    [theme.breakpoints.down('md')]: {
      marginRight: 0,
    },
  },
  menuButtonUC: {
    marginRight: 35,
    [theme.breakpoints.down('md')]: {
      marginRight: 0,
    },
    minHeight: 100,
  },
  hide: {
    display: 'none',
  },

  title: {
    display: 'inline !important',
    marginLeft: '1rem',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  logo: {
    display: 'flex',
    width: 210,
    height: 35,
    margin: '0 10px',
  },
  ucareLogo: {
    display: 'flex',
    width: 210,
    height: 50,
    margin: '0 10px',
  },
}));
function Header({
  toggle,
  history,
  impersonatedPatient,
  doLogoutUser,
  doShowDependentsModal,
  doGetAppVersion,
  dispatch,
  isAuthenticated,
  openDependentsModal,
  dependents,
  account,
  signin,
  doPostEndConversation,
  chatroom,
  appVersion,
  nurseSignin,
  subscriptionStatus,
}) {
  const [values, setValues] = useState({
    anchorEl: null,
    password: null,
  });

  const classes = useStyles();

  const [version, setVersion] = useState(localStorage.getItem('memberVersion'));

  useEffect(() => {
    dispatch(doGetAppVersion());
  }, []);

  useEffect(() => {
    if (appVersion && appVersion.data && !appVersion.error) {
      const active = appVersion.data.find(app => app.is_active);
      if (active && version !== active.versions) {
        localStorage.setItem('memberVersion', active.versions);
        setVersion(active.versions);
        window.location.reload();
      }
    }
  }, [appVersion]);

  useEffect(() => {
    if (
      !openDependentsModal.shownAtleastOnce &&
      dependents.data &&
      !!dependents.data.length &&
      (account.data &&
        account.data.patient.membershipTypeId === MEMBER_TYPES.VDPC_Lite &&
        (subscriptionStatus.data && subscriptionStatus.data.is_active))
    ) {
      dispatch(doShowDependentsModal(true));
    }
  }, [dependents, subscriptionStatus, account]);

  const actualPatient = impersonatedPatient.data
    ? impersonatedPatient
    : account;

  const isMenuOpen = Boolean(values.anchorEl);

  const hasDashboardLink = withDashboardLinks.includes(
    history.location.pathname.toLowerCase(),
  );

  const handleProfileMenuOpen = event => {
    setValues({ ...values, anchorEl: event.target });
  };

  const handleMenuClose = () => {
    setValues({ ...values, anchorEl: null });
  };
  const handleFamilyMenuOpen = () => {
    dispatch(doShowDependentsModal(true));
  };

  const handleLogout = () => {
    if (chatroom.data) {
      dispatch(doPostEndConversation(chatroom.data.conference_no));
    }
    dispatch(doLogoutUser());
  };

  const handleChangePassword = () => {
    dispatch(push('/change-password'));
  };

  const handleMyAccount = () => {
    dispatch(push('/my-account'));
  };
  const renderProfileMenu = (
    <Menu
      anchorEl={values.anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      {!nurseSignin.data && (
        <MenuItem onClick={handleMyAccount}>My Account</MenuItem>
      )}
      <MenuItem onClick={handleChangePassword}>Change Password</MenuItem>
      <MenuItem onClick={handleLogout}>Log out</MenuItem>
    </Menu>
  );

  const accounts =
    (account.data && [
      { firstName: 'You', id: account.data.patient.id },
      ...(dependents.data ? dependents.data : []),
    ]) ||
    [];

  const isPathAUC = ['covid-nj'].includes(
    history.location.pathname.split('/')[1],
  );
  let isMemberTypeCovid =
    account.data &&
    account.data.patient.membershipTypeId === MEMBER_TYPES.Covid;

  if (!account.data && signin.data) {
    isMemberTypeCovid =
      signin.data &&
      signin.data.patient.membershipTypeId === MEMBER_TYPES.Covid;
  }
  const logo = isMemberTypeCovid || isPathAUC ? AUCLogo : akosLogo;

  return (
    <AppBar
      position="fixed"
      className={classNames(
        isPathAUC || isMemberTypeCovid ? classes.ucareAppBar : classes.appBar,
        {
          [classes.appBarShift]: toggle,
        },
      )}
    >
      <Toolbar
        disableGutters={!toggle}
        className={classNames(
          isPathAUC || isMemberTypeCovid
            ? classes.menuButtonUC
            : classes.menuButton,
        )}
        style={{ alignItems: 'center', justifyContent: 'space-between' }}
      >
        <div style={{ display: 'flex' }}>
          <figure className={isPathAUC ? classes.ucareLogo : classes.logo}>
            <img src={logo} alt="" style={{ width: '100%' }} />
          </figure>
        </div>
        <div>
          {hasDashboardLink && (
            <Tooltip title="Dashboard">
              <Link component={MyLink} to="/">
                <DashboardIcon color="secondary" />
              </Link>
            </Tooltip>
          )}
          {isAuthenticated && (
            <Fragment>
              {dependents && !!dependents.data.length && (
                <Tooltip title="Family">
                  <IconButton
                    className={classes.icon}
                    aria-owns={
                      openDependentsModal.show ? 'material-appbar' : undefined
                    }
                    aria-haspopup="true"
                    onClick={handleFamilyMenuOpen}
                    color="inherit"
                  >
                    <PeopleIcon
                      color={
                        isMemberTypeCovid || isPathAUC ? 'primary' : 'inherit'
                      }
                    />
                  </IconButton>
                </Tooltip>
              )}
              <Tooltip
                title={
                  actualPatient.data
                    ? actualPatient.data.patient.firstName
                    : 'Profile'
                }
              >
                <IconButton
                  className={classes.icon}
                  aria-owns={isMenuOpen ? 'material-appbar' : undefined}
                  aria-haspopup="true"
                  onClick={handleProfileMenuOpen}
                  color="inherit"
                >
                  <AccountIcon
                    color={
                      isMemberTypeCovid || isPathAUC ? 'primary' : 'inherit'
                    }
                  />
                </IconButton>
              </Tooltip>
              {!dependents.loading && accounts.length > 1 && (
                <DependentsModal accounts={accounts} />
              )}
            </Fragment>
          )}
          {renderProfileMenu}
        </div>
      </Toolbar>
    </AppBar>
  );
}

const { bool, object, func } = PropTypes;
Header.propTypes = {
  toggle: bool,
  history: object,
  dispatch: func.isRequired,
  isAuthenticated: bool,
  doLogoutUser: func.isRequired,
  doShowDependentsModal: func.isRequired,
  doGetAppVersion: func.isRequired,
  openDependentsModal: object.isRequired,
  dependents: object.isRequired,
  impersonatedPatient: object.isRequired,
  account: object.isRequired,
  doPostEndConversation: func.isRequired,
  chatroom: object.isRequired,
  appVersion: object,
  nurseSignin: object.isRequired,
  subscriptionStatus: object.isRequired,
  signin: object.isRequired,
};

Header.defaultProps = {
  toggle: false,
};

const mapStateToProps = createStructuredSelector({
  openDependentsModal: makeShowDependentsModal(),
  dependents: makePatientDependents(),
  impersonatedPatient: makePatientImpersonation(),
  account: makeSelectVerifyAuth(),
  chatroom: makeSelectMemberJoinRoom(),
  appVersion: selectAppVersion(),
  nurseSignin: makeSelectNurseSignin(),
  subscriptionStatus: makeSelectBraintreeSubscriptionStatus(),
  signin: makeSelectSignin(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doLogoutUser: logoutUser,
    doShowDependentsModal: showDependentsModal,
    doPostEndConversation: postEndConversation,
    doGetAppVersion: getAppVersion,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Header);
