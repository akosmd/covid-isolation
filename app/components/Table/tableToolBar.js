import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';

import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

import { lighten } from '@material-ui/core/styles/colorManipulator';

const toolbarStyles = makeStyles(theme => ({
  root: {
    paddingRight: theme.spacing(1),
  },
  /* eslint-disable*/
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  /* eslint-enable */
  spacer: {
    flex: '1 1 100%',
  },
  actions: {
    color: theme.palette.text.secondary,
    width: '80%',
  },
  title: {
    flex: '0 0 auto',
  },
}));

function EnhancedTableToolbar(props) {
  const { numSelected, title, subTitle, options, avatar } = props;
  const classes = toolbarStyles();
  if (!title) return null;

  return (
    <Toolbar
      className={classNames(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      <div className={classes.title}>
        <Grid container justify="center" alignItems="center">
          <Grid item>{avatar && avatar()}</Grid>
          <Grid item>
            <Typography variant="h6" id="tableTitle">
              {title}
            </Typography>
            <Typography>{subTitle}</Typography>
          </Grid>
        </Grid>
      </div>

      <div className={classes.spacer} />
      <div className={classes.actions}>{options && options()}</div>
    </Toolbar>
  );
}

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number,
  title: PropTypes.string,
  subTitle: PropTypes.string,
  options: PropTypes.func,
  avatar: PropTypes.func,
};

export default EnhancedTableToolbar;
