import React, { useState, useEffect } from 'react';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { push } from 'connected-react-router';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

import Button from 'components/GradientButton';
import FlexView from 'components/FlexView';
import ExpansionPanel from 'components/ExpansionPanel';

import theme from 'utils/theme';

import { makeSelectActualPatient } from 'containers/App/selectors';
import {
  makeSelectMedicationList,
  makeSelectAllegyList,
  makeSelectMedicalConditionList,
  makeSelectPatientCallId,
  makeSelectDocAlias,
} from 'containers/App/legacySelectors';
import {
  updatePatientCallId,
  setSymptom,
  addUserToWaitingRoom,
} from 'containers/App/legacyActions';
import symptomList from './symptomList';

const styles = makeStyles({
  root: {
    borderBottom: `1px solid ${theme.palette.primary.main}`,
    padding: '15px 0px',
  },
  item: {
    margin: 'auto',
  },
});

const List = ({
  docAlias,
  patient,
  allergyList,
  medicationList,
  medicalConditionList,
  callId,
  dispatch,
  roomName,
  doUpdatePatientCallId,
  doAddUserToRoom,
  doSetSymptom,
  handleBack,
  onSymptomSelected,
  onCancel,
}) => {
  const [symptom, selectSymptom] = useState([]);

  const handleProceed = () => {
    if (!docAlias.data) return;
    const firstDoc = docAlias.data.result[0];
    const allergies = allergyList.data
      ? allergyList.data.map(allergy => allergy.Name)
      : undefined;
    const medications = medicationList.data
      ? medicationList.data.map(med => med)
      : undefined;
    const conditions = medicalConditionList.data
      ? medicalConditionList.data.map(cond => cond)
      : undefined;

    const params = {
      docId: firstDoc.id,
      groupId: firstDoc.group_id,
      userId: patient.data.patient.patientLegacyId,
      symptom: JSON.stringify(symptom),
      allergy: JSON.stringify(allergies) || '["No Allergies"]',
      medication: JSON.stringify(medications) || '["No Medications"]',
      medical_condition:
        JSON.stringify(conditions) || '["No Pre-existing medical conditions"]',
      call_id: `${callId.data.result.call_id}`,
      date_of_injury: '',
      state_of_injury: '',
      employer_name: '',
      alias: roomName,
    };
    dispatch(doAddUserToRoom(params));
    if (onSymptomSelected) {
      onSymptomSelected();
    } else {
      dispatch(doSetSymptom(symptom));
      dispatch(push('/chat?talkToProvider=true'));
    }
  };

  const chooseSymptom = payload => {
    const data = symptom;
    if (!_.includes(symptom, payload)) {
      data.push(payload);
    } else {
      _.remove(symptom, item => item === payload);
    }
    selectSymptom(data);
  };
  const classes = styles();

  useEffect(() => {
    if (docAlias.data && callId.data) {
      const firstDoc = docAlias.data.result[0];
      const allergies = allergyList.data
        ? allergyList.data.map(allergy => allergy.Name)
        : undefined;
      const medications = medicationList.data
        ? medicationList.data.map(med => med)
        : undefined;
      const conditions = medicalConditionList.data
        ? medicalConditionList.data.map(cond => cond)
        : undefined;

      dispatch(
        doUpdatePatientCallId({
          docId: firstDoc.id,
          groupId: firstDoc.group_id,
          userId: patient.data.patientLegacyId,
          symptom: `[]`,
          allergy: JSON.stringify(allergies) || '["No Allergies"]',
          medication: JSON.stringify(medications) || '["No Medications"]',
          medical_condition:
            JSON.stringify(conditions) ||
            '["No Pre-existing medical conditions"]',
          call_id: `${callId.data.result.call_id}`,
          date_of_injury: '',
          state_of_injury: '',
          employer_name: '',
          alias: roomName,
        }),
      );
    }
  }, [docAlias, callId]);

  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Typography variant="h4" color="primary" className={classes.root}>
            {'Please select symptom patient is calling about.'}
          </Typography>
        </Grid>
        <Grid item xs={12} md={6} className={classes.item}>
          <ExpansionPanel
            data={symptomList}
            id="symptom-list"
            handleSelect={chooseSymptom}
          />
        </Grid>
      </Grid>
      <FlexView justify="center" align="center" maxwidth={700} margin="auto">
        <FlexView margin={8} padding={0}>
          <Button fullWidth onClick={handleBack}>
            <Typography> {'Back'} </Typography>
          </Button>
        </FlexView>
        <FlexView margin={8} padding={0}>
          <Button fullWidth onClick={handleProceed}>
            <Typography> {'Proceed'} </Typography>
          </Button>
        </FlexView>
        <FlexView margin={8} padding={0}>
          <Button
            fullWidth
            onClick={() => (onCancel ? onCancel() : dispatch(push('/')))}
          >
            <Typography> {'Cancel'} </Typography>
          </Button>
        </FlexView>
      </FlexView>
    </>
  );
};

const mapStateToProps = createStructuredSelector({
  patient: makeSelectActualPatient(),
  medicationList: makeSelectMedicationList(),
  allergyList: makeSelectAllegyList(),
  medicalConditionList: makeSelectMedicalConditionList(),
  callId: makeSelectPatientCallId(),
  docAlias: makeSelectDocAlias(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doSetSymptom: setSymptom,
    doUpdatePatientCallId: updatePatientCallId,
    doAddUserToRoom: addUserToWaitingRoom,
  };
}

const { object, func, string } = PropTypes;

List.propTypes = {
  patient: object.isRequired,
  allergyList: object.isRequired,
  medicationList: object.isRequired,
  medicalConditionList: object.isRequired,
  callId: object.isRequired,
  docAlias: object.isRequired,
  dispatch: func.isRequired,
  doSetSymptom: func.isRequired,
  doAddUserToRoom: func.isRequired,
  doUpdatePatientCallId: func.isRequired,
  onSymptomSelected: func,
  handleBack: func.isRequired,
  onCancel: func,
  roomName: string,
};

List.defaultProps = {
  roomName: 'medical',
};
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(List);
