import React, { useState } from 'react';
import PropTypes from 'prop-types';

import AddCondition from './addCondition';
import SelectSymptom from './selectSymptom';

const MedicalHistory = ({ onSymptomSelected, onCancel, roomName }) => {
  const [step, setStep] = useState(1);

  return step === 1 ? (
    <AddCondition
      handleNext={() => setStep(2)}
      onSymptomSelected={onSymptomSelected}
      onCancel={onCancel}
      roomName={roomName}
    />
  ) : (
    <SelectSymptom
      handleBack={() => setStep(1)}
      onSymptomSelected={onSymptomSelected}
      roomName={roomName}
      onCancel={onCancel}
    />
  );
};

MedicalHistory.propTypes = {
  onSymptomSelected: PropTypes.func,
  onCancel: PropTypes.func,
  roomName: PropTypes.string,
};

export default MedicalHistory;
