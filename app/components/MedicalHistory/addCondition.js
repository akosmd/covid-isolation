import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { push } from 'connected-react-router';
import PropTypes from 'prop-types';
import _ from 'lodash';
import moment from 'moment';
import { useSnackbar } from 'notistack';

import { Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

import Autocomplete from 'components/Autocomplete';
import TextField from 'components/TextField';
import Button from 'components/GradientButton';
import FlexView from 'components/FlexView';

import {
  makeSelectActualPatient,
  makeSelectAppError,
} from 'containers/App/selectors';
import {
  makeSelectMedication,
  makeSelectMedicationList,
  makeSelectAllergies,
  makeSelectAllegyList,
  makeSelectMedicalConditionList,
  makeSelectPatientCallId,
} from 'containers/App/legacySelectors';
import {
  getMedications,
  getAllergies,
  setMedicationList,
  setAllergyList,
  setMedicalConditionList,
  generatePatientCallId,
  getDocAlias,
} from 'containers/App/legacyActions';

import theme from 'utils/theme';
import { popData, pushData } from 'utils/formHelper';
import List from './list';

const styles = makeStyles({
  root: {
    borderBottom: `1px solid ${theme.palette.primary.main}`,
    padding: '15px 0px',
  },
});

function Conditions({
  patient,
  appError,
  medications,
  allergies,
  allergyList,
  callId,
  medicationList,
  medicalConditionList,
  dispatch,
  doGetMedications,
  doGetAllergies,
  doSetMedicationList,
  doSetAllergyList,
  doSetMedicalConditionList,
  doGeneratePatientCallId,
  doGetDocAlias,
  handleNext,
  onCancel,
  roomName,
}) {
  const [medication, setMedication] = useState('');
  const [allergy, setAllergy] = useState('');
  const [medicalCondition, setMedicalCondition] = useState('');
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    if (appError.error) {
      enqueueSnackbar(appError.error, {
        variant: 'error',
      });
    }
  }, [appError]);

  useEffect(() => {
    const { patient: detail } = patient.data;
    dispatch(doGetDocAlias({ alias: roomName }));
    if (!callId.loading && !callId.data)
      dispatch(
        doGeneratePatientCallId({
          patientId: detail.patientLegacyId,
          patientEmail: detail.email,
          patientDob: detail.birthDateAt,
          uuid: detail.uuid,
          staffid: 500,
          call_started: moment(Date.now()).format('YYYY/MM/DD'),
          room_name: roomName,
          call_type: 'connect',
          device_type: window.navigator.userAgent,
        }),
      );
  }, [callId]);

  const handleMedicationSearch = payload => {
    setMedication(payload);
    const params = new FormData();
    params.set('searchString', payload);

    const debounced = _.debounce(
      () => dispatch(doGetMedications(params)),
      2000,
    );
    if (payload) debounced();
  };

  const handleAllergySearch = payload => {
    const params = new FormData();
    setAllergy(payload);
    params.set('searchTerm', payload);

    const debounced = _.debounce(() => dispatch(doGetAllergies(params)), 2000);
    if (payload) debounced();
  };

  const onSetMedicationList = payload => {
    if (payload !== null && payload.value) {
      const list = pushData(medicationList.data || [], payload.value);
      dispatch(doSetMedicationList(list));
      setMedication('');
    }
  };

  const removeMedicationList = payload => {
    if (medicationList.data.length === 1) {
      dispatch(doSetMedicationList(false));
    } else {
      const list = popData(medicationList.data, payload);
      dispatch(doSetMedicationList(list));
    }
  };

  const onSetAllergyList = payload => {
    if (payload !== null) {
      const list = pushData(allergyList.data || [], payload);
      dispatch(doSetAllergyList(list));
      setAllergy('');
    }
  };

  const removeAllergyList = payload => {
    if (allergyList.data.length === 1) {
      dispatch(doSetAllergyList(false));
    } else {
      const list = popData(allergyList.data, payload);
      dispatch(doSetAllergyList(list));
    }
  };

  const onSetMedicalConditionList = payload => {
    if (payload !== null) {
      const list = pushData(medicalConditionList.data || [], payload);
      dispatch(doSetMedicalConditionList(list));
      setMedicalCondition('');
    }
  };

  const removeMedicalConditionList = payload => {
    if (medicalConditionList.data.length === 1) {
      dispatch(doSetMedicalConditionList(false));
    } else {
      const list = popData(medicalConditionList.data, payload);
      dispatch(doSetMedicalConditionList(list));
    }
  };

  const onNextClick = () => {
    // dispatch(doSetSymptom(payload));
    dispatch(push('/chat?talkToProvider=true'));
  };

  const classes = styles();

  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Typography variant="h4" color="primary" className={classes.root}>
            Confirm the {`patient's`} medical history
          </Typography>
        </Grid>
        <Grid item xs={12} md={4}>
          <Typography variant="body1">Medications</Typography>
          <Autocomplete
            id="medication-autocomplete"
            data={medications.data || undefined}
            onSelect={onSetMedicationList}
            loading={medications.loading}
            error={medications.error}
            noOptionsText="Type Medication to search"
            label="Type to search"
            loadingText="Searching medications"
            onChange={payload => handleMedicationSearch(payload)}
            fullWidth
            inputValue={medication}
          />
          <List
            id="medication-list"
            data={medicationList.data}
            removeItem={removeMedicationList}
            noListText="No Medications"
          />
        </Grid>
        <Grid item xs={12} md={4}>
          <Typography variant="body1">
            Pre-existing medical conditions
          </Typography>
          <FlexView padding="0px">
            <FlexView padding="0px">
              <TextField
                field={{
                  id: 'medical-conditions',
                  caption: 'Enter Medical Condition',
                  fullWidth: true,
                  value: medicalCondition,
                }}
                onChange={e => setMedicalCondition(e.target.value)}
                variant="outlined"
                keyEvents={{
                  handleKeys: ['enter'],
                  onKeyEvent: () => onSetMedicalConditionList(medicalCondition),
                }}
              />
            </FlexView>
            <FlexView flex="none" padding="0 0 0 15px" margin="16px 0 8px 0">
              <Button
                onClick={() => onSetMedicalConditionList(medicalCondition)}
              >
                <Typography> {'Add'} </Typography>
              </Button>
            </FlexView>
          </FlexView>
          <List
            id="medical-condition-list"
            data={medicalConditionList.data}
            removeItem={removeMedicalConditionList}
            noListText="No medical conditions"
          />
        </Grid>
        <Grid item xs={12} md={4}>
          <Typography variant="body1">Allergies</Typography>
          <Autocomplete
            id="allergies-autocomplete"
            data={allergies.data || []}
            onSelect={onSetAllergyList}
            formatedData
            noOptionsText={
              allergies.data
                ? `Could not find allergy`
                : 'Type allergy to search'
            }
            loading={allergies.loading}
            label="Type to search"
            loadingText="Searching allergy"
            error={allergies.error}
            onChange={payload => handleAllergySearch(payload)}
            fullWidth
            inputValue={allergy}
          />
          <List
            id="allergy-list"
            data={allergyList.data}
            removeItem={removeAllergyList}
            noListText="No allergies"
          />
        </Grid>
      </Grid>
      <FlexView justify="center" align="center" maxwidth={400} margin="auto">
        <FlexView margin={8} padding={0}>
          <Button fullWidth onClick={handleNext || onNextClick}>
            <Typography> {'Next'} </Typography>
          </Button>
        </FlexView>
        <FlexView margin={8} padding={0}>
          <Button
            fullWidth
            onClick={() => (onCancel ? onCancel() : dispatch(push('/')))}
          >
            <Typography> {'Cancel'} </Typography>
          </Button>
        </FlexView>
      </FlexView>
    </>
  );
}

const mapStateToProps = createStructuredSelector({
  patient: makeSelectActualPatient(),
  appError: makeSelectAppError(),
  medications: makeSelectMedication(),
  medicationList: makeSelectMedicationList(),
  allergies: makeSelectAllergies(),
  allergyList: makeSelectAllegyList(),
  medicalConditionList: makeSelectMedicalConditionList(),
  callId: makeSelectPatientCallId(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doGetMedications: getMedications,
    doGetAllergies: getAllergies,
    doSetMedicationList: setMedicationList,
    doSetAllergyList: setAllergyList,
    doSetMedicalConditionList: setMedicalConditionList,
    doGeneratePatientCallId: generatePatientCallId,
    doGetDocAlias: getDocAlias,
  };
}

const { oneOfType, bool, func, object, string } = PropTypes;

Conditions.propTypes = {
  patient: object.isRequired,
  appError: oneOfType([bool, object]),
  medications: object.isRequired,
  callId: object.isRequired,
  allergies: object.isRequired,
  allergyList: object.isRequired,
  medicationList: object.isRequired,
  medicalConditionList: object.isRequired,
  dispatch: func.isRequired,
  doGetMedications: func.isRequired,
  doGetAllergies: func.isRequired,
  doSetMedicationList: func.isRequired,
  doSetAllergyList: func.isRequired,
  doSetMedicalConditionList: func.isRequired,
  doGeneratePatientCallId: func.isRequired,
  doGetDocAlias: func.isRequired,
  handleNext: func.isRequired,
  onCancel: func,
  roomName: string,
};

Conditions.defaultProps = {
  roomName: 'VPDC',
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(Conditions);
