/**
 *
 * Chat
 *
 */

import React, { useEffect, useState } from 'react';
import { DirectLine } from 'botframework-directlinejs';
import ReactWebChat from 'botframework-webchat';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { push } from 'connected-react-router';

import { connect } from 'react-redux';
import { compose } from 'redux';

import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

import HomeIcon from '@material-ui/icons/Home';

import { Grid, CardHeader } from '@material-ui/core';
import { REACT_APP_WEBCHAT_TOKEN } from 'utils/config';

import HelpIcon from '@material-ui/icons/Help';
import ExitIcon from '@material-ui/icons/ExitToApp';
import PhoneIcon from '@material-ui/icons/Phone';

import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';

import 'typeface-lato';
import logo from 'images/akos-logo.png';

import akosIcon from 'images/akos-icon.png';

import {
  makeSelectVerifyAuth,
  makePatientImpersonation,
} from 'containers/App/selectors';

// import { makeSelectSymptom } from 'containers/App/legacySelectors';

import './adaptiveCard.css';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  speedDialWrapper: {
    position: 'relative',
    marginTop: theme.spacing(3),
    height: 380,
  },
  speedDial: {
    position: 'absolute',
    '&.MuiSpeedDial-directionUp, &.MuiSpeedDial-directionLeft': {
      bottom: theme.spacing(15),
      right: theme.spacing(2),
    },
    '&.MuiSpeedDial-directionDown, &.MuiSpeedDial-directionRight': {
      top: theme.spacing(2),
      bottom: theme.spacing(15),
      left: theme.spacing(2),
    },
  },
  grid: {
    width: '99%',
    margin: '0 auto',
    [theme.breakpoints.down('md')]: {
      height: '95vh',
    },
    [theme.breakpoints.up('md')]: {
      height: '98vh',
    },
  },
  card: {
    minWidth: 375,
    height: 500,
    minHeight: 400,
    position: 'fixed',
    bottom: theme.spacing(10),
    right: theme.spacing(2),
    [theme.breakpoints.down('md')]: {
      minWidth: '96%',
      height: '90%',
      width: '96%',
      right: theme.spacing(1),
    },
    display: 'flex',
    flexDirection: 'column',
  },

  cardFullScreen: {
    display: 'flex',
    height: '97%',
    flexDirection: 'column',
    alignContent: 'stretch',
    justifyContent: 'stretch',
  },
  cardContent: {
    minHeight: 300,
    width: '100%',
    height: '100%',
  },

  cardHeaderTitle: {
    color: theme.palette.secondary.main,
  },

  cardRoot: {
    background: 'linear-gradient(to right, #55bcbe 0%,#85c3a6 100%) !important',
  },
  cardSubHeader: {
    color: theme.palette.primary.main,
  },
  fullContent: {
    display: 'flex',
    height: '90vh',
    overflowY: 'scroll',
    padding: 0,
    paddingBottom: '60px !important',
    [theme.breakpoints.up('md')]: {
      paddingBottom: '0 !important',
    },
  },

  chatAvatar: {
    background: '#12bcc5',
  },
  logo: {
    display: 'flex',
    width: 210,
    height: 35,
    margin: '0 10px',
    [theme.breakpoints.down('md')]: {
      marginRight: 0,
      width: 150,
      height: 35,
    },
  },
}));

const styleSet = window.WebChat.createStyleSet({
  rootHeight: '100%',
  rootWidth: '100%',
  bubbleBackground: '#12bcc5',
  bubbleBorderWidth: 'none',
  bubbleBorderColor: '#12bcc5',
  bubbleTextColor: '#fff',
  bubbleBorderRadius: '0px 5px 5px 5px',
  primaryFont: "'Lato', 'sans-serif'",
  bubbleFromUserBorderRadius: '6px',
  bubbleFromUserBackground: '#e1ffc7',
  cardEmphasisBackgroundColor: '#fff',
  fontFamily: 'Lato, Arial, sans-serif',
  botAvatarImage: akosIcon,
  userAvatarInitials: 'P',
});

function AssuredHealthChat({
  dispatch,
  // symptom,
  patient,
  impersonatedPatient,
  history,
}) {
  const [helpAnchor, setHelpAnchor] = useState(null);

  const isMenuOpen = Boolean(helpAnchor);

  const directLine = new DirectLine({
    token: REACT_APP_WEBCHAT_TOKEN,
  });

  const actualPatient = impersonatedPatient.data
    ? impersonatedPatient
    : patient;

  const classes = useStyles();
  useEffect(() => {
    directLine.activity$.subscribe(activity => {
      if (activity && activity.type === 'endOfConversation') {
        setTimeout(() => {
          dispatch(push('/'));
        }, 2000);
      }
    });
  }, []);
  useEffect(() => {
    startScenario();
  }, [patient]);

  useEffect(() => {
    setCallNowEvent();
  }, [directLine.activity$]);

  const handleHelpOpen = event => {
    setHelpAnchor(event.target);
  };

  const hanleHelpClose = () => {
    setHelpAnchor(null);
  };

  const handleStartOver = () => {
    const first = document.querySelectorAll('ul')[0];
    if (first) first.innerHTML = '';

    startScenario();
  };

  const handleQuit = () => {
    directLine
      .postActivity({
        from: { id: patient.data.patient.email },
        type: 'message',
        text: 'quit',
      })
      .subscribe(activity => {
        if (activity) {
          setTimeout(() => {
            dispatch(push('/'));
          }, 3000);
        }
      });
  };

  const handleTalkToProvider = () => {
    const { genderId, ...rest } = actualPatient.data.patient;
    const params = {
      ...rest,
      gender: genderId === 12000 ? 'M' : 'F',
      token: actualPatient.data.token,
    };
    directLine
      .postActivity({
        type: 'event',
        value: {
          trigger: 'Handoff',
          args: {
            patientDetails: params,
          },
        },
        from: { id: patient.data.patient.email },
        name: 'BeginDebugScenario',
      })
      .subscribe(() => {
        setCallNowEvent();
      });
  };

  // directLine.activity$
  //   .filter(activity => activity.type === 'event')
  //   .subscribe(
  //     () =>
  //       symptom.data &&
  //       directLine
  //         .postActivity({
  //           from: { id: patient.data.patient.email },
  //           type: 'message',
  //           text: symptom.data,
  //         })
  //         .subscribe(),
  //   );

  const startScenario = () => {
    const trigger = 'patient_portal_individual';
    const { location } = history;
    let talkToProvider = false;
    const { genderId, ...rest } = actualPatient.data.patient;
    const params = {
      ...rest,
      gender: genderId === 12000 ? 'M' : 'F',
      token: actualPatient.data.token,
    };
    if (location.search.includes('talkToProvider=true')) {
      talkToProvider = true;
    }
    directLine
      .postActivity({
        type: 'event',
        value: {
          trigger,
          args: {
            patientDetails: params,
            talkToProvider,
          },
        },
        from: { id: patient.data.patient.email },
        name: 'BeginDebugScenario',
      })
      .subscribe(() => {
        setCallNowEvent();
      });
  };

  const setCallNowEvent = () => {
    const el = document.getElementsByClassName('style-custom');
    if (el && el[0]) {
      // eslint-disable-next-line func-names
      el[0].onclick = function(e) {
        e.preventDefault();
        handleClickCallNow();
      };
    }
  };

  const handleClickCallNow = () => {
    const tel = document.getElementById('tel');
    if (tel) {
      tel.click();
    }
  };

  const renderHelp = (
    <Menu
      anchorEl={helpAnchor}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={hanleHelpClose}
    >
      <MenuItem onClick={handleStartOver}>Start over</MenuItem>
      <MenuItem onClick={handleTalkToProvider}>Talk to Provider now</MenuItem>
      <MenuItem onClick={handleQuit}>Quit</MenuItem>
    </Menu>
  );

  const renderFullScreen = () => (
    <div className={classes.root}>
      <a href="tel:+1-844-410-6650" id="tel" style={{ display: 'none' }}>
        Call
      </a>
      <Grid
        container
        direction="row"
        justify="space-around"
        alignItems="stretch"
        className={classes.grid}
      >
        <Grid item xs={12}>
          <Card elevation={1} className={classes.cardFullScreen}>
            <CardHeader
              className={classes.cardRoot}
              action={
                <div>
                  <Tooltip title="Back to Home">
                    <IconButton
                      aria-label="Home"
                      color="secondary"
                      onClick={() => history.push('/')}
                    >
                      <HomeIcon />
                    </IconButton>
                  </Tooltip>
                  <Tooltip title="Help">
                    <IconButton
                      aria-label="Help"
                      color="secondary"
                      onClick={handleHelpOpen}
                    >
                      <HelpIcon />
                    </IconButton>
                  </Tooltip>
                  <Tooltip title="Call now">
                    <IconButton
                      aria-label="Call now"
                      color="secondary"
                      onClick={handleClickCallNow}
                    >
                      <PhoneIcon />
                    </IconButton>
                  </Tooltip>

                  <Tooltip title="Quit Chat">
                    <IconButton
                      aria-label="Quit"
                      color="secondary"
                      onClick={handleQuit}
                    >
                      <ExitIcon />
                    </IconButton>
                  </Tooltip>

                  {renderHelp}
                </div>
              }
              subheader={
                <div style={{ display: 'flex' }}>
                  <figure className={classes.logo}>
                    <img src={logo} alt="Akos" style={{ width: '100%' }} />
                  </figure>
                </div>
              }
              classes={{
                title: classes.cardHeaderTitle,
                subheader: classes.cardSubHeader,
              }}
            />
            <CardContent className={classes.fullContent}>
              <ReactWebChat
                directLine={directLine}
                userID={patient.data.patient.email}
                styleSet={styleSet}
              />
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </div>
  );

  return renderFullScreen();
}

const { object, func } = PropTypes;
AssuredHealthChat.propTypes = {
  patient: object.isRequired,
  // symptom: object.isRequired,
  impersonatedPatient: object.isRequired,
  history: object.isRequired,
  dispatch: func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  patient: makeSelectVerifyAuth(),
  impersonatedPatient: makePatientImpersonation(),
  // symptom: makeSelectSymptom(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(AssuredHealthChat);
