/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import history from 'utils/history';
import languageProviderReducer from 'containers/LanguageProvider/reducer';
import patientReducer, { patientMigrations } from 'containers/App/reducer';
import legacyReducer, { legacyMigrations } from 'containers/App/legacyReducer';

import { createMigrate } from 'redux-persist';

import { persist } from 'utils/reduxPersist';

/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
const MIGRATION_DEBUG = false;

const patientPersistConfig = {
  key: 'persistedMember',
  version: 1.2,
  migrate: createMigrate(patientMigrations, { debug: MIGRATION_DEBUG }),
  blacklist: ['appError'],
};
const legacyPersistConfig = {
  key: 'persistedLegacy',
  version: 1.2,
  migrate: createMigrate(legacyMigrations, { debug: MIGRATION_DEBUG }),
  blacklist: [
    'allergyList',
    'medicationList',
    'medicalConditionList',
    'symptom',
    'idImage',
  ],
};

export default function createReducer(injectedReducers = {}) {
  const rootReducer = combineReducers({
    patient: persist(patientPersistConfig, patientReducer),
    legacy: persist(legacyPersistConfig, legacyReducer),
    language: languageProviderReducer,
    router: connectRouter(history),
    ...injectedReducers,
  });

  return rootReducer;
}
