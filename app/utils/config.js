export const API_URL =
  process.env.environment === 'production'
    ? 'https://api.akosmd.com/'
    : 'https://staging-api.akosmd.com/';
export const GI_API_URL =
  process.env.environment === 'production'
    ? 'https://bot.akosmd.com/prodchatbot/ai/'
    : 'https://bot.akosmd.com/devchatbot/ai/';
export const COVID_API_URL =
  process.env.environment === 'production'
    ? 'https://bot.akosmd.com/prodchatbot/ai/'
    : 'https://bot.akosmd.com/devchatbot/ai/';
export const COVID_STANDALONE_URL =
  process.env.environment === 'production'
    ? 'https://bot.akosmd.com/prodchatbot/ai/'
    : 'https://bot.akosmd.com/devchatbot/ai/';
export const COVID_LANDING_URL =
  process.env.environment === 'production'
    ? 'https://medical.akosmd.com/#!/room/'
    : 'https://staging-medical.akosmd.com/#!/room/';
export const COVID_CONFERENCE_URL =
  process.env.environment === 'production'
    ? 'https://medical.akosmd.com/#!/room/'
    : 'https://staging-medical.akosmd.com/#!/room/';
export const NEW_API_URL =
  process.env.environment === 'production'
    ? 'https://newapi.akosmd.com/v1'
    : 'https://staging-newapi.akosmd.com/v1/';
export const REST_API_URL =
  process.env.environment === 'production'
    ? 'https://restapi.akosmd.com/'
    : 'https://staging-restapi.akosmd.com/';

export const CONNECT_API_URL =
  process.env.environment === 'production'
    ? 'https://connect-api.akosmd.com/'
    : 'https://staging-connect-api.akosmd.com/';

export const INTEGRATION_API =
  process.env.environment === 'production'
    ? 'https://integration.akosmd.com/api/'
    : 'https://staging-integration.akosmd.com/api/';

export const INXITE_URL =
  process.env.environment === 'production'
    ? 'https://integration.akosmd.com/api/inxite/patient/'
    : 'https://staging-integration.akosmd.com/api/inxite/patient/';

export const INXITE_SSO_URL =
  process.env.environment === 'production'
    ? 'https://smartcare.inxitehealth.com/'
    : 'https://demo2.inxitehealth.com/';

export const ADVINOW_SSO_URL =
  process.env.environment === 'production'
    ? 'https://pilottools.advinow.net/WebKiosk/patient/saml?token='
    : 'https://pilottools.advinow.net/WebKiosk/patient/saml?token=';

export const { REACT_APP_WEBCHAT_TOKEN } = process.env.variables;

export const { GI_WEBCHAT_TOKEN } = process.env.variables;

export const VDPC_CONFERENCE_URL =
  process.env.environment === 'production'
    ? 'https://medical.akosmd.com/#!/room/VPDC?uuid='
    : 'https://staging-medical.akosmd.com/#!/room/VPDC?uuid=';

export const REGULAR_CONFERENCE_URL =
  process.env.environment === 'production'
    ? 'https://medical.akosmd.com/#!/room/medical?uuid='
    : 'https://staging-medical.akosmd.com/#!/room/medical?uuid=';

export const PROVIDER_PORTAL_URL =
  process.env.environment === 'production'
    ? 'https://provider.akosmd.com/'
    : 'https://staging-provider.akosmd.com/';

export const GOOGLE_CHAT_URL =
  process.env.environment === 'production'
    ? 'https://chat.googleapis.com/v1/spaces/AAAAJjWs0gM/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=ZNtlNKXv06yt9oxamO8dbc0P9MdflNQtrsgq3Aww2KQ%3D'
    : 'https://chat.googleapis.com/v1/spaces/AAAAwgtXzH0/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=BdiO02-s5YjeWb39PeSeJ3CrULrnxHdEDhDkkhkvZ-k%3D';

export const SOCKET_URL =
  process.env.environment === 'production'
    ? 'https://socket.akosmd.com'
    : 'https://staging-socket.akosmd.com';

export const restapiBasicAuth = {
  username: '12345',
  password: '12345',
};
export const newApiBasicAuth = {
  username: '12345',
  password: '12345',
};

export const WS_HOST =
  process.env.environment === 'production'
    ? 'integration.akosmd.com'
    : 'staging-integration.akosmd.com';

export const { PUSHER_KEY } = process.env.variables;

export const GOODBYE_MESSAGE =
  "Goodbye! If you have any further questions, please don't hesitate to contact us again.";

export const { OPEN_TOK_API } = process.env.variables;

export const MEMBER_TYPES = {
  individual: 70001,
  VDPC: 70002,
  VDPC_Lite: 70003,
  SharedHealth: 70004,
  MedClinic: 70005,
  Covid: 70006,
};

export const VDPC_LITE_PLAN_TYPES = {
  employee: 'EMP',
  employeeWithSpouse: 'ESP',
  employeeWithChildren: 'ECH',
  family: 'FAM',
};
