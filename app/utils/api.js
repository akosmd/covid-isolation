/* eslint-disable indent */
import axios from 'axios';

import {
  API_URL,
  NEW_API_URL,
  INTEGRATION_API,
  REST_API_URL,
  CONNECT_API_URL,
  GI_API_URL,
  COVID_API_URL,
} from 'utils/config';

export const api = axios.create({
  baseURL: API_URL,
});
export const newApi = axios.create({
  baseURL: NEW_API_URL,
});

export const integrationApi = axios.create({
  baseURL: INTEGRATION_API,
});

export const restApi = axios.create({
  baseURL: REST_API_URL,
});

export const connectApi = axios.create({
  baseURL: CONNECT_API_URL,
});
export const giApi = axios.create({
  baseURL: GI_API_URL,
});
export const covidApi = axios.create({
  baseURL: COVID_API_URL,
});

[api, giApi, covidApi, newApi, restApi, integrationApi, connectApi].forEach(
  item => {
    item.interceptors.request.use(
      config => config,
      error => Promise.reject(error),
    );
    item.interceptors.response.use(
      response => response,
      error => {
        // Do something with response error
        // if (!error.response) return Promise.reject(error);
        switch (error.response.status) {
          case 401: {
            const payload = error.response
              ? {
                  status: 401,
                  error: error.response.messsage || error.response,
                }
              : {
                  status: 401,
                  error: 'User not Authorized',
                };
            return payload;
          }
          case 403: {
            const payload = error.response
              ? {
                  status: 403,
                  error: error.response.data.messsage || error.response,
                }
              : {
                  status: 403,
                  error: 'User not Authorized',
                };
            return payload;
          }
          case 422:
          case 404:
          case 400:
            return error.response;
          case 500:
            return error.response;
          case 409:
            return { status: 409, error: error.response.data };
          default:
            return Promise.reject(error);
        }
      },
    );
  },
);
