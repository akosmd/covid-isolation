export const RESTART_ON_REMOUNT = '@@saga-injector/restart-on-remount';
export const DAEMON = '@@saga-injector/daemon';
export const ONCE_TILL_UNMOUNT = '@@saga-injector/once-till-unmount';

export const FORGOT_PASS_URL =
  process.env.environment === 'production'
    ? 'https://tools.akosmd.com/forgot-password/member'
    : 'https://staging-tools.akosmd.com/forgot-password/member';
